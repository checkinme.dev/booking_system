<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PatientLaboView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW patient_labo_view AS
        (
            SELECT pateint.pateinid,
            pateint.firstname,
            pateint.lstname,
            pateint.allergies,
            pateint.sex,
            pateint.age,
            pateint.history_disease,
            pateint.address,
            pateint.address2,
            pateint.village,
            pateint.commune, 
            pateint.district, 
            pateint.phone1, 
            pateint.pro_city,
            labo_lab.id, 
            labo_lab.Date,
            labo_lab.diagnosis,
            labo_lab.labId,
            labo_lab.BP,
            labo_lab.Pr,
            labo_lab.rr,
            labo_lab.Spo2,
            labo_lab.T,
            labo_lab.remark,
            labo_lab.`status`,
            labo_lab.created_by,
            labo_lab.created_at,
            labo_lab.updated_by
            FROM pateint INNER JOIN labo_lab ON pateint.pateinid = labo_lab.pateinId
        )
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS patient_labo_view');
    }
}
