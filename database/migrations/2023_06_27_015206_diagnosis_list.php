<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DiagnosisList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosis_list', function (Blueprint $table) {
            $table->id();
            $table->string("preid", 25)->nullable();
            $table->string("product_no",25)->nullable();
            $table->string("description", 100)->nullable();
            $table->string("dose", 100)->nullable();
            $table->string("morning", 25)->nullable();
            $table->string("afternoon", 25)->nullable();
            $table->string("evening", 25)->nullable();
            $table->string("night", 25)->nullable();
            $table->string("unit_price", 100)->nullable();
            $table->string("day", 25)->nullable();
            $table->string("unit", 25)->nullable();
            $table->string("qty", 25)->nullable();
            $table->string("amount", 25)->nullable();
            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('diagnosis_list'); 
    }
}
