<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FurnitureView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW furniture_view AS
        (
            SELECT rf.*, f.furniture, f.description, f.image FROM room_furniture rf INNER JOIN furnitures f ON f.id = rf.furniture_no
        )
      ");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS furniture_view');

    }
}
