<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductReorderPoint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW product_reorder_point AS (
            SELECT
            al.*,
            COALESCE((SELECT COALESCE (SUM(inventory), 0) / 3 AS amc_inventery FROM product_in_sale WHERE created_at >= now() - INTERVAL 3 MONTH  and product_no = al.product_no  GROUP BY product_no),0) AS amc_inventery,
            COALESCE ((COALESCE((SELECT(COALESCE (SUM(unit_price), 0))/3 AS amc_inventery FROM saleline WHERE created_at >= now() - INTERVAL 3 MONTH  and product_no = al.product_no  GROUP BY product_no),0)),0) AS amc_amount,
            COALESCE (COALESCE (SUM(al.inventorys), 0) / (COALESCE((SELECT COALESCE (SUM(inventory), 0) / 3 AS amc_inventery FROM product_in_sale WHERE created_at >= now() - INTERVAL 3 MONTH  and product_no = al.product_no  GROUP BY product_no),1)),0) AS mos
            FROM
                allproductinstock al
            LEFT OUTER JOIN saleline sl ON al.product_no = sl.product_no
            WHERE
                CAST(reorder_point AS INT) >= CAST(inventorys AS INT)
            GROUP BY al.product_no,al.description,al.stock_unit_of_measure_code,al.reorder_point,al.inventorys,unit_price
            ORDER BY inventorys
        )"
    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS product_reorder_point');
    }
}