<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW product_services AS (
            SELECT
                lb.`id`,
                p.product_no,
                p.description,
                lb.test_type,
                p.group_code,
                lb.normal_value,
                p.unit_price,
                lb.`status`
            FROM
                product p
            INNER JOIN labotest lb ON p.product_no = lb.product_no
            WHERE
                p.type = 'ServiceTest'
         )");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS product_services");
    }
}