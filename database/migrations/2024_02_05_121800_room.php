<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Room extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->id();
            $table->string("roomtype_no",255)->nullable();
            $table->string("room_no",255)->nullable();
            $table->string("size",255)->nullable();
            $table->string("image",255)->nullable();
            $table->string("room_name", 100)->nullable();
            $table->longText("description")->nullable();
            $table->string("currency", 50)->nullable();
            $table->string("adults", 50)->nullable();
            $table->string("room_price", 50)->nullable();
            $table->string("status", 10)->nullable();
            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room');
    }
}
