<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExpritAlert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW  exprit_alert AS (
            SELECT
                pu.id,
                P.product_no,
                P.description,
                P.reorder_point,
                pu.document_no,
            COALESCE((SELECT COALESCE (SUM(inventory), 0) / 1 AS amc_inventery FROM product_in_sale WHERE created_at >= now() - INTERVAL 1 MONTH  and product_no = P.product_no  GROUP BY product_no),0) AS amc_inventery,
            COALESCE ((COALESCE((SELECT(COALESCE (SUM(unit_price), 0))/1 AS amc_inventery FROM saleline WHERE created_at >= now() - INTERVAL 1 MONTH  and product_no = P.product_no  GROUP BY product_no),0)),0) AS amc_amount,
            COALESCE (COALESCE (SUM(pu.inventory), 0) / (COALESCE((SELECT COALESCE (SUM(inventory), 0) / 1 AS amc_inventery FROM product_in_sale WHERE created_at >= now() - INTERVAL 1 MONTH  and product_no = P.product_no  GROUP BY product_no),1)),0) AS mos,
            P.stock_unit_of_measure_code as stock_uint,
            COALESCE ((SELECT COALESCE (unit_price, 0)FROM product_variant_code WHERE product_no = P.product_no and variant_unit_of_measure_code = P.stock_unit_of_measure_code), 0) AS unit_price,
            COALESCE (SUM(pu.inventory), 0) AS stock_onhand,
            COALESCE (SUM(pu.inventory), 0)*COALESCE((SELECT COALESCE (unit_price, 0)FROM product_variant_code WHERE product_no = P.product_no and variant_unit_of_measure_code = P.stock_unit_of_measure_code),0) AS stock_aoumt,
            pu.curency_code,
            COALESCE(to_days(pu.exprit_date) - to_days(CURRENT_TIMESTAMP()),0) AS countday_exprit,
            pu.exprit_date,
            pu.created_at
            FROM
                product P
            LEFT OUTER JOIN product_stock_keeping_units pu ON P.product_no = pu.product_no
            WHERE
                P.type = 'Products' and pu.statuse = 'open' and pu.exprit_date <= now() + INTERVAL 6 MONTH 
            GROUP BY
                pu.id,
                P.product_no,
                P.product_barcode,
                P.description,
                P.stock_unit_of_measure_code,
                P.reorder_point,
                P.unit_price,
            pu.unit_price,
            pu.curency_code,
            pu.exprit_date,
            pu.document_no,
            pu.created_at
            ORDER BY countday_exprit ASC
         )");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
          DB::statement("DROP VIEW IF EXISTS exprit_alert");
    }
}