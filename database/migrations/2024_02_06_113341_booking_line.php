<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BookingLine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_line', function (Blueprint $table) {
            $table->id();
            $table->string("document_no", 25)->nullable();
            $table->string("line_no", 25)->nullable();
            $table->string("option", 255)->nullable();
            $table->string("checkindate", 25)->nullable();
            $table->string("checkoutdate", 25)->nullable();
            $table->string("inventory", 25)->nullable();;
            $table->string("dicount", 255)->nullable(); 
            $table->string("price", 255)->nullable();
            $table->string("total_amount", 255)->nullable();
            $table->string("curency_code", 25)->nullable();
            $table->string("remark", 255)->nullable();
            $table->string("statue", 25)->nullable();
            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_line');
    }
}
