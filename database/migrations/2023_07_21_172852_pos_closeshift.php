<?php

use Illuminate\Database\Migrations\Migration;

class PosCloseshift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW pos_closeshift_view AS
        (
         SELECT
            sf.id,
            sf.deposit,
            sf.deposit2,
            sf.description,
            s.type,
            s.paymeny_method,
            COALESCE (SUM(s.total_amount), 0) AS total_amount,
            (
                COALESCE (SUM(s.total_amount), 0) / COALESCE (s.exchane_rate, 0)
            ) AS total_amount_usd,
            sf.created_at,
            sf.created_by
        FROM
            sale s
        INNER JOIN close_shift sf ON s.closeshift_id = sf.id
        GROUP BY
            sf.deposit,
            sf.deposit2,
            sf.description,
            s.paymeny_method,
            s.type,
            s.exchane_rate,
            sf.created_at,
            sf.created_by,
            sf.id   
        )
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS pos_closeshift_view');
    }
}
