<?php

use Illuminate\Database\Migrations\Migration;

class StockOnhandView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW stock_onhand AS (
            SELECT
              P.product_no,
              P.description,
              P.reorder_point,
              COALESCE (
                (
                  SELECT
                    COALESCE (SUM(inventory), 0) AS amc_inventery
                  FROM
                    product_in_sale
                  WHERE
                    created_at >= now() - INTERVAL 1 MONTH
                  AND product_no = P.product_no
                  GROUP BY
                    product_no
                  ORDER BY
                    created_at ASC
                ),
                0
              ) AS amc_inventery,
              COALESCE (
                (
                  COALESCE (
                    (
                      SELECT
                        (
                          COALESCE (SUM(unit_price), 0)
                        ) / 3 AS amc_inventery
                      FROM
                        saleline
                      WHERE
                        created_at >= now() - INTERVAL 3 MONTH
                      AND product_no = P.product_no
                      GROUP BY
                        product_no
                    ),
                    0
                  )
                ),
                0
              ) AS amc_amount,
              (
                COALESCE (
                  (
                    SELECT
                      COALESCE (SUM(inventory), 0)
                    FROM
                      product_stock_keeping_units
                    WHERE
                      statuse = 'open'
                    AND product_no = P.product_no
                    GROUP BY
                      product_no,
                      description
                  ),
                  0
                ) / COALESCE (
                  (
                    SELECT
                      COALESCE (SUM(inventory), 0) / 1 AS amc_inventery
                    FROM
                      product_in_sale
                    WHERE
                      created_at >= now() - INTERVAL 1 MONTH
                    AND product_no = P.product_no
                    GROUP BY
                      product_no
                    ORDER BY
                      created_at ASC
                  ),
                  1
                )
              ) AS mos,
              P.stock_unit_of_measure_code AS stock_uint,
              COALESCE (
              (SELECT COALESCE (unit_price, 0)FROM product_variant_code WHERE product_no = P.product_no and variant_unit_of_measure_code = P.stock_unit_of_measure_code),
                0
              ) AS unit_price,
              COALESCE (
                (
                  SELECT
                    COALESCE (SUM(inventory), 0)
                  FROM
                    product_stock_keeping_units
                  WHERE
                    statuse = 'open'
                  AND product_no = P.product_no
                  GROUP BY
                    product_no,
                    description
                ),
                0
              ) AS stock_onhand,
              COALESCE (
                (
                  SELECT
                    COALESCE (SUM(inventory), 0) * COALESCE (
                      (SELECT COALESCE (unit_price, 0)FROM product_variant_code WHERE product_no = P.product_no and variant_unit_of_measure_code = P.stock_unit_of_measure_code),
                      0
                    ) AS unit_price
                  FROM
                    product_stock_keeping_units
                  WHERE
                    statuse = 'open'
                  AND product_no = P.product_no
                  GROUP BY
                    product_no,
                    description
                ),
                0
              ) AS stock_aoumt,
              (
                SELECT
                  curency_code
                FROM
                  product_stock_keeping_units
                WHERE
                  statuse = 'open'
                LIMIT 1
              ) AS curency_code
            FROM
              product P
            WHERE
              P.type = 'Products'
            GROUP BY
              P.product_no,
              P.product_barcode,
              P.description,
              P.stock_unit_of_measure_code,
              P.reorder_point
         )");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS stock_onhand');
    }
}
