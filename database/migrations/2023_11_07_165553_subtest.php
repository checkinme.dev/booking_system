<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Subtest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtest', function (Blueprint $table) {
            $table->id();
            $table->string("product_no", 255)->nullable();
            $table->string("sub_product_no",255)->nullable();
            $table->string("description", 100)->nullable();
            $table->string("stock_unit_of_measure_code", 100)->nullable();
            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subtest');
    }
}
