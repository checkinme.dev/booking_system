<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PaymentReceiveView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW payment_receive_view AS
        (
            SELECT
            pam.id,
            pam.document_no,
            pam.document,
            pam.document_type,
            pam.decription,
            pam.totale_balanec,
            pam.exchane_rate,
            pam.currency_code,
            pam.statue,
            pam.paymant_amount,
            pam.sub_code,
            ro.created_at,
            ro.total_amount
        FROM
            payment_amount pam
        INNER JOIN receive_order ro ON pam.sub_code = ro.id
        )
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS payment_receive_view');
    }
}
