<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roomprice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_price', function (Blueprint $table) {
            $table->id();
            $table->string("room_no",255)->nullable();
            $table->string("description",255)->nullable();
            $table->string("type", 100)->nullable();
            $table->string("qty", 10)->nullable();
            $table->string("price", 10)->nullable();
            $table->string("status", 10)->nullable();
            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->string("created_at", 50)->nullable();
            $table->string("updated_at", 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
