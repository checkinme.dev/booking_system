<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Expenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->id();
            $table->string('document_no',50)->nullable();
            $table->string('document',250)->nullable();
            $table->string('description',250)->nullable();
            $table->string('Payment_method',25)->nullable();
            $table->string('totat_exspan',100)->nullable();
            $table->string("statue", 25)->nullable();
            $table->string("exspan_date", 25)->nullable();
            $table->string("created_by", 50)->nullable();
            $table->string("updated_by", 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expenses');  
       }
}
