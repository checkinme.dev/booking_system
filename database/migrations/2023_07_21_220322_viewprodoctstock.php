<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Viewprodoctstock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::statement("
        CREATE VIEW viewprodoctstock AS
        (
            SELECT
            COALESCE (
                (
                    SELECT
                        COALESCE (SUM(inventory), 0)
                    FROM
                        product_stock_keeping_units
                    WHERE
                        statuse = 'open'
                    AND product_no = P.product_no
                    GROUP BY
                        product_no,
                        description
                ),
                0
            ) AS inventory,
            P.*
        FROM
            product P
        )
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS viewprodoctstock');
    }
}
