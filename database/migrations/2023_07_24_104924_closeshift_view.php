<?php

use Illuminate\Database\Migrations\Migration;

class CloseshiftView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
        CREATE VIEW closeshift_view AS
        (
            SELECT
                cf.id,
                u.user_code,
                u.user_name,
                COALESCE (cf.deposit, 0) AS deposit,
                COALESCE (cf.deposit2, 0) AS deposit2,
            COALESCE ((SELECT exchane_rate FROM sale WHERE closeshift_id = cf.id ORDER BY created_at ASC LIMIT 1),0) as exchane_rate,
                COALESCE (SUM(s.total_amount), 0) AS total_amount,
                COALESCE (SUM(s.total_amount)/ COALESCE((SELECT exchane_rate FROM sale WHERE closeshift_id = cf.id ORDER BY created_at ASC LIMIT 1), 1)) AS total_amountusd,
            cf.description,
                cf.created_at
            FROM
                close_shift cf
            INNER JOIN users u ON u.id = cf.created_by
            INNER JOIN sale s ON s.closeshift_id = cf.id
            GROUP BY
                cf.id,
                u.user_code,
                u.user_name,
                cf.deposit,
                cf.deposit2,
                cf.description,
                cf.created_at
        )
      ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS closeshift_view');
    }
}
