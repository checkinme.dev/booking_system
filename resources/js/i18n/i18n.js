import lngtest from "../i18n/lngtext.js";
class ii8n{
    constructor() {
        try {
           
            this.leg = window.localStorage.getItem("leg");
            if(this.leg == null || this.leg ==''){
                let legn = "English";
                window.localStorage.setItem("leg", legn);
                this.leg = window.localStorage.getItem("leg");
            }
        } catch (error) {}
    }
    check() {
        if(this.leg != null || this.leg !=''){
            let legn = "English";
            window.localStorage.setItem("leg", legn);
            return legn;
        }
    }
    changeleg(){
        if(this.leg =="English"){
            let legn = "ខ្មែរ";
            window.localStorage.setItem("leg", legn);
        }else{
            let legn = "English";
            window.localStorage.setItem("leg", legn);
        }
        return true;
    }
    checkleg(){
        if(this.leg =="English" || this.leg ==""){
           return true;
        }else{
          return false;
        }
    }
    lngText(text){
        var xx = lngtest.getlngText()[text];
        try {
            if(this.leg =="English" || this.leg ==""){

                return xx['name_en'];
     
             }else{
     
                 return xx['name_kh'];
             }
        } catch (error) {
            console.log("Errortra :"+ text);
        }
    }
}
export default new ii8n();