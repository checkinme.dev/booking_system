import axios from "axios";
class Auth {
    constructor() {
        try {
            this.token = window.localStorage.getItem("token");
            let userData = window.localStorage.getItem("user");
            this.user = userData ? JSON.parse(userData) : null;
            if (this.token) {
                axios.defaults.headers.common["Authorization"] =
                    "Bearer " + this.token;
            }
        } catch (error) {}
    }
    login(token, user, permission) {
        window.localStorage.setItem("token", token);
        window.localStorage.setItem("user", JSON.stringify(user));
        window.localStorage.setItem("permission", permission);
        axios.defaults.headers.common["Authorization"] = "Bearer " + token;
        this.token = token;
        this.user = user;
    }
    check() {
        return !!this.token;
    }
    checkPermission(formname) {
        let checkme = false;
        let userData = window.localStorage.getItem("user");
        let getuser = userData ? JSON.parse(userData) : null;
        let permissionData = window.localStorage.getItem("permission");
        let ep = permissionData.split(",");
        ep.forEach(element => {

            if (!checkme && element == formname) {
                checkme = true;
            }

        });
        if (!checkme) {
            if (getuser.role == "System" || getuser.role == "Admin") {
                checkme = true;
            } else {
                checkme = false;
            }
        }
        return checkme;

    }
    logout() {
        // window.localStorage.clear();
        window.localStorage.removeItem("token");
        window.localStorage.removeItem("user");
        window.localStorage.removeItem("permission");
        this.user = null;
    }
}
export default new Auth();
