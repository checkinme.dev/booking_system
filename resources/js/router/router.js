import {
    createRouter,
    createWebHistory
} from "vue-router";
import App from "../components/App.vue";
import Brands from "../components/Brands/Brands.vue";
import Product from "../components/Product/product.vue";
import Viewproduct from "../components/Product/view-product.vue";
import Category from "../components/Category/Category.vue";
import Supplies from "../components/Supplies/Supplies.vue";
import Group from "../components/Group/Product_group.vue";
import Serailcode from "../components/Serailcode/serailcode.vue";
import Purchase from "../components/Purchase/purchase-order.vue";
import Purchasview from "../components/Purchase/view-puchese.vue";
import MenuProductSetting from "../components/MenuProductsetup.vue";
import MenuPurchase from "../components/MenuPurchase.vue";
import MenuStock from "../components/MenuStock.vue";
import Stockalert from "../components/StockAlert/stockalert.vue";
import MenuClinic from "../components/MenuClinic.vue";
import MenuMedical from "../components/MenuMedical.vue";
import Receptorder from "../components/Recept/Recept-order.vue";
import viewRecept from "../components/Recept/view-Recept.vue";
import unitcode from "../components/unitcode/unitcode.vue";
import StockTransection from "../components/StockTransection/stock-transection.vue";
import Currency from "../components/Currency/currency.vue";
import ExchangeRate from "../components/ExchangeRate/ExchangeRate.vue";
import StockCount from "../components/StockCount/stock-count.vue";
import Viewstockcount from "../components/StockCount/view-stock-count.vue";
import paymenMethod from "../components/PaymentMethod/paymenMethod.vue";
import paymenview from "../components/Payment/paymen_view.vue";
import paymen from "../components/Payment/paymen.vue";
import login from "../components/login/login.vue";
import Laboratory from "../components/Prescriptions/prescriptions.vue";
import Consultation from "../components/Consultation/consultation.vue";
import convertoInvoine from "../components/Prescriptions/convertoInvoine.vue";
import Labolap from "../components/Laboratory/Laboratory.vue";
import test from '../components/Laboratory/Test.vue';
import addTestLabo from "../components/Laboratory/AddTestLabo.vue";
import patientRegister from '../components/Register/patientRegister.vue';
import testserviceview from '../components/Laboratory/TestServiceView.vue'
import TestInvoice from '../components/TestInvoice/testinvoice.vue';
import viewlabolap from "../components/Laboratory/Viewlaboratory.vue";
import Employee from "../components/Employee/employee.vue"
import Viewlaboratory from '../components/viewPrescription/Viewprescription.vue';
import detailshif from '../components/Detailcloseshif/detailshif.vue';
import saledetail from '../components/Saledetail/saledetail.vue';
import company from "../components/Company/company.vue";
import BeginningStock from "../components/StockBeginnig/BeginningStock.vue"
import StockOnHand from "../components/StockOnHand/StockOnHand.vue"
import createbeginingstock from "../components/StockBeginnig/CreateBeginningStock.vue";
import role from "../components/Role/role.vue";
import stockonhand_date from "../components/Expire/expire.vue";
import ex_alert from "../components/Exlert/ex_alert.vue";
import user from "../components/User/user.vue";
import permission from "../components/Permission/permission.vue";
import prescription from "../components/Diagnosis/Diagnosis.vue";
import prescriptionview from "../components/PrescriptionView/prescriptionview.vue";
import viewall from "../components/Viewall/viewall.vue";
import report from "../components/Report/report.vue";
import salereport from "../components/Report/salereport.vue";
import saleitem from "../components/Report/saleitem.vue";
import stockinven from "../components/Report/stockinven.vue";
import incomereport from "../components/Report/sale-product.vue";
import incomepen from "../components/Report/income-expen.vue";
import closereport from "../components/Report/closereport.vue";
import expensesreport from "../components/Expenses/expenses.vue";
import expenses from "../components/Expensesview/expensesview.vue";
import expenses1 from "../components/Expenses1/expenses1.vue";
import income from "../components/Income/income.vue";
import income1 from "../components/Income1/income1.vue";
import labototal from "../components/Labototal/labototal.vue";
import groupService from "../components/Groupservice/groupService.vue";
import calendar from "../components/Calendar/calendar";
import calendarView from "../components/Calendar/calendarView.vue";
import Appointment from "../components/Appointment/Appointment.vue";
import booking from "../components/booking/booking.vue";
import room_type from "../components/room_type/room_type.vue";
import room from "../components/room_type/room.vue";
import room_furniture from "../components/room_type/room_furniture.vue";
import booking_line from "../components/room_type/booking_line.vue";
import room_price from "../components/room_type/room_price.vue";
import bookingone from "../components/room_type/bookingone.vue";
import furniture from "../components/room_type/furniture.vue";
import testone from "../components/room_type/testone.vue";
import product_model from "../components/Model/Product_model.vue";
import pro_unittype from "../components/UnitType/Product_UnitType.vue";
import view_room from "../components/room_type/view_room.vue";

const routes = [
    {
        path: '/appointment',
        component : Appointment,
        name : 'Appointment'
    },
    {
        path: "/dashboard",
        component: App,
        name: "App"
    },
    {
        path: "/brands",
        component: Brands,
        name: "Brands"
    },
    {
        path: "/Product",
        component: Product,
        name: "Product"
    },
    {
        path: "/viewproduct",
        component: Viewproduct,
        name: "Viewproduct"
    },
    {
        path: "/category",
        component: Category,
        name:'Category'
    },
    {
        path: "/supplies",
        component: Supplies,
        name:'Supplies'
    },
    {
        path: "/incomepen",
        component: incomepen,
        name: "incomepen"
    },
    {
        path: "/group",
        component: Group,
        name:'Group'
    },
    {
        path: "/serailcode",
        component: Serailcode
    },
    {
        path: "/purchase",
        component: Purchase,
        name: "Purchase"
    },
    {
        path: "/Purchasview",
        component: Purchasview,
        name:'Purchasview'
    },
    {
        path: "/productsetting",
        component: MenuProductSetting,
        name:'MenuProductSetting'
    },
    {
        path: "/menupurchase",
        component: MenuPurchase,
        name:'MenuPurchase'
    },
    {
        path: "/menustock",
        component: MenuStock,
        name:'MenuStock'
    },
    {
        path: "/stockalert",
        component: Stockalert,
        name:'Stockalert'
    },
    {
        path: "/menuclinic",
        component: MenuClinic,
        name:'MenuClinic'
    },
    {
        path: "/pos",
        component: MenuMedical,
        name:'MenuMedical'
    },
    {
        path: "/receptorder",
        component: Receptorder,
        name: "recept"
    },
    {
        path: "/viewrecept",
        component: viewRecept,
        name:'viewRecept'
    },
    {
        path: "/unitcode",
        component: unitcode,
        name:'unitcode'
    },
    {
        path: "/stocktransection",
        component: StockTransection,
        name:'StockTransection'
    },
    {
        path: "/exchangerate",
        component: Currency,
        name:'Currency'
    },
    {
        path: "/stockcount",
        component: StockCount,
        name: "stockcount"
    },
    {
        path: "/viewstockcount",
        component: Viewstockcount,
        name:'Viewstockcount'
    },
    {
        path: "/paymenMethod",
        component: paymenMethod,
        name:'paymenMethod'
    },
    {
        path: "/paymen",
        component: paymen,
        name: "paymen"
    },
    {
        path: "/paymenview",
        component: paymenview,
        name:'paymenview'
    },
    {
        path: "/",
        component: login,
        name: "login"
    },
    {
        path: "/consultation",
        component: Consultation,
        name: "consultation"
    },
    {
        path: "/Laboratory",
        component: Laboratory,
        name: "Laboratory"
    },
    {
        path: "/prescriptionview",
        component: prescriptionview,
        name: "prescriptionview"
    },
    {
        path: "/stockinven",
        component: stockinven,
        name: "stockinven"
    },
    {
        path: "/convertoinvoine",
        component: convertoInvoine,
        name: "convertoinvoine",
    },
    {
        path: "/incomereport",
        component: incomereport,
        name: "incomereport",
    },
    {
        path: "/group",
        component: groupService,
        name: "group",
    },
    {
        path: "/Labolap",
        component: Labolap,
        name: "Labolap"
    },
    {
        path: "/testitem",
        component: test,
        name: "test"
    },
    {
        path: "/addtestlabo",
        component: addTestLabo,
        name: "addtestlabo"
    },
    {
        path: "/employee",
        component: Employee,
        name: "employee"
    },
    {
        path: "/patientRegister",
        component: patientRegister,
        name: "patientRegister",
    },
    {
        path: "/Viewlaboratory",
        component: Viewlaboratory,
        name: "Viewlaboratory",
    },
    {
        path: "/testserviceview",
        component: testserviceview,
        name: "Testserviceview",
    },
    {
        path: "/expensesreport",
        component: expensesreport,
        name: "expensesreport",
    },
    {
        path: "/expenses",
        component: expenses,
        name: "expenses",
    },
    {
        path: "/expenses1",
        component: expenses1,
        name: "expenses1",
    },
    {
        path: "/income",
        component: income,
        name: "income",
    },
    {
        path: "/income1",
        component: income1,
        name: "income1",
    },
    {
        path: "/labototal",
        component: labototal,
        name: "labototal",
    },
    {
        path: "/report",
        component: report,
        name: "report",
    },
    {
        path: "/salereport",
        component: salereport,
        name: "salereport",
    },
    {
        path: "/saleitem",
        component: saleitem,
        name: "saleitem",
    },
    {
        path: "/closereport",
        component: closereport,
        name: "closereport",
    },
    {
        path: "/testinvoice",
        component: TestInvoice,
        name: "testinvoice"
    },
    {
        path: "/viewlabolap",
        component: viewlabolap,
        name: "viewlabolap",
    },
    {
        path: "/detailshif",
        component: detailshif,
        name: "detailshif"
    },
    {
        path: "/saledetail",
        component: saledetail,
        name: "saledetail"
    },
    {
        path: "/company",
        component: company,
        name: "company"
    },
    {
        path: '/beginningstock',
        component: BeginningStock,
        name: 'beginningstock'
    },
    {
        path: '/stockonhand',
        component: StockOnHand,
        name: 'stockonhand'
    },
    {
        path: "/creatbeginnigstock",
        component: createbeginingstock,
        name: 'createbeginnigstock'
    },
    {
        path: "/role",
        component: role,
        name: 'role'
    },
    {
        path: "/user",
        component: user,
        name: 'user'
    },
    {
        path: "/permission",
        component: permission,
        name: 'permission'
    },
    {
        path: '/prescription',
        component: prescription,
        name: 'prescription'
    },
    {
        path: "/stockonhand_date",
        component: stockonhand_date,
        name: 'stockonhand_date'
    },
    {
        path: "/ex_alert",
        component: ex_alert,
        name: 'ex_alert'
    },
    {
        path: "/viewall",
        component: viewall,
        name: 'viewall'
    },
    {
        path: "/booking",
        component: booking,
        name: 'booking'
    },
    {
        path: "/room_type",
        component: room_type,
        name: 'room_type'
    },
    {
        path: "/room",
        component: room,
        name: 'room'
    },
    {
        path: "/room_furniture",
        component: room_furniture,
        name: 'room_furniture'
    },
    {
        path: "/booking_line",
        component: booking_line,
        name: 'booking_line'
    },
    {
        path: "/room_price",
        component: room_price,
        name: 'room_price'
    },
    {
        path: "/bookingone",
        component: bookingone,
        name: 'bookingone'
    },
    {
        path: "/furniture",
        component: furniture,
        name: 'furniture'
    },
    {
        path: "/testone",
        component: testone,
        name: 'testone'
    },
    {
        path: "/product_model",
        component: product_model,
        name: 'product_model'
    },
    {
        path: "/pro_unittype",
        component: pro_unittype,
        name: 'pro_unittype'
    },
    {
        path: "/view_room",
        component: view_room,
        name: 'view_room'
    },

    {path: "/calendar", component: calendar, name: 'calendar'},
    {path: "/calendarView", component: calendarView, name: 'calendarView'},
    {path: "/calendarView/:dayDate/:month", component: calendarView, name: 'calendarView'},
];
export default createRouter({
    mode: "history",
    history: createWebHistory(),
    routes: routes,
});
