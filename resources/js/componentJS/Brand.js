import {
    ref,
    reactive,
    watch,
    computed,
    onMounted
} from "vue";
import Auth from '../auth/Auth';
import axios from "axios";
import moment from "moment";
import readXlsxFile from "read-excel-file";
import * as XLSX from "xlsx-js-style";
import router from "../router/router";
import { useRoute } from 'vue-router'


export default function useBrand() {
    const items = ref([]);
    const printData = ref([])
    const search = ref("");
    const checkexcel = ref([]);
    const exefile = ref("");
    const errors = ref("");
    const excel = ref("Import");
    var number_page = ref(1)
    const route = useRoute()
    var number = ref(1)
  

    const dateobj = new Date();
     const newdate = moment(dateobj.toISOString()).format('DD-MM-YYYY')

    const company = ref([]);
    const form = reactive({
        id: "",
        brand_code: "",
        brand_name: "",
        brand_name_2: "",
        inactived: "",
        created_by: "",
        updated_by: "",
    });
    const braExcels = ref([{
        brand_code: "",
        brand_name: "",
        brand_name_2: "",
        inactived: "",
    }, ]);
    onMounted(() => {
        if (Auth.check()) {
            if(!Auth.checkPermission('BRAND_ALL')){
                router.push({ name: 'App'})
            }
        }
        if(route.query.page != 1){
            number_page.value = route.query.page
            axios
            .get("api/v1/brands?page=" +number_page.value, {
                params: {
                    search: search.value,
                },
            })
            .then(({
                data
            }) => {
                items.value = data.brand;
                printData.value = data.print
                form.brand_name = search

            });
          }else{
            getBrand();
          }
          let myPagenumber = route.query
            if(JSON.stringify(myPagenumber) === '{}'){
                number.value = 1
            } 
            else {
            number.value = route.query.page
        }


       
        getcompany();
    });
    const getcompany = async () => {
        axios.get("/api/v1/getSetup/").then(({
            data
        }) => {
            company.value = data;
        });
    };
    const getBrand = async (p = 1) => {
        form.brand_name = search
        router.push({name: 'Brands', query: {page : p}})
        axios
            .get("api/v1/brands?page=" + p, {
                params: {
                    search: search.value,
                },
            })
            .then(({
                data
            }) => {
                items.value = data.brand;
                printData.value = data.print
                form.brand_name = search
                number.value = route.query.page
            });
    };
    const saveBrand = async (form) => {
        axios
            .post("api/v1/brands/store", form)
            .then(() => {})
            .catch((error) => {
                errors.value = error.response.data.errors;
                setTimeout(() => (errors.value = false), 3000);
            });
    };
    const deleteBrand = async (id) => {
        form.inactived = 'Yes'
        axios.post("api/v1/brands/update/" + id, form);
    };
    const updateBrand = async (x, form) => {
        axios.post("api/v1/brands/update/" + x, form);
    };
    const dataexcelBrand = async (event) => {
        excel.value = "Import";
        exefile.value = "";
        const xlsxfile = event.target.files ? event.target.files[0] : null;
        readXlsxFile(xlsxfile).then((rows) => {
            var x = 0;
            rows.forEach((element) => {
                if (x >= 0) {
                    if (x == 0) {
                        checkexcel.value = element;
                    } else {
                        if (
                            checkexcel.value[0] == "brand_name" &&
                            checkexcel.value[1] == "brand_name_2"
                        ) {
                            braExcels.value.push({
                                brand_name: element[0],
                                brand_name_2: element[1],
                            });
                        } else {
                            exefile.value = " Fail Data";
                        }
                    }
                }
                x++;
            });
        });
        exefile.value = "";
    };
    const savexcelBrand = async () => {
        var b = 0;
        braExcels.value.forEach((el) => {
            if (b != 0) {
                axios.post("api/v1/brands/store/", el).then((data) => {});
            }
            b++;
        });
    };
    const Import = async () => {
        excel.value = "Import";
        braExcels.value = [{}];
    };
    const exportSheet = async () => {
        excel.value = "Export";
        braExcels.value = [{}];
        axios.get("api/v1/brands/getdata/").then((res) => {
            res.data.forEach((x) => {
                braExcels.value.push({
                    brand_name: x.brand_name,
                    brand_name_2: x.brand_name_2,
                });
            });
        });
    };
    const exportData = async () => {
        var ex = braExcels.value.reduce((y, x) => {
            if (x.brand_name_2 == null || x.brand_name_2 == "") {
                x.brand_name_2 = "Null";
            }
            y.push({
                Name: x.brand_name,
                Description: x.brand_name_2,
            });
            return y;
        }, []);
        var z = ex.slice(1);
        var invoicesWS = XLSX.utils.json_to_sheet(z, {
            origin: "A10",
            raw: true,
        });
        var wb = XLSX.utils.book_new();
        XLSX.utils.sheet_add_aoa(
            invoicesWS,
            [
                ["PRODUCTS"],
                ["COMPANY NAME"],
                ["NAME : Cambo Pharma"],
                ["PHONE NUMBER : (+855)12 381215"],
                ["EMAIL ADDRESS : Cambo.pharma@gmail.com"],
                ["ADDRESS : ត្រើយកោះ ស្រុកទឹកឈូ ខេត្តកំពត"],
                ["DATE"],
                [new Date()],
            ], {
                origin: "A1",
                display: false,
                dateNF: "DD-MM-YYYY",
                cellDates: false,
            }
        );
        var wscols = [{
                width: 20,
            },
            {
                width: 20,
            },
        ];
        invoicesWS["!cols"] = wscols;

        var merge = [{
                s: {
                    c: 0,
                    r: 0,
                },
                e: {
                    c: 11,
                    r: 0,
                },
            },
            {
                s: {
                    c: 0,
                    r: 1,
                },
                e: {
                    c: 11,
                    r: 1,
                },
            },
            {
                s: {
                    c: 0,
                    r: 2,
                },
                e: {
                    c: 11,
                    r: 2,
                },
            },
            {
                s: {
                    c: 0,
                    r: 3,
                },
                e: {
                    c: 11,
                    r: 3,
                },
            },
            {
                s: {
                    c: 0,
                    r: 4,
                },
                e: {
                    c: 11,
                    r: 4,
                },
            },
            {
                s: {
                    c: 0,
                    r: 5,
                },
                e: {
                    c: 11,
                    r: 5,
                },
            },
            {
                s: {
                    c: 0,
                    r: 6,
                },
                e: {
                    c: 11,
                    r: 6,
                },
            },
            {
                s: {
                    c: 0,
                    r: 7,
                },
                e: {
                    c: 11,
                    r: 7,
                },
            },
            {
                s: {
                    c: 0,
                    r: 8,
                },
                e: {
                    c: 11,
                    r: 8,
                },
            },
        ];
        invoicesWS["!merges"] = merge;
        XLSX.utils.book_append_sheet(wb, invoicesWS, "Brand");
        var x = wb.Sheets.Brand;
        for (var row in x) {
            const style = x[row];
            try {
                style.s = {
                    alignment: {
                        horizontal: "right",
                        vertical: "right",
                        wrapText: true,
                    },
                    border: {
                        top: {
                            style: "thin",
                        },
                        right: {
                            style: "thin",
                        },
                        bottom: {
                            style: "thin",
                        },
                        left: {
                            style: "thin",
                        },
                    },
                };
            } catch (error) {}
        }
        x["A1"].s = {
            font: {
                name: "Segoe UI Variable Display Semib",
                sz: 16,
                bold: true,
                color: {
                    rgb: "326c7a",
                },
            },
            alignment: {
                horizontal: "center",
                vertical: "center",
            },
        };
        x["A2"].s = {
            font: {
                name: "Segoe UI Variable Display Semib",
                sz: 16,
                bold: true,
            },
            fill: {
                fgColor: {
                    rgb: "8DD5E8",
                },
            },
            alignment: {
                horizontal: "center",
                vertical: "center",
            },
        };
        x["A7"].s = {
            font: {
                sz: 12,
                bold: true,
            },
            alignment: {
                name: "Bahnschrift SemiLight SemiConde",
                horizontal: "center",
                vertical: "center",
            },
            fill: {
                fgColor: {
                    rgb: "8DD5E8",
                },
            },
        };
        const sheet = ["A3", "A4", "A5", "A6", "A8"];
        for (var sheets of sheet) {
            x[sheets].s = {
                font: {
                    name: "Bahnschrift SemiLight SemiConde",
                    sz: 12,
                },
                fill: {
                    fgColor: {
                        rgb: "ffffff",
                    },
                },
                alignment: {
                    horizontal: "center",
                    vertical: "center",
                },
            };
        }
        const colName = ["A10", "B10"];
        for (var itm of colName) {
            x[itm].s = {
                font: {
                    name: "Bahnschrift SemiLight SemiConde",
                    sz: 12,
                    bold: true,
                },
                fill: {
                    fgColor: {
                        rgb: "8DD5E8",
                    },
                },
                border: {
                    top: {
                        style: "thin",
                    },
                    right: {
                        style: "thin",
                    },
                    bottom: {
                        style: "thin",
                    },
                    left: {
                        style: "thin",
                    },
                },
            };
        }
        XLSX.writeFile(wb, "Brand.xlsx");
    };
    return {
        items,
        printData,
        search,
        form,
        checkexcel,
        exefile,
        excel,
        braExcels,
        company,
        newdate,
        errors,
        Import,
        getBrand,
        deleteBrand,
        saveBrand,
        updateBrand,
        dataexcelBrand,
        savexcelBrand,
        getcompany,
        exportSheet,
        exportData,
        number
    };
}
