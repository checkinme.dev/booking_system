import { ref, reactive, watch, computed, onMounted } from "vue";
import Auth from '../auth/Auth'
import axios from "axios";
import moment from "moment";
import router from "../router/router";
import readXlsxFile from "read-excel-file";
import { useRoute } from 'vue-router'
import * as XLSX from "xlsx-js-style";
export default function useCategory() {
    const Categories = ref([{}]);
    const dataPrint = ref([])
    const errors = ref("");
    const search = ref("");
    const checkexcel = ref([]);
    const company = ref([]);
    const dateobj = new Date();
    const newdate = moment(dateobj.toISOString()).format('DD-MM-YYYY')
    const exefile = ref("");
    const excel = ref("Import");
    
    const form = reactive({
        id: "",
        cat_name: "",
        cat_name_2: "",
        inactived: "",
        created_by: "",
        updated_by: "",
    });
    const CatExcels = ref([
        {
            cat_name: "",
            cat_name_2: "",
        },
    ]);
    const route = useRoute()
    var number_page = ref(1)
    var number = ref(1)

    onMounted(() => {
        if (Auth.check()) {
            if(!Auth.checkPermission('CATEGORY_ALL')){
                router.push({ name: 'App'})
            }
        }
       
        if(route.query.page != 1){
            number_page.value = route.query.page
            router.push({name: 'Category', query: {page : number_page.value}})
            axios
            .get("api/v1/category?page=" + number_page.value, {
                params: {
                    search: search.value,
                },
            })
            .then(({ data }) => {
                Categories.value = data.category;
                dataPrint.value = data.print
                form.cat_name = search;
            });
          }else{
            getCategory();
          }

    //       let myPagenumber = route.query
    //       if(JSON.stringify(myPagenumber) === '{}'){
    //           number.value = 1
    //       } 
    //       else {
    //       number.value = route.query.page
    //   }
          

        
        getcompany();
    });
    // watch(search, () => {
    //     getCategory();
    // });
    var getCategory = async (p = 1) => {
        router.push({name: 'Category', query: {page : p}})
        axios
            .get("api/v1/category?page=" + p, {
                params: {
                    search: search.value,
                },
            })
            .then(({ data }) => {
                Categories.value = data.category;
                dataPrint.value = data.print
                form.cat_name = search;
                // number.value = route.query.page
                // console.log(number.value)
                let myPagenumber = route.query
                if(JSON.stringify(myPagenumber) === '{}'){
                    number.value = 1
                } 
                else {
                number.value = route.query.page
            }
            });
    };
    const createCategory = async (form) => {
        axios
            .post("api/v1/categorys/create", form)
            .then(() => {})
            .catch((error) => {
                errors.value = error.response.data.errors;
                setTimeout(() => (errors.value = false), 2000);
            });
    };
    const DeleteCategory = async (x, form) => {
        form.inactived = "Yes";
        axios.post("api/v1/categorys/updete/" + x, form);
    };
    const updateCategory = async (x, form) => {
        axios.post("api/v1/categorys/updete/" + x, form);
    };
    const printCategory = async () => {
        var printContents = document.getElementById("purchaseorder").innerHTML;
        var mywindow;
        mywindow = window.open(
            "",
            "mydiv",
            "height=800,width=1200,scrollbars=yes",
            ""
        );
        mywindow.document.write("<html><head><title>my div</title>");
        mywindow.document.write('<link rel="stylesheet" href="css/style.css">');
        mywindow.document.write('<link rel="stylesheet" href="css/app.css">');
        mywindow.document.write(
            '<link rel="stylesheet" href="css/pstyle.css">'
        );
        mywindow.document.write("</head><body>");
        mywindow.document.write(printContents);
        mywindow.document.write("</body> </html>");
        setTimeout(() => {
            mywindow.document.close();
            mywindow.focus();
            mywindow.print();
            setTimeout(function () {
                window.close();
            }, 1);
            cb();
        }, 1000);
    };
    const dataexcelCategory = async (event) => {
        exefile.value = "";
        const xlsxfile = event.target.files ? event.target.files[0] : null;
        readXlsxFile(xlsxfile).then((rows) => {
            var x = 0;
            rows.forEach((element) => {
                if (x >= 0) {
                    if (x == 0) {
                        checkexcel.value = element;
                    } else {
                        if (
                            checkexcel.value[0] == "cat_name" &&
                            checkexcel.value[1] == "cat_name_2"
                        ) {
                            CatExcels.value.push({
                                cat_name: element[0],
                                cat_name_2: element[1],
                            });
                        } else {
                            exefile.value = " Fail Data";
                        }
                    }
                }
                x++;
            });
        });
        exefile.value = "";
    };
    const savexcelCategoy = async () => {
        var x = 0;
        CatExcels.value.forEach((e) => {
            if (x != 0) {
                axios.post("api/v1/category/excel", e).then((data) => {});
            }
            x++;
        });
    };
    const getcompany = async () => {
        axios.get("/api/v1/getSetup/").then((response) => {
            company.value = response.data;
            dateformart.value = moment(new Date()).format("YYYY-MM-DD");
        });
    };
    const Import = async () => {
        excel.value = "Import";
        CatExcels.value = [{}];
    };
    const exportSheet = async () => {
        excel.value = "Export";
        CatExcels.value = [{}];
        axios.get("api/v1/category/getCatName").then((res) => {
            res.data.forEach((z) => {
                CatExcels.value.push({
                    cat_name: z.cat_name,
                    cat_name_2: z.cat_name_2,
                });
            });
        });
    };
    const exportData = async () => {
        var ex = CatExcels.value.reduce((y, x) => {
            if (x.cat_name_2 == null || x.cat_name_2 == "") {
                x.cat_name_2 = "Null";
            }
            y.push({
                Name: x.cat_name,
                Description: x.cat_name_2,
            });
            return y;
        }, []);
        var z = ex.slice(1);
        var invoicesWS = XLSX.utils.json_to_sheet(z, {
            origin: "A10",
            raw: true,
        });
        var wb = XLSX.utils.book_new();
        XLSX.utils.sheet_add_aoa(
            invoicesWS,
            [
                ["PRODUCTS"],
                ["COMPANY NAME"],
                ["NAME : Cambo Pharma"],
                ["PHONE NUMBER : (+855)12 381215"],
                ["EMAIL ADDRESS : Cambo.pharma@gmail.com"],
                ["ADDRESS : ត្រើយកោះ ស្រុកទឹកឈូ ខេត្តកំពត"],
                ["DATE"],
                [new Date()],
            ],
            {
                origin: "A1",
                display: false,
                dateNF: "DD-MM-YYYY",
                cellDates: false,
            }
        );
        var wscols = [
            {
                width: 20,
            },
            {
                width: 20,
            },
        ];
        invoicesWS["!cols"] = wscols;

        var merge = [
            {
                s: {
                    c: 0,
                    r: 0,
                },
                e: {
                    c: 11,
                    r: 0,
                },
            },
            {
                s: {
                    c: 0,
                    r: 1,
                },
                e: {
                    c: 11,
                    r: 1,
                },
            },
            {
                s: {
                    c: 0,
                    r: 2,
                },
                e: {
                    c: 11,
                    r: 2,
                },
            },
            {
                s: {
                    c: 0,
                    r: 3,
                },
                e: {
                    c: 11,
                    r: 3,
                },
            },
            {
                s: {
                    c: 0,
                    r: 4,
                },
                e: {
                    c: 11,
                    r: 4,
                },
            },
            {
                s: {
                    c: 0,
                    r: 5,
                },
                e: {
                    c: 11,
                    r: 5,
                },
            },
            {
                s: {
                    c: 0,
                    r: 6,
                },
                e: {
                    c: 11,
                    r: 6,
                },
            },
            {
                s: {
                    c: 0,
                    r: 7,
                },
                e: {
                    c: 11,
                    r: 7,
                },
            },
            {
                s: {
                    c: 0,
                    r: 8,
                },
                e: {
                    c: 11,
                    r: 8,
                },
            },
        ];
        invoicesWS["!merges"] = merge;
        XLSX.utils.book_append_sheet(wb, invoicesWS, "Brand");
        var x = wb.Sheets.Brand;
        for (var row in x) {
            const style = x[row];
            try {
                style.s = {
                    alignment: {
                        horizontal: "right",
                        vertical: "right",
                        wrapText: true,
                    },
                    border: {
                        top: {
                            style: "thin",
                        },
                        right: {
                            style: "thin",
                        },
                        bottom: {
                            style: "thin",
                        },
                        left: {
                            style: "thin",
                        },
                    },
                };
            } catch (error) {}
        }
        x["A1"].s = {
            font: {
                name: "Segoe UI Variable Display Semib",
                sz: 16,
                bold: true,
                color: {
                    rgb: "326c7a",
                },
            },
            alignment: {
                horizontal: "center",
                vertical: "center",
            },
        };
        x["A2"].s = {
            font: {
                name: "Segoe UI Variable Display Semib",
                sz: 16,
                bold: true,
            },
            fill: {
                fgColor: {
                    rgb: "8DD5E8",
                },
            },
            alignment: {
                horizontal: "center",
                vertical: "center",
            },
        };
        x["A7"].s = {
            font: {
                sz: 12,
                bold: true,
            },
            alignment: {
                name: "Bahnschrift SemiLight SemiConde",
                horizontal: "center",
                vertical: "center",
            },
            fill: {
                fgColor: {
                    rgb: "8DD5E8",
                },
            },
        };
        const sheet = ["A3", "A4", "A5", "A6", "A8"];
        for (var sheets of sheet) {
            x[sheets].s = {
                font: {
                    name: "Bahnschrift SemiLight SemiConde",
                    sz: 12,
                },
                fill: {
                    fgColor: {
                        rgb: "ffffff",
                    },
                },
                alignment: {
                    horizontal: "center",
                    vertical: "center",
                },
            };
        }
        const colName = ["A10", "B10"];
        for (var itm of colName) {
            x[itm].s = {
                font: {
                    name: "Bahnschrift SemiLight SemiConde",
                    sz: 12,
                    bold: true,
                },
                fill: {
                    fgColor: {
                        rgb: "8DD5E8",
                    },
                },
                border: {
                    top: {
                        style: "thin",
                    },
                    right: {
                        style: "thin",
                    },
                    bottom: {
                        style: "thin",
                    },
                    left: {
                        style: "thin",
                    },
                },
            };
        }
        XLSX.writeFile(wb, "Brand.xlsx");
    };
    return {
        Categories,
        form,
        dataPrint,
        search,
        CatExcels,
        checkexcel,
        exefile,
        excel,
        company,
        newdate,
        number,
        errors,
        getcompany,
        getCategory,
        createCategory,
        DeleteCategory,
        updateCategory,
        printCategory,
        dataexcelCategory,
        savexcelCategoy,
        Import,
        exportSheet,
        exportData,
    };
}
