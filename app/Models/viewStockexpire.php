<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class viewStockexpire extends Model
{
    use HasFactory;
    protected $table = 'stock_onhand_exprit';
    protected $fillable = [
        'product_no',
        'description',
        'reorder_point',
        'amc_inventery',
        'amc_amount',
        'mos',
        'stock_uint',
        'unit_price',
        'stock_onhand',
        'stock_aounmt',
        'curency_code',
        'countday_exprit',
        'exprit_date',
        'created_at',
    ];
}
