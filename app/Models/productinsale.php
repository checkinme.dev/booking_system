<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productinsale extends Model
{
    use HasFactory;
    protected $table = "product_in_sale";
    protected $fillable = [
        'id',
        'document_no',
        'stock_id',
        'product_no',
        'description',
        'unit_code',
        'inventory',
        'per_unit',
        'boom_product_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
        
    ];
}
