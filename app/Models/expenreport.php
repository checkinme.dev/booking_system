<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class expenreport extends Model
{
    use HasFactory;
    protected $table = "expenreport";
    protected $fillable = [
        'id',
        'document_no',
        'document',
        'description',
        'Paymeny_method',
        'totat_exspan',
        'exspan_date',
        'statue',
        'created_by',
        'updated_by',
        'user_name',
        'created_at',
        'updated_at',
    ];
}
