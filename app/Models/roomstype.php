<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class roomstype extends Model
{
    use HasFactory;
    protected $table = 'room_type';
    protected $fillable = [
        'id',
        'roomtype',
        'description',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'   
    ];
}
