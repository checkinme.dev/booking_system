<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laboratory extends Model
{
    use HasFactory;
    protected $table = "labo_lab_list";
    protected $fillable = [
        'id',
        'labId',
        'product_no',
        'product_type',
        'description',
        'test_type',
        'normal_value',
        'test_value',
        'status',
        'stock_unit_of_measure_code',
        'unit_price',
        'remark',
        'created_by',
        'updated_by'
    ];
}
