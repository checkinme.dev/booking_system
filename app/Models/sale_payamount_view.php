<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sale_payamount_view extends Model
{
    use HasFactory;
    protected $table = "sale_payamount_view";
    protected $fillable = [
        'id',
        'document_no',
        'document_type',
        'closeshift_id',
        'description',
        'amoun',
        'total_amount_usd',
        'total_amount',
        'paymeny_method',
        'change_amount',
        'change_amount_usd',
        'total_receipts',
        'total_receipt_usd',
        'exchane_rate',
        'type',
        'discount',
        'curency_code',
        'statue',
        'created_by',
        'updated_by'

    ];
}
