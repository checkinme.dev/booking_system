<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class viewprodoctstock extends Model
{
    use HasFactory;
    protected $table ="viewprodoctstock";
    protected $fillable = [
        'id',
        'product_no',
        'product_barcode',
        'description',
        'description_2',
        'inventory',
        'image_url',
        'type',
        'stock_unit_of_measure_code',
        'purche_unit_of_measure_code',
        'bom_no',
        'reorder_point',
        'sup_code',
        'brand_code',
        'group_code',
        'cat_code',
        'variant_code',
        'curency_code',
        'unit_price',
        'inactived',
        'is_deleted',
        'created_by',
        'updete_by',
        'delete_by',
        'created_at',
        'updated_at'
    ];
}
