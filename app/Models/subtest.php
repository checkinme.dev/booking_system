<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subtest extends Model
{
    use HasFactory;
    protected $table = "subtest";
    protected $fillable = [
        'id',
        'product_no',
        'sub_product_no',
        'description',
        'stock_unit_of_measure_code',
        'created_by',
        'updated_by'
    ];
}
