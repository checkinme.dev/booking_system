<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pos_closeshift extends Model
{
    use HasFactory;
    protected $table = "pos_closeshift_view";
    protected $fillable = [
        'id',
        'deposit',
        'deposit2',
        'description',
        'type',
        'total_amount',
        'total_amount_usd',
        'created_by'
    ];
}
