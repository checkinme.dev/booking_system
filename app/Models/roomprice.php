<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class roomprice extends Model
{
    use HasFactory;
    protected $table = 'room_price';
    protected $fillable = [
        'id',
        'room_no',
        'desctiption',
        'type',
        'qty',
        'price',
        'status',
        'created_by',
        'updated_by'
    ];

}
