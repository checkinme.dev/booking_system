<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class active_tread extends Model
{
    use HasFactory;
    protected $table = "active_tread";
    protected $fillable = [
        'VARIABLE_NAME',
        'VARIABLE_VALUE'
    ];
}
