<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class diagnosis_list extends Model
{
    use HasFactory;
    protected $table ="diagnosis_list";
    protected $fillable = [
    'preid',
    'product_no',
    'description',
    'dose',
    'morning',
    'afternoon',
    'evening',
    'night',
    'day',
    'qty',
    'unit_price',
    'unit',
    'amount',
    'created_by',
    'updated_by'
    ];
}
