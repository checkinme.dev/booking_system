<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class closeshift_view extends Model
{
    use HasFactory;
    protected $table ="closeshift_view";
    protected $fillable = [
        'id',
        'user_id',
        'user_code',
        'user_name',
        'deposit',
        'deposit2',
        'exchane_rate',
        'total_amount',
        'total_amountusd',
        'description',
        'created_at',
    ];
}
