<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class income_report_view extends Model
{
    use HasFactory;
    protected $table = "income_report_view";
    protected $fillable = [
        'id',
        'document_no',
        'product_no',
        'description',
        'unit_price',
        'inventory',
        'boom_product_id',
        'per_unit',
        'created_by',
        'stock_id',
        'created_at',
        'buy_price',
        'paymeny_method',
        'type',
        'sel_price'
    ];
}
