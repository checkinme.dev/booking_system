<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class expenses_type extends Model
{
    use HasFactory;
    protected $table ="expenses_type";
    protected $fillable = [
        'id',
        'expenses_name',
        'description',
        'inactived',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
}
