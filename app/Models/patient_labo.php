<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class patient_labo extends Model
{
    use HasFactory;
    protected $table = 'patient_labo_view';
    protected $fillable = [
        'pateinid',
        'labId',
        'firstname',
        'lstname',
        'allergies',
        'sex',
        'age',
        'history_disease',
        'address',
        'address2',
        'village',
        'commune',
        'district',
        'phone1',
        'pro_city',
        'Date',
        'diagnosis',
        'BP',
        'Pr',
        'rr',
        'Spo2',
        'T',
        'remark',
        'status',
        'created_by',
        'updated_by'
    ];
}
