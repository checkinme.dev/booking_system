<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class amc extends Model
{
    use HasFactory;
     protected $table = "amc_view";
     protected $fillable = [
        'product_no',
        'description',
        'amc_inventery',
        'amc_sale_total',
    ];
}