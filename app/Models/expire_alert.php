<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class expire_alert extends Model
{
    use HasFactory;
    protected $table ="exprit_alert";
    protected $fillable = [
        'id',
        'description',
        'document_no',
        'reorder_point',
        'amc_inventery',
        'amc_amount',
        'mos',
        'stock_uint',
        'unit_price',
        'stock_onhand',
        'stock_aoumt',
        'curency_code',
        'countday_exprit',
        'created_at',
        
    ];

}
