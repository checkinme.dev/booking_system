<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class payment_receive_view extends Model
{
    use HasFactory;
    protected $table = "payment_receive_view";
    protected $fillable = [
        'id',
        'document_no',
        'document',
        'document_type',
        'decription',
        'totale_balanec',
        'exchane_rate',
        'statue',
        'currency_code',
        'paymant_amount',
        'sub_code',
        'created_at',
        'total_amount'
    ];
}
