<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class furnitureroom_view extends Model
{
    use HasFactory;
    protected $table = "furnitureroom_view";
    protected $fillable = [
        'id',
        'room_no',
        'furniture_no',
        'qty',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'furniture',
        'description',
        'image'
    ];
}
