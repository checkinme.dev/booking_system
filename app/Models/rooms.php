<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rooms extends Model
{
    use HasFactory;
    protected $table = 'room';
    protected $fillable = [
        'id',
        'roomtype_no',
        'room_no',
        'size',
        'image',
        'room_name',
        'description',
        'currency',
        'room_price',
        'status',
        'adults',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}

