<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class labolab extends Model
{
    use HasFactory;
    protected $table = 'labo_lab';
    protected $fillable = [
        'id',
        'labId',
        'pateinId',
        'Age',
        'diagnosis',
        'Date',
        'BP',
        'Pr',
        'rr',
        'Spo2',
        'T',
        'status',
        'remark',
        'created_by',
        'updated_by'
    ];
}
