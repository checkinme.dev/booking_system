<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product_services extends Model
{
    use HasFactory;
    protected $table = "product_services";
    protected $fillable = [
        'id',
        'product_no',
        'description',
        'test_type',
        'group_code',
        'normal_value',
        'unit_price',
        'status'
    ];
}