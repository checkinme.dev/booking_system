<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\roomprice;

class RoompriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showroomprice()
    {
        $roomprice = roomprice::get();
        return $roomprice;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createroomprice(Request $request)
    {
        $requestData = $request->all();
     
        $validator = Validator::make( $requestData , [
            'room_no'   => 'required',
            'type'       => 'required',
            'unit'     => 'required',
            'price'   => 'required',
            
            
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $roomprice = roomprice::create([

            'room_no'    => $request->room_no,
            'type'       => $request->type,
            'unit'       => $request->unit,
            'price'      => $request->price,
            
        ]);
        if($roomprice){
             $roomprice = roomprice::get();
             return $roomprice;
        }
        else{
            return 'false';
        }
    }


    public function updateroomprice($id, Request $request){
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'room_no'       => 'required',
            'type'          => 'required',
            'unit'          => 'required',
            'price'         => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $data = roomprice::find($id);
        if($data != null){
            $data->room_no      = $request->room_no;
            $data->type         = $request->type;
            $data->unit         = $request->unit;
            $data->price        = $request->price;
            $data->save();
            
            if($data){
                return "Update successfully";
            }
        }else {
            return "No data to update";
        }
    }


    public function deleteroomprice($id){
        $data = roomprice::find($id);
        if($data != null){
            $data->delete();
            return "Deleted successfully";
        }else {
            return "No data to delete";
        }
    }


    public function searchroomprice(Request $request)
    {
        //return $request->all();
       // return $request->name;
        $roomprice = roomprice::where("room_no", 'LIKE', "%$request->room_no%")
                        ->where("type", 'LIKE', "%$request->type%")
                        ->where("unit", 'LIKE', "%$request->unit%")
                        ->where("price", 'LIKE', "%$request->price%")
                        ->get();
        if ($roomprice) {
            return $roomprice;
        } else {
            return ['statue :' => "Note Date"];
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
