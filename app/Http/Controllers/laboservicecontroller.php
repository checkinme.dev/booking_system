<?php

namespace App\Http\Controllers;

use App\Models\employee;
use App\Models\labolab;
use App\Models\product;
use App\Models\product_services;
use App\Models\User;
use Illuminate\Http\Request;

class laboservicecontroller extends Controller
{
    public function SearchTestService($id,Request $request){
        if($request->group != '' && $request->name == ''){
            $result = product_services::where('test_type','=',$id)
                                        ->where('group_code', 'LIKE','%'.$request->group.'%')->get();
                return $result;
        }
        if($request->name != '' && $request->group == ''){
            $result = product_services::where('test_type','=',$id)
                                        ->where('description','LIKE','%'.$request->name.'%')
                                        ->get();
            return $result;
        }
        if($request->name != '' && $request->group != ''){
            $result = product_services::where('test_type','=',$id)
                                        ->where('group_code','LIKE','%'.$request->group.'%')
                                        ->where('description','LIKE','%'.$request->name.'%')
                                        ->get();
            return $result;
        }
    }
    public function getDoctorName(Request $request){
        $pres =  labolab::where('labId','=',$request->preid)->first();
        if($pres){
            
            $user = User::where('user_code','=',$pres->created_by)->first();
            $employee = employee::where('id','=',$user->user_code)->first();
            $name = $employee->lstname.' '.$employee->firstname;

            return response()->json([
                'doctor' => $name
            ]);
        }else{
            return response()->json([
                'message' => 'No data to return'
            ]);
        }
        
    }
    public function get($id){
        $diagnosis = product_services::where('test_type','=',$id)->get();
        if($diagnosis){
            return $diagnosis;
        }
        else{
            return "failed";
        }
    }

    
}
