<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\sale_payamount_view;
use Carbon\Carbon;
use App\Models\productinsale;
use App\Models\stockkeeping;
use App\Models\Saleline;
use App\Models\income_report_view;
use App\Models\Sale;
use App\Models\exchangerate;
use App\Models\expenses_type;
use App\Models\expenses;
use App\Models\startstock;
use App\Models\payment_receive_view;


class ReportController extends Controller
{
    public function index(){
        $data = sale_payamount_view::get();
        return count($data);

    }

    public function getSaleProduct(Request $request){
        // $object = array();

        $percentage = 0;

        $inventory = 0;
        $inventory_before = 0;
        $increment_inventory  = 0;

        $sum_amount = 0;
        $sum_amount_before = 0;

        $increment_amount = 0;

        if($request->type == 'saleproduct' && $request->created_by != ''){
                 $increment = 0;

                 $datalast30day = sale_payamount_view::whereDate('created_at','>=',$request->startDate)
                                                     ->whereDate('created_at','<=',$request->endDate)
                                                     ->where('type','=','pos')
                                                     ->where('statue','=', 'open')
                                                     ->get();
                 if(count($datalast30day) != 0){
                     foreach ($datalast30day as $el) {
                         $sum_amount += floatval($el->total_amount);

                         $subElement = productinsale::where('document_no','=',$el->document_no)->get();
                         if(count($subElement) != 0){
                             foreach ($subElement as $subEl) {
                                 $inventory += intval($subEl->inventory);
                             }
                         }
                     }
                 }
                 //get data before last 30 days in 30 days
                 $beforeThisMonth = sale_payamount_view::whereDate('created_at','>=',$request->thirdDate)
                                                         ->whereDate('created_at', '<', $request->startDate)
                                                         ->where('type','=','pos')
                                                         ->where('statue','=', 'open')
                                                         ->get();
                 if(count($beforeThisMonth) != 0){
                     foreach ($beforeThisMonth as $el) {
                         $sum_amount_before += floatval($el->total_amount);

                         $subElement = productinsale::where('document_no','=',$el->document_no)->get();
                         if(count($subElement) != 0){
                             foreach ($subElement as $subEl) {
                                 $inventory_before += intval($subEl->inventory);
                             }
                         }
                     }
                 }
                 if($sum_amount_before == 0 && $sum_amount == 0){
                     $percentage = 0;
                 }
                 else if($sum_amount_before == 0 && $sum_amount != 0){
                     $percentage = 100;
                 }
                 else if($sum_amount_before !=0 && $sum_amount == 0){
                     $percentage = -100;
                 }
                 else if($sum_amount != 0 && $sum_amount_before != 0){
                     $percentage = (($sum_amount - $sum_amount_before) / $sum_amount_before ) * 100;
                 }


                 $increment_amount = $sum_amount - $sum_amount_before;
                 $increment_inventory = $inventory - $inventory_before;

                 return ['data'=> $datalast30day,
                 'total'=> $sum_amount,'item_this_month'=>$inventory,
                 'total_before'=>$sum_amount_before,
                 'inventory_before'=>$inventory_before,
                 'percentage'=>$percentage,
                 'increment_amount' => $increment_amount,
                 'increment_inven' =>$increment_inventory
             ];
        }
        if($request->type == 'saleservice' && $request->created_by != ''){
         $total_serve = 0;
         $total_serve_last = 0;

         $this_months = sale_payamount_view::whereDate('created_at','>=',$request->startDate)
                                            ->whereDate('created_at','<=',$request->endDate)
                                            ->where('type','!=','pos')
                                            ->get();

        $last_months = sale_payamount_view::whereDate('created_at','>=',$request->thirdDate)
                                            ->whereDate('created_at', '<', $request->startDate)
                                            ->where('type','!=','pos')
                                            ->get();

        if(count($this_months) > 0){
            foreach ($this_months as $el) {
                $sum_amount += floatval($el->total_amount);
                $subelement = Saleline::where('document_no','=',$el->document_no)->get();
                $total_serve += count($subelement);
            }
        }
        if(count($last_months) > 0){
            foreach ($last_months as $key) {
                $sum_amount_before += floatval($key->total_amount);
                $subEl = Saleline::where('document_no','=',$key->document_no)->get();
                $total_serve_last += count($subEl);
            }
        }
        if($sum_amount_before == 0 && $sum_amount == 0){
            $percentage = 0;
        }
        else if($sum_amount_before == 0 && $sum_amount != 0){
            $percentage = 100;
            $increment_amount = $sum_amount - $sum_amount_before;
        }else if($sum_amount == 0 && $sum_amount_before != 0){
            $percentage = -100;
            $increment_amount  = $sum_amount - $sum_amount_before;
        }
        else if($sum_amount !=0 && $sum_amount_before != 0){
            $increment_amount = $sum_amount - $sum_amount_before;
            $percentage = ($increment_amount / $sum_amount_before) * 100;
        }
        $increment_inventory = $total_serve - $total_serve_last;


        return [
        'total'=>$sum_amount,
        'last month'=>$last_months,
        'percentage'=>$percentage,
        'increment_amount'=>$increment_amount,
        'increment_inven'=>$increment_inventory,
        'item_this_month' => $total_serve,
        'number of service last month'=>$total_serve_last
        ];
    }

}



    public function dataCountDay(){
        $startDate = Carbon::parse("2022-8-9");
        $endDate = Carbon::parse("2022-8-11");

        $diffInDays = $startDate->diffInDays($endDate);
        return $diffInDays;

    }

    public function productReport(Request $request){

        if($request->created_by != null && $request->created_by != ''){
            $buy_inventory = 0;
            $sale_inventory = 0;
            $stock_remain = 0;

            $buy_inventory_last = 0;
            $sale_inventory_last = 0;
            $stock_remain_last = 0;
            $percentage = 0;

            $thirdDate = $request->thirdDate;

            $stockeeping = stockkeeping::whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)->get();
            if(count($stockeeping) != 0){
                foreach ($stockeeping as $el) {
                    $buy_inventory += floatval($el->inventory_new);
                }
            }

            $stockkeeping_last = stockkeeping::whereDate('created_at','>=',$request->thirdDate)
                                            ->whereDate('created_at','<',$request->startDate)
                                                ->get();
            if(count($stockkeeping_last) != 0){
                foreach ($stockkeeping_last as $el) {
                    $buy_inventory_last += floatval($el->inventory_new);
                }
            }


            //product in sale
            $pro_in_sale = productinsale::whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)->get();
            if(count($pro_in_sale) != 0){
                foreach ($pro_in_sale as $element) {
                    $sale_inventory += floatval($element->inventory);
                }
            }

            $pro_in_sale_last = productinsale::whereDate('created_at','>=',$request->thirdDate)
                                            ->whereDate('created_at','<',$request->startDate)
                                            ->get();
            if(count($pro_in_sale_last) != 0){
                foreach ($pro_in_sale_last as $key) {
                    $sale_inventory_last += floatval($key->inventory);
                }
            }

            $stock_remain = $buy_inventory - $sale_inventory;
            $stock_remain_last = $buy_inventory_last - $sale_inventory_last;


            if($stock_remain == 0 && $stock_remain_last == 0 ){
                $percentage = 0;
            }
            else if($stock_remain != 0 && $stock_remain_last == 0){
                $percentage = 100;
            }
            else if($stock_remain != 0 && $stock_remain_last != 0){
                $percentage = (($stock_remain - $stock_remain_last) / $stock_remain_last) * 100;
            }


            return ['stockkeeping'=>$stockeeping,
            'buy_invent'=>$buy_inventory,
            'sale_invent'=>$sale_inventory,
            'remain'=>$stock_remain,
            'remain_last'=>$stock_remain_last,
            'percentage'=>$percentage
        ];
        }else{
            return "Cann't visit my api";
        }


    }






    public function incomeReport(Request $request){

        $pos = array();
        $prescription = array();
        $labo = array();

        $pos_object = array();
        $pre_object = array();
        $labo_object = array();

        $pos_total_amount = 0;
        $pos_total_income = 0;
        $pos_inventory = 0;
        $pos_buy_price = 0;

        $pre_total_amount = 0;
        $pre_total_income = 0;
        $pre_inventory = 0;
        $pre_buy_price = 0;

        $pre_total_amount = 0;
        $labo_total_amount = 0;

        $total_medicine_sale = 0;
        $total_medicine_buy = 0;
        $total_medicine_income = 0;

        $total_buy = 0;
        $total_sell =0;
        $total_income = 0;



        if($request->created_by != null){

            if($request->startDate != null && $request->endDate != null){

                //find pos sale//
                $pos_data = income_report_view::whereDate('created_at', '>=',$request->startDate)
                                                ->whereDate('created_at', '<=', $request->endDate)
                                                ->where('type','=','pos')
                                                ->get()->groupBy('paymeny_method');
                if(count($pos_data) > 0){
                    foreach ($pos_data as $pos_data_el) {

                      $income = 0;
                      $sel_price = 0;
                      $buy_price = 0;
                      $inventory = 0;
                      if(count($pos_data_el) > 0){
                        foreach ($pos_data_el as $key) {
                            $sel_price += floatval($key->inventory)* floatval($key->sel_price);
                            $buy_price += floatval($key->inventory) * floatval($key->buy_price);
                            $inventory += floatval($key->inventory);
                            $income = $sel_price - $buy_price ;
                        }
                      }
                      $pos_total_amount += $sel_price;
                      $pos_total_income += $income;
                      $pos_inventory += $inventory;
                      $pos_buy_price += $buy_price;

                      array_push($pos_object, array("method"=> $pos_data_el[0]->paymeny_method,'description'=>"Sale Medicine in POS",'inventory'=>$inventory, 'sel_total'=>$sel_price, 'buy_price'=> $buy_price,'income'=>$income));

                    }

                    array_push($pos, array('description'=>"Sale Medicine in POS",'qty'=>$pos_inventory,'sel_price'=>$pos_total_amount,'buy_price'=>$pos_buy_price,'method'=>"All",'total'=>$pos_total_amount,'income'=>$pos_total_income,'items'=>$pos_object, ));
                }
                //end //

                //find prescription //
                $pre_data = income_report_view::whereDate('created_at', '>=',$request->startDate)
                                                ->whereDate('created_at', '<=', $request->endDate)
                                                ->where('type','=','prescription')
                                                ->get()->groupBy('paymeny_method');

                if(count($pre_data) > 0){

                    foreach ($pre_data as $pre_data_el) {
                        $preincome = 0;
                        $presel_price = 0;
                        $prebuy_price = 0;
                        $preinventory = 0;
                        if(count($pre_data_el) > 0){
                          foreach ($pre_data_el as $el) {
                              $presel_price += floatval($el->inventory)* floatval($el->sel_price);
                              $prebuy_price += floatval($el->inventory) * floatval($el->buy_price);
                              $preinventory += floatval($el->inventory);
                              $preincome = $presel_price - $prebuy_price ;
                          }
                        }
                        $pre_total_amount += $presel_price;
                        $pre_total_income += $preincome;
                        $pre_inventory += $preinventory;
                        $pre_buy_price += $prebuy_price;

                        array_push($pre_object, array("method"=> $pre_data_el[0]->paymeny_method,'description'=>"Sale Medicine in Prescription",'inventory'=>$preinventory, 'sel_total'=>$presel_price, 'buy_price'=> $prebuy_price,'income'=>$preincome));

                      }

                      array_push($prescription, array('description'=>"Sale Medicine in Prescription",'qty'=>$pre_inventory,'sel_price'=>$pre_total_amount,'buy_price'=>$pre_buy_price,'method'=>"All",'total'=>$pre_total_amount,'income'=>$pre_total_income,'items'=>$pre_object, ));
                }

                $total_medicine_buy = $pos_buy_price + $pre_buy_price;
                $total_medicine_sale = $pos_total_amount + $pre_total_amount;

                $total_medicine_income = $pos_total_income + $pre_total_income;

                //Find laboratory
                $labo_data = Sale::whereDate('created_at', '>=',$request->startDate)
                                ->whereDate('created_at', '<=', $request->endDate)
                                ->where('type','=','pos')
                                ->get()->groupBy('paymeny_method');
                $labo_inventory = 0;

                if(count($labo_data) > 0){

                    foreach ($labo_data as $labo_data_el) {
                        $laboincome = 0;
                        $labosel_price = 0;

                        $preinventory = 0;
                        if(count($labo_data_el) > 0){
                            $labo_inventory_el = 0;
                        foreach ($labo_data_el as $el) {
                            $sub_labo = Saleline::where('document_no','=', $el->document_no)->get();
                            $labo_inventory_el += count($sub_labo);
                            if(count($sub_labo) > 0){
                                foreach ($sub_labo as $sub_el) {
                                    $labosel_price += floatval($sub_el->unit_price);
                                }
                            }
                            }
                        }

                        $labo_inventory += $labo_inventory_el;
                        $labo_total_amount += $labosel_price;

                        array_push($labo_object, array("method"=> $labo_data_el[0]->paymeny_method,'description'=>"Sale Test in Labo",'inventory'=>$labo_inventory_el, 'sel_total'=>$labosel_price, 'buy_price'=> null,'income'=>$labosel_price));

                    }
                    array_push($labo, array('description'=>"Sale Test in Labo",'qty'=>$labo_inventory,'sel_price'=>$labo_total_amount,'buy_price'=>null,'method'=>"All",'total'=>$labo_total_amount,'income'=>$labo_total_amount,'items'=>$labo_object ));

                }

                $total_buy = $total_medicine_buy + 0;
                $total_sell = $total_medicine_sale + $labo_total_amount;
                $total_income = $total_medicine_income + $labo_total_amount;

                $total_buy_usd = 0;
                $total_sell_usd =0;
                $total_income_usd = 0;

                $exchange = exchangerate::first();

                $total_buy_usd = $total_buy / floatval($exchange->exchange_rate);
                $total_sell_usd = $total_sell / floatval($exchange->exchange_rate);
                $total_income_usd = $total_income / floatval($exchange->exchange_rate);



                return ['pos'=>$pos,
                'prescription'=>$prescription,
                'product_buy'=>$total_medicine_buy,
                'product_sell'=>$total_medicine_sale,
                'product_income'=>$total_medicine_income,
                'labo'=> $labo,
                'labo_sell'=>$labo_total_amount,
                'labo_income'=>$labo_total_amount,
                'labo_buy'=>null,
                'total_buy'=> $total_buy,
                'total_sell'=>$total_sell,
                'total_income'=>$total_income,
                'total_buy_usd'=> $total_buy_usd,
                'total_sell_usd'=>$total_sell_usd,
                'total_income_usd'=>$total_income_usd,



            ];

            }else {
                return "Can't visit my api.";
            }
        }else{
            return "No rights to visit my api";
        }




    }
    public function addExpenseType(Request $request){

        if($request->created_by != null){
            $data = expenses_type::create([
                'expenses_name'=>$request->description,
                'description'=>$request->description,
                'inactived'=>$request->inactived,
                'created_by' => $request->created_by,

            ]);
            if($data){
                return "Created successfully";
            }
        }else {
            return "Can't create expense";
        }

    }
    public function updateExpense($id, Request $request){
        $data = expenses_type::find($id);
        if($data != null){
            $data->expenses_name = $request->description;
            $data->description = $request->description;
            $data->inactived = $request->inactived;
            $data->created_by = $request->created_by;
            $data->save();

            if($data){
                return "Update successfully";
            }
        }else {
            return "No data to update";
        }
    }
    public function delete($id){
        $data = expenses_type::find($id);
        if($data != null){
            $data->delete();
            return "Deleted successfully";
        }else {
            return "No data to delete";
        }
    }

    public function getExpense(Request $request){
        //expense
        $expense_gerneral = array();
        $expanse_object = array();

        $total_expanse_riel = 0;
        $total_expanse_usd = 0;
        //start stock
        $start_stock = array();
        $startStock_object = array();
        $total_start_riel = 0;
        $total_start_usd = 0;
        // purchase order
        $purchase = array();
        $total_purchase_riel =0;
        $total_purchase_usd =0;

        $all_total = 0;


        if($request->created_by != null){
            if($request->startDate != null && $request->endDate != null){
                $exchange = exchangerate::where('curency_no','=','USD')->first();
                $rate = $exchange->exchange_rate;

                $data = expenses::whereDate('exspan_date','>=', $request->startDate)
                                ->whereDate('exspan_date','<=',$request->endDate)
                                ->get()->groupBy('document');

                if(count($data) > 0){
                    foreach ($data as $el) {
                        $total = 0;
                        if(count($el) > 0){
                            foreach ($el as $subEl) {
                                $total += floatval($subEl->totat_exspan);
                            }
                        }
                        $total_expanse_riel += $total;
                        $total_expanse_usd = $total_expanse_riel / floatval($rate);
                        array_push($expense_gerneral, array('expense_type'=>$el[0]->document, 'total'=>$total, 'method'=>"All"));
                    }
                    array_push($expanse_object, array('description'=>"All general expenses",'method'=>"All",'total_riel'=>$total_expanse_riel, 'total_usd'=>$total_expanse_usd,"items"=> $expense_gerneral));

                }

                //start stock
                $data_start = startstock::whereDate('created_at','>=', $request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        ->get()->groupBy('paymentmethod_code');
                // return $data_start;
                 if(count($data_start) > 0){
                    foreach ($data_start as $el) {
                        $stotal = 0;
                        if(count($el) > 0){
                            foreach ($el as $subel) {
                                $stotal += floatval($subel->total_amount);
                            }
                        }
                        $total_start_riel += $stotal;
                        $total_start_usd = floatval($total_start_riel) / floatval($rate);

                        array_push($start_stock, array('description'=>"Start stock",'method'=>$el[0]->paymentmethod_code, 'total'=>$stotal));

                    }
                    array_push($startStock_object, array('description'=>"All start stock", 'method'=>'All','total_riel'=>$total_start_riel, 'total_usd'=>$total_start_usd,'items'=>$start_stock));
                 }

                 $purchasedata = payment_receive_view::whereDate('created_at','>=',$request->startDate)
                                                ->whereDate('created_at',"<=",$request->endDate)
                                                ->where('statue','=','open')
                                                ->get();
                if(count($purchasedata) > 0){
                    foreach ($purchasedata as $element) {
                        $total_purchase_riel += floatval($element->total_amount);
                        $total_purchase_usd = $total_purchase_riel / floatval($rate);


                    }
                    array_push($purchase, array('description'=>"Purchase Medicine", 'method'=>'All','total_riel'=>$total_purchase_riel,'total_usd'=>$total_purchase_usd,'items'=>$purchasedata));
                }

                // return $purchase;
                $all_total = $total_expanse_riel + $total_purchase_riel + $total_start_riel;

                return ['general'=>$expanse_object,'startstock'=>$startStock_object,'purchase'=>$purchase, 'total_riel'=>$all_total, 'exchange'=>$rate];

            }else{
                return "No data";
            }

        }else{
            return "No rights to view";
        }



    }
}
