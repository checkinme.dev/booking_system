<?php

namespace App\Http\Controllers;

use App\Models\active_tread;
use App\Models\brand;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    public function index(Request $request)
    {
        $brand = brand::orderBy('id', 'desc')->where('inactived', '!=', 'Yes')
            ->where('brand_name', 'LIKE',$request->search.'%')
            ->paginate(15);
        
            $brands = brand::orderBy('id','desc')->where('inactived','!=', 'Yes')
                        ->get();

        return ['brand'=>$brand, 'print'=>$brands];
    }

    public function store(Request $request)
    {
        $request->validate([
            'brand_name' => 'required',
        ]);
        $brand = brand::create([
            'brand_code' => $request['brand_name'],
            'brand_name' => $request['brand_name'],
            'brand_name_2' => $request['brand_name_2'],
            'inactived' => 'No',
            'created_by' => $request['created_by'],
        ]);
        if ($brand) {
            return $brand;
        } else {
            return ['statue :' => 'faile'];
        }
    }

    public function update($id, Request $request)
    {
        $brand = brand::find($id);
        $brand->brand_code = $request->brand_name;
        $brand->brand_name = $request->brand_name;
        $brand->brand_name_2 = $request->brand_name_2;
        $brand->inactived = $request->inactived;
        $brand->updated_by = $request->updated_by;
        $brand->save();
        if ($brand) {
            return ['statue :' => 'Succesfull'];
        } else {
            return ['statue :' => 'faile '];
        }
    }

    public function getdata()
    {
        $brand = brand::where('inactived', '!=', 'Yes')->get();

        return $brand;
    }

    public function getActivetread()
    {
        $brand = active_tread::get();

        return $brand;
    }
    public function searchBrand($search){
       
            $data = brand::where('inactived','!=','Yes')
            ->where('brand_code','LIKE','%'.$search.'%')->get();
        return ['data' => $data];
    }
}
