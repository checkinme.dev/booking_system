<?php

namespace App\Http\Controllers;

use App\Models\permittion_role;
use App\Models\setup;
use App\Models\User;
use App\Models\UserShowPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Auth\Middleware\Authenticate;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'user_name' => 'required|max:55',
            'password' => 'required|max:55',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $requestData['password'] = Hash::make($requestData['password']);
        $user = User::create($requestData);
        $pateint = UserShowPassword::where('user_code', '=', $user->user_code)->get();

        return $pateint;
    }

    public function createsetup(Request $request)
    {
        $pateint = setup::create([
          'company_name' => $request['company_name'],
          'company_Address' => $request['company_Address'],
          'company_Phone' => $request['company_Phone'],
          'company_Email' => $request['company_Email'],
          'main_Currency' => $request['main_Currency'],
          'product_check' => $request['product_check'],
          'created_by' => $request['created_by'],
        ]);
        if ($pateint) {
            return $pateint;
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function selectsetup()
    {
        $pateint = setup::orderBy('id', 'desc')->first();

        return $pateint;
    }

    public function login(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'user_name' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
            ], 422);
        }

        if (!auth()->attempt($requestData)) {
            return response()->json(['error' => 'UnAuthorised Access'], 401);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        $x = permittion_role::where('role', '=', auth()->user()->role)->first();

        // return $x;

        return response(['user' => auth()->user(), 'token' => $accessToken, 'permission' => $x], 200);
    }

    public function me(Request $request)
    {
        $user = $request->user();

        return response()->json(['user' => $user], 200);
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];

        return response($response, 200);
    }

    public function updatesetup($id, Request $request)
    {
        $dir = $this->directory();
        $unitcode = setup::find($id);
        if ($request->company_image != $unitcode['company_image']) {
            File::delete($dir.$unitcode['company_image']);
            $dir = $this->directory();
            $this->makeDirectory($dir);
            $img = time().'.'.explode(';', explode('/', $request->company_image)[1])[0];
            Image::make($request->company_image)->save(public_path($dir).$img);
        } else {
            $img = $request->company_image;
        }
        $unitcode->company_image = $img;
        $unitcode->company_name = $request->company_name;
        $unitcode->company_Address = $request->company_Address;
        $unitcode->company_Phone = $request->company_Phone;
        $unitcode->company_Email = $request->company_Email;
        $unitcode->main_Currency = $request->main_Currency;
        $unitcode->product_check = $request->product_check;
        $unitcode->created_by = $request->created_by;
        $unitcode->save();
        if ($unitcode) {
            return ['statue :' => 'SUCCESSE'];
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function makeDirectory($directory)
    {
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    public function directory()
    {
        return $dir = 'img'.DIRECTORY_SEPARATOR.'company'.DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        $pateint = UserShowPassword::orderBy('id', 'desc')->paginate(15);

        return $pateint;
    }

    public function getuseId(Request $request)
    {
        if ($request['created_by'] != '' & $request['user_code'] != '') {
            $pateint = UserShowPassword::where('user_code', '=', $request->user_code)->get();

            return $pateint;
        }
    }

    public function update($id, Request $request)
    {
        $user = UserShowPassword::find($id);
        if (!empty($user) && Hash::check($request->password, $user->password)) {
            $user->update([
                'password' => bcrypt($request['password']),
            ]);
        }

        $user->user_name = $request->user_name;
        $user->email = $request->email;
        // $user->password = Hash::make($request->password);
        $user->role = $request->role;
        $user->statue = $request->statue;
        $user->save();
        if ($user) {
            return ['message' => 'Update Successfully'];
        } else {
            return ['message' => 'Error Update'];
        }
    }

    public function deleteUser($id, Request $request)
    {
        if ($request['created_by'] != '' & $request['user_code'] != '') {
            $pateint = UserShowPassword::find($id);
            $pateint->delete();

            return UserShowPassword::get();
        }
    }

    public function updatePassword(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'user_name' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
            ], 422);
        }

        if (!auth()->attempt($requestData)) {
            return response()->json(['message' => 'UnAuthorised Access', 'status' => 2]);
        }

        $user = auth()->user();

        // return $user;
        $user->update([
            'password' => bcrypt($request['new_password']),
            'user_name' => $request->user_name,
            'email' => $request->email,
        ]);

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        $x = permittion_role::where('role', '=', auth()->user()->role)->first();

        if ($user) {
            return response()->json(['message' => 'Change password succesfully', 'status' => 1]);
        } else {
            return response()->json(['message' => 'Failed to update password', 'status' => 0]);
        }
    }
}
