<?php

namespace App\Http\Controllers;

use App\Models\unitofmeasure;
use Illuminate\Http\Request;

class UnitofMeasureContraller extends Controller
{
    public function allUnite()
    {
        $u = unitofmeasure::get();

        return $u;
    }

    public function searchUnit(Request $request)
    {
        if ($request->unit_code != null) {
            $result = unitofmeasure::where('unit_code', 'LIKE', '%'.$request->unit_code.'%')
            ->orderBy('id', 'desc')->paginate(15);

            return $result;
        }
        if ($request->unit_of_measure != null) {
            $result = unitofmeasure::where('unit_of_measure', 'LIKE', '%'.$request->unit_of_measure.'%')
            ->orderBy('id', 'desc')->paginate(15);

            return $result;
        }
        if ($request->type != null) {
            $result = unitofmeasure::where('type', 'LIKE', '%'.$request->type.'%')
            ->orderBy('id', 'desc')->paginate(15);

            return $result;
        }
    }

    public function searchUnittype($search){
        $units= unitofmeasure::where('inactived', '!=', 'Yes')->where('unit_code','LIKE','%'.$search.'%')->get();
        return $units;
    }

    public function index()
    {
        $unit = unitofmeasure::orderBy('id', 'desc')->where('inactived', '!=', 'Yes')->paginate(15);
        $units= unitofmeasure::orderBy('id', 'desc')->where('inactived', '!=', 'Yes')->get();

        return ['unit'=>$unit, 'print'=>$units];
    }

    public function getunite()
    {
        $unitofmeasure = unitofmeasure::where('type', '=', 'Product')->get();

        return $unitofmeasure;
    }

    public function getunitservice()
    {
        $unitofmeasure = unitofmeasure::where('type', '!=', 'Product')->where('inactived','!=','Yes')->get();

        return $unitofmeasure;
    }

    public function store(Request $request)
    {
        $request->validate([
            'unit_code' => 'required',
            'type' => 'required',
        ]);

        return unitofmeasure::create([
            'code' => $request->unit_code,
            'unit_code' => $request->unit_code,
            'unit_of_measure' => $request->unit_of_measure,
            'type' => $request->type,
            'inactived' => 'No',
            'created_by' => $request->created_by,
        ]);
    }

    public function update($id, Request $request)
    {
        $unitcode = unitofmeasure::where('unit_code', '=', $id)->first();
        $unitcode->code = $request->unit_code;
        $unitcode->unit_code = $request->unit_code;
        $unitcode->unit_of_measure = $request->unit_of_measure;
        $unitcode->type = $request->type;
        $unitcode->inactived = $request->inactived;
        $unitcode->updated_by = $request->updated_by;
        $unitcode->save();

        return $unitcode;
    }

    public function destroy($id)
    {
        $unitcode = unitofmeasure::find($id);
        $unitcode->delete();

        return response()->json(['message' => 'Unitcode deleted successfully']);
    }

    public function Getunitforproduct()
    {
        $data = unitofmeasure::where('type', '=', 'Product')->get();

        return $data;
    }
}
