<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\paymentMethod;

class PaymentMethodController extends Controller
{
   
    public function index(Request $request)
    {
        if($request->paymentmethod == ''){
            $curency = paymentMethod::where('inactived','!=','Yes')->paginate(15);
            $curencys = paymentMethod::where('inactived','!=','Yes')->get();
        }
        else{
            $curency = paymentMethod::where('paymentmethod', 'LIKE',$request->paymentmethod.'%')->where('inactived','!=','Yes')->orderBy('id', 'desc')->paginate(15);
            $curencys = paymentMethod::where('paymentmethod', 'LIKE',$request->paymentmethod.'%')
                                    ->where('inactived','!=','Yes')->orderBy('id', 'desc')->get();
        }
       
        return ['currecy'=>$curency, 'print'=>$curencys];
    }
 public function allPay()
    {
        $p = paymentMethod::get();
        return $p;
    }
    public function store(Request $request)
    {
        $request->validate([
            'paymentmethod' => 'required',
        ]);
        $exchangerate = paymentMethod::create([
            'paymentmethod_code' => $request['paymentmethod'],
            'paymentmethod' => $request['paymentmethod'],
            'description' => $request['description'],
            'inactived' =>'No',
            'created_by' => $request['created_by'], 
        ]);
      return $exchangerate;
    }

    public function update($id,Request $request)
    {
        $exchangerate = paymentMethod::find($id);
        $exchangerate->paymentmethod_code =$request->paymentmethod;
        $exchangerate->paymentmethod =$request->paymentmethod;
        $exchangerate->description =$request->description;
        $exchangerate->inactived =$request->inactived;
        $exchangerate->updated_by =$request->updated_by;
        $exchangerate->save();
        if($exchangerate){
            return $exchangerate;
        }else{
           return ['statue :'=>"faile"]; 
        }
    }

    public function destroy($id)
    {

        $curency = paymentMethod::find($id);
        $curency->delete();
        return paymentMethod::get();

    }

}
