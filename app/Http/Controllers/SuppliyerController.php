<?php

namespace App\Http\Controllers;

use App\Models\suppliyer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class SuppliyerController extends Controller
{
    public function index(Request $request)
    {
        $suppliyer = suppliyer::orderBy('id', 'desc')->where('inactived', '!=', 'Yes')->paginate(15);

        $suppliers = suppliyer::orderBy('id', 'desc')->where('inactived', '!=', 'Yes')->get();

        return ['sup'=>$suppliyer, 'print'=>$suppliers ];
    }

    public function SearchSup(Request $request)
    {
        if ($request->sup_name != null || $request->sup_name != '') {
            $result = suppliyer::where('sup_name', 'LIKE', '%'.$request->sup_name.'%')
            ->orderBy('id', 'desc')->paginate(15);

            return $result;
        }
        if ($request->address != null) {
            $result = suppliyer::where('address', 'LIKE', '%'.$request->address.'%')
            ->orderBy('id', 'desc')->paginate(15);

            return $result;
        }
        if ($request->phone_no != null) {
            $result = suppliyer::where('phone_no', 'LIKE', '%'.$request->phone_no.'%')
            ->orderBy('id', 'desc')->paginate(15);

            return $result;
        }
        if ($request->contact_name != null) {
            $result = suppliyer::where('contact_name', 'LIKE', '%'.$request->contact_name.'%')
            ->orderBy('id', 'desc')->paginate(15);

            return $result;
        }
        if ($request->contact_phone != null) {
            $result = suppliyer::where('contact_phone', 'LIKE', '%'.$request->contact_phone.'%')
            ->orderBy('id', 'desc')->paginate(15);

            return $result;
        }
    }

    public function getdatasub()
    {
        $suppliyer = suppliyer::where('inactived','=','No')->get();

        return $suppliyer;
    }

    public function store(Request $request)
    {
        $request->validate([
            'sup_name' => 'required',
            'address' => 'required',
            'phone_no' => 'required',
            'contact_name' => 'required',
            'contact_phone' => 'required',
        ]);
        if ($request->image_url != '' || $request->image_url != null) {
            $photo = $this->uploadPhoto($request);
        } else {
            $photo = 'avatar5.png';
        }
        $sup = suppliyer::create([
            // 'id' => $request->id,
            'sup_code' => $request->sup_name,
            'sup_name' => $request->sup_name,
            'sup_name_2' => $request->sup_name_2,
            'image_url' => $photo,
            'address' => $request->address,
            'phone_no' => $request->phone_no,
            'phone_no_2' => $request->phone_no_2,
            'fax_no' => $request->fax_no,
            'email' => $request->email,
            'contact_name' => $request->contact_name,
            'contact_phone' => $request->contact_phone,
            'thumbnail' => $photo,
            'status' => $request->status,
            'inactived' => 'No',
            'created_by' => $request->created_by,
        ]);
        if ($sup) {
            return response()->json(['msg' => 'success']);
        } else {
            return response()->json(['msg' => 'unsuccess']);
        }
    }

    public function update($id, Request $request)
    {
        $sup = suppliyer::findOrFail($id);
        $dir = $this->directory();
        if ($request->image_url != $sup['image_url']) {
            // File::delete($dir . $sup['image_url']);
            $photo = $this->uploadPhoto($request);
        } else {
            $photo = $request->image_url;
        }

        $sup->update([
            'id' => $request->id,
            'sup_code' => $request->sup_name,
            'sup_name' => $request->sup_name,
            'sup_name_2' => $request->sup_name_2,
            'image_url' => $photo,
            'address' => $request->address,
            'phone_no' => $request->phone_no,
            'phone_no_2' => $request->phone_no_2,
            'fax_no' => $request->fax_no,
            'email' => $request->email,
            'contact_name' => $request->contact_name,
            'contact_phone' => $request->contact_phone,
            'thumbnail' => $photo,
            'status' => $request->status,
            'inactived' => $request->inactived,
            'is_deleted' => '0',
            'updated_by' => $request->updated_by,
        ]);

        return response()->json(["$sup"]);
    }

    public function makeDirectory($directory)
    {
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    public function directory()
    {
        return $dir = 'img'.DIRECTORY_SEPARATOR.'suppliyer'.DIRECTORY_SEPARATOR;
    }

    public function uploadPhoto(Request $request)
    {
        $dir = $this->directory();
        $file = $request->image_url;
        $this->makeDirectory($dir);
        $photo = time().'.'.explode(';', explode('/', $file)[1])[0];
        Image::make($file)->save(public_path($dir).$photo);

        return $photo;
    }

    public function searchSupplier($search){
        $data = suppliyer::where('inactived','!=','Yes')
            ->where('sup_name','LIKE','%'.$search.'%')->get();
        return $data;
    }
}
