<?php

namespace App\Http\Controllers;

use App\Models\viewStockexpire;
use Illuminate\Http\Request;

class StockExpireController extends Controller
{
    public function index(Request $request)
    {
        if($request->product_no == '' && $request->mos == '' && $request->amc_amount == ''&& $request->stock_aoumt == '' && $request->startDate == '' && $request->endDate == '' && $request->startEx == '' && $request->endEx == '')
        {
            $ex = viewStockexpire::orderBy('product_no', 'desc')->paginate(15);
            $print = viewStockexpire::orderBy('product_no', 'desc')->get();
            return ['ex'=>$ex, 'print' =>$print];
        }
        
       else if ($request->product_no != null) {
            $result = viewStockexpire::where('product_no', 'LIKE', '%'.$request->product_no.'%')
            ->orWhere('description', 'LIKE', '%'.$request->product_no.'%')
            ->orderBy('product_no', 'desc')->paginate(15);
            $print = viewStockexpire::where('product_no', 'LIKE', '%'.$request->product_no.'%')
            ->orWhere('description', 'LIKE', '%'.$request->product_no.'%')
            ->orderBy('product_no', 'desc')->get();
            return ['ex'=>$result, 'print' =>$print];
        }
       
        else if ($request->mos != null) {
            $check = explode(',', $request->mos);
            $result = viewStockexpire::where('mos', $check[0], $check[1])
            ->orderBy('product_no', 'desc')->paginate(15);

            $print = viewStockexpire::where('mos', $check[0], $check[1])
            ->orderBy('product_no', 'desc')->get();

            return ['ex'=>$result, 'print' =>$print];
        }

        else if ($request->amc_amount != null) {
            $check1 = explode(',', $request->amc_amount);
            $result1 = viewStockexpire::where('amc_amount', $check1[0], $check1[1])
           ->orderBy('product_no', 'desc')->paginate(5);
           $print = viewStockexpire::where('amc_amount', $check1[0], $check1[1])
           ->orderBy('product_no', 'desc')->get();

           return ['ex'=>$result1, 'print' =>$print];
        }

        else if($request->stock_aoumt != null){
            $result = viewStockexpire::where('stock_aoumt','LIKE','%'.$request->stock_aoumt.'%')
                                      ->orderBy('product_no', 'desc')
                                      ->paginate(15);
            $print =   viewStockexpire::where('stock_aoumt','LIKE','%'.$request->stock_aoumt.'%')
            ->orderBy('product_no', 'desc')
            ->get();
            return ['ex'=>$result, 'print' =>$print];
        }

       else if($request->startDate != null && $request->endDate != null){
            $result = viewStockexpire::whereBetween('created_at',[$request->startDate, $request->endDate])
                                      ->paginate(15);
            $print = viewStockexpire::whereBetween('created_at',[$request->startDate, $request->endDate])
                                     ->get();
            return ['ex'=>$result, 'print' =>$print];
        }
       else if($request->startEx != null && $request->endEx != null){
            $result = viewStockexpire::whereBetween('exprit_date',[$request->startEx, $request->endEx])
                                      ->paginate(5);
            $print = viewStockexpire::whereBetween('exprit_date',[$request->startEx, $request->endEx])
            ->get();
            return ['ex'=>$result, 'print' =>$print];
        }
    }



} 
