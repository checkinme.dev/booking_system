<?php

namespace App\Http\Controllers;
use App\Models\stocktransaction;
use App\Models\setup;
use App\Models\product_reorder;
use App\Models\product_reorder_point;
use App\Models\product_exprit_date;
use App\Models\expire_alert;
use App\Models\product;
use App\Models\viewprodoctstock;
use Illuminate\Http\Request;
use App\Models\productvariantcode;
use Illuminate\Contracts\Session\Session;

class StockTransactionContraller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->name == ''){
            $productvariantcode = stocktransaction::orderBy('created_at','DESC')->paginate(15);    
            $print = stocktransaction::orderBy('created_at','DESC')->get();    


            if ($productvariantcode) {
                return ['stock'=>$productvariantcode, 'print' =>$print];
            } else {
                return ['statue :' => "Note Date"];
            }
        }
        else if($request->name != ''){
            $data = stocktransaction::where('document_no','LIKE','%'.$request->name.'%')
                                    ->orwhere('description','LIKE','%'.$request->name.'%')
                                    ->orwhere('document_type','LIKE','%'.$request->name.'%')
                                    ->orwhere('product_no','LIKE','%'.$request->name.'%')
                                    ->paginate(15);
            $print = stocktransaction::where('document_no','LIKE','%'.$request->name.'%')
                                    ->orwhere('description','LIKE','%'.$request->name.'%')
                                    ->orwhere('document_type','LIKE','%'.$request->name.'%')
                                    ->orwhere('product_no','LIKE','%'.$request->name.'%')
                                    ->get();
            return ['stock'=>$data, 'print' =>$print]; 
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function mySearch($search){
        $data = stocktransaction::where('document_no','LIKE','%'.$search.'%')
                                ->orwhere('description','LIKE','%'.$search.'%')
                                ->orwhere('document_type','LIKE','%'.$search.'%')
                                ->orwhere('product_no','LIKE','%'.$search.'%')
                                ->paginate(15);
        return $data;
    }
    public function stockalert($search){
        $result = product_reorder::where('description','LIKE','%'.$search.'%')
                                  ->orwhere('reorder_point','LIKE','%'.$search.'%')
                                  ->orwhere('inventorys','LIKE','%'.$search.'%')
                                  ->get();
        return $result;
    }
    public function ex_alert($search){
        $result = expire_alert::where('description','LIKE','%'.$search.'%')
                                ->orwhere('reorder_point','LIKE','%'.$search.'%')
                                ->orwhere('amc_inventery','LIKE','%'.$search.'%')
                                ->orwhere('mos','LIKE','%'.$search.'%')
                                ->paginate(10);
        return $result;
    }
    public function viewproduct($search){
        $result = viewprodoctstock::whereIn('product_no', viewprodoctstock::select('product_no')
                                            ->where('product_no','LIKE','%'.$search.'%')
                                            ->orwhere('description','LIKE','%'.$search.'%')
                                            ->orwhere('description_2','LIKE','%'.$search.'%')
                                            ->orwhere('unit_price','LIKE','%'.$search.'%')
                                            ->orwhere('brand_code','LIKE','%'.$search.'%')
                                            ->orwhere('stock_unit_of_measure_code','LIKE','%'.$search.'%')
                                            ->orwhere('cat_code','LIKE','%'.$search.'%')
                                            ->orwhere('sup_code','LIKE','%'.$search.'%')
                                            ->get()
                                        )
                                        ->Where('inactived', '!=', 'Yes')
                                        ->paginate(10);

        return $result;
    }

    public function viewproductVariancode(Request $request){

        $object = array();

        if($request->search != '' && $request->search != null){
            $result = viewprodoctstock::whereIn('product_no', viewprodoctstock::select('product_no')
                                                    ->where('product_no','LIKE','%'.$request->search.'%')
                                                    ->orwhere('description','LIKE','%'.$request->search.'%')
                                                    ->orwhere('description_2','LIKE','%'.$request->search.'%')
                                                    ->orwhere('unit_price','LIKE','%'.$request->search.'%')
                                                    ->orwhere('brand_code','LIKE','%'.$request->search.'%')
                                                    ->orwhere('stock_unit_of_measure_code','LIKE','%'.$request->search.'%')
                                                    ->orwhere('cat_code','LIKE','%'.$request->search.'%')
                                                    ->orwhere('sup_code','LIKE','%'.$request->search.'%')
                                                    ->get()
                                                )
                                                ->Where('inactived', '!=', 'Yes')
                                                ->get();
            // where('product_no','LIKE','%'.$request->search.'%')
            //                         ->where('inactived','!= ', 'Yes')
            //                         ->orwhere('description','LIKE','%'.$request->search.'%')
            //                         ->orwhere('description_2','LIKE','%'.$request->search.'%')
            //                         ->orwhere('unit_price','LIKE','%'.$request->search.'%')
            //                         ->orwhere('brand_code','LIKE','%'.$request->search.'%')
            //                         ->orwhere('stock_unit_of_measure_code','LIKE','%'.$request->search.'%')
            //                         ->orwhere('cat_code','LIKE','%'.$request->search.'%')
            //                         ->orwhere('sup_code','LIKE','%'.$request->search.'%')
            //                         ->get();
                                                         

        }
        if($request->search == ''){
            $result = viewprodoctstock::orderBy('product_no','DESC')->where('type','=','Products')->get();
        }

        if(count($result) > 0){
            foreach ($result as $el) {
                $productvariantcode = productvariantcode::where('product_no', '=', $el->product_no)
                ->get();
                array_push($object, array('el' =>$el,'items'=>$productvariantcode ));

            }
        }

        return ['product'=>$result,'detail'=>$object];
    }



}