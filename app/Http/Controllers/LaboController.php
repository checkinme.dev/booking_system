<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\laboratory;
use App\Models\prescriptions;
use App\Models\labolab;
use App\Models\patient_labo;
use App\Models\diagnosis_list;
use App\Models\testtingitem;
use App\Models\subtest;
use App\Models\product;

class LaboController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = laboratory::get();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = product::where('product_no','=',$request->product_no)->first();

        $result = laboratory::create([
            'labId' => $request->labId,
            'product_no' => $request->product_no,
            'product_type' => $request->product_type,
            'description' => $request->description,
            'test_type' => $request->test_type,
            'normal_value' => $request->normal_value,
            'stock_unit_of_measure_code' => $data->stock_unit_of_measure_code,
            'test_value' => $request->test_value,
            'unit_price' => $request->unit_price,
            'status' => $request->status,
            'remark' => $request->remark,
            'created_by' => $request->created_by,
            'updated_by' => $request->updated_by
        ]
        );
        $subtests = subtest::where('product_no','=',$request->product_no)->get();

        if(count($subtests) > 0){
            foreach($subtests as $subtest){
                $test = testtingitem::where('product_no','=',$subtest->sub_product_no)->where('test_type','=',$request->test_type)->first();
                laboratory::create([
                    'labId' => $request->labId,
                    'product_no' => $subtest->product_no,
                    'description' => $test->description,
                    'test_type' => $test->test_type,
                    'normal_value' => $test->normal_value,
                    'stock_unit_of_measure_code' => $subtest->stock_unit_of_measure_code,
                    'test_value' => null,
                    'unit_price' => null,
                    'status' => 'Subtest',
                    'remark' => $request->remark,
                    'created_by' => $request->created_by,
                    'updated_by' => $request->updated_by
                ]);
            }
        }
        return 'Created successfully';
    }
    public function getLaboId($id)
    {
        $object = [];
        $datas = laboratory::where('labId','=',$id)->where('status','=','Open')->get();
        if(count($datas) > 0){
            foreach($datas as $data){
                $subtest = laboratory::where('product_no','=',$data->product_no)
                                    ->where('labId','=',$id)
                                    ->where('status','=','Subtest')->get();
                array_push($object, array('test' => $data, 'subtest' => $subtest));
            }
        }
         return $object;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $data = laboratory::find($id);
        $data->description = $request->description;
        $data->product_type = $request->product_type;
        $data->test_type = $request->test_type;
        $data->normal_value = $request->normal_value;
        $data->test_value = $request->test_value;
        $data->status = $request->status;
        $data->remark  = $request->remark;
        $data->unit_price = $request->unit_price;
        $data->updated_by = $request->updated_by;
        $data->save();
        return "Updated successfully";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = laboratory::find($id);
        $subtests = laboratory::where('product_no','=',$data->product_no)->get();
        if(count($subtests) > 0){
            foreach($subtests as $subtest){
                $subtest->delete();
            }
        }
        return "Deleted successfully";
    }
    public function submitLabo($id,Request $request)
    {
        $data = prescriptions::where('preid','=',$id)->first();

        if($data->type == 'labo' && $data->status == 'open'){
            $labo = labolab::create([
                'labId' => $data->preid,
                'pateinId' => $data->pateinid,
                'Age' => $data->age,
                'diagnosis' => $data->group_code,
                'Date' => $data->visit_date,
                'BP' => $data->bp,
                'Pr' => $data->pr,
                'rr' => $data->rr,
                'Spo2' => $data->spo2,
                'T' => $data->t,
                'created_by' => $request->created_by,
                'status' => 'open',
                'remark' => $data->remark
            ]);
            $data->status = 'inprocess';
            $data->save();
            return [
                "prescription" => $data,
                "Labo" => $labo
            ];
        }
        else{
            return [
                'message' => "failed for creating",
                'status' => 0
            ];
        }
     
    }
    public function SubmitBackfromLabo($id,Request $request)
    {
        $data = prescriptions::where('preid','=',$id)->first();
        $labo = labolab::where('labId','=',$id)->first();
        $labo->status = "success";
        $labo->save();
        $data->status = "success";
        $data->save();
        return ["status" => "Successfully"];
    }

    public function LaboLab(Request $request){
        $result = labolab::orderBy('id','DESC')->get();
        return $result;
    }
    public function getPatientLabo(Request $request)
    {
        $result = patient_labo::orderBy('id','DESC')
                                ->where('status','=','open')
                                ->get();
        return $result;
    }
    public function getViewPatientLabo(Request $request)
    {
        if($request->name == '' && $request->startDate == '' && $request->endDate == ''){
            $result = patient_labo::orderBy('id','DESC')->paginate(15);
            $print = patient_labo::orderBy('id','DESC')->get();
            return ['data'=>$result,'print'=>$print];
        }
        if($request->name != ''){
            $search = explode(' ',$request->name);
            if(count($search) == 1){
                $result = patient_labo::orderBy('labId','desc')
                                    ->where('pateinid','LIKE',$search[0].'%')
                                    ->orwhere('firstname','LIKE',$search[0].'%')
                                    ->orwhere('lstname','LIKE',$search[0].'%')
                                    ->orwhere('sex','LIKE',$search[0].'%')
                                    ->orwhere('age','LIKE',$search[0].'%')
                                    ->orwhere('pro_city','LIKE',$search[0].'%')
                                    ->paginate(15);
                $print = patient_labo::orderBy('labId','desc')
                                ->where('pateinid','LIKE',$search[0].'%')
                                ->orwhere('firstname','LIKE','%'.$search[0].'%')
                                ->orwhere('lstname','LIKE','%'.$search[0].'%')
                                ->orwhere('sex','LIKE',$search[0].'%')
                                ->orwhere('age','LIKE',$search[0].'%')
                                ->orwhere('pro_city','LIKE',$search[0].'%')
                                ->get();
                 return ['data'=>$result,'print' => $print];
            }
            if(count($search) == 2){
                $result = patient_labo::orderBy('labId','desc')
                                        ->where('firstname','LIKE','%'.$search[0].'%')
                                        ->where('lstname','LIKE','%'.$search[1].'%')
                                        ->paginate(15);
                $print = patient_labo::orderBy('labId','desc')
                                        ->where('firstname','LIKE','%'.$search[0].'%')
                                        ->where('lstname','LIKE','%'.$search[1].'%')
                                        ->get();
                 return ['data'=>$result,'print' => $print];
            }
        }
        if($request->startDate != '' && $request->endDate != ''){
            $result = patient_labo::orderBy('labId','desc')
                                    // ->whereBetween('Date',[$request->startDate,$request->endDate])
                                    ->whereDate('Date','>=',$request->startDate)
                                    ->whereDate('Date','<=',$request->endDate)
                                    ->paginate(15);
            $print = patient_labo::orderBy('labId','desc')
                                // ->whereBetween('Date',[$request->startDate,$request->endDate])
                                ->whereDate('Date','>=',$request->startDate)
                                ->whereDate('Date','<=',$request->endDate)
                                ->get();
            return ['data'=>$result,'print' => $print];
        }
        
    }
    public function getOneLabolab($id, Request $request)
    {
        $result = patient_labo::where('labId', '=',$id)->first();
        return $result;
    }

    public function DeleteMedicine($id){
        $data = diagnosis_list::find($id);
        $data->delete();
        return "Deleted Successfully";
    }
    public function searchPatientLabo(Request $request){
        if($request->name != ''){
            $search = explode(' ',$request->name);
            if(count($search) == 1){
                $result = patient_labo::orderBy('labId','desc')
                                    ->where('pateinid','LIKE',$search[0].'%')
                                    ->orwhere('firstname','LIKE','%'.$search[0].'%')
                                    ->orwhere('lstname','LIKE','%'.$search[0].'%')
                                    ->orwhere('sex','LIKE',$search[0].'%')
                                    ->orwhere('age','LIKE',$search[0].'%')
                                    ->orwhere('pro_city','LIKE',$search[0].'%')
                                    ->paginate(15);
            return $result;
            }
            if(count($search) == 2){
                $result = patient_labo::orderBy('labId','desc')
                                        ->where('firstname','LIKE','%'.$search[0].'%')
                                        ->where('lstname','LIKE','%'.$search[1].'%')
                                        ->paginate(15);
                return $result;
            }
        }
        if($request->startDate != '' && $request->endDate != ''){
            $result = patient_labo::orderBy('labId','desc')
                                    ->whereBetween('Date',[$request->startDate,$request->endDate])
                                    ->paginate(15);
            return $result;
        }
       
    }
    public function getDataReport(Request $request){
        return patient_labo::orderBy('labId','desc')->get();
    }
   
}
