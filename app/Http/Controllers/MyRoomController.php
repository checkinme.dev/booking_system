<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\rooms;
use App\Models\setup;
use App\Models\roomfurniture;
use App\Models\roomprice;
use App\Models\roomstype;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Models\RoomFurnitureView;
use App\Models\furnitures;


class MyRoomController extends Controller
{
    public function generateId(Request $request){
        $id = Carbon::now()->format('dmYHis');
        $setup = setup::first();
        $result = $this->createRoom($id, $request);
        return $result;
    }
    // create room
    public function createRoom($id, Request $request){
        $setup = setup::first();
        $room = rooms::create([
            'roomtype_no' => '',
            'room_no' => $id,
            'size' => '',
            'room_name' => '',
            'description' => '',
            'currency' => $setup->main_Currency,
            'adults' => '',
            'room_price' => '',
            'status' => 'open',
            'created_by' => $request->created_by
        ]);
        $roomFurniture = $this->createFurniture($id,$request);
        $roomprice = $this->createRoomPrice($id, $request);
        if($room){
            return ['room' => $room,'roomfurniture' => $roomFurniture, 'roomprice' => $roomprice];
        }
    }
    //create furniture
    public function createFurniture($id,Request $request){
        $createFurniture = roomfurniture::create([
            'room_no' => $id,
            'furniture_no' => '',
            'qty' => '',
            'status' => 'open',
            'created_by' => $request->created_by
        ]);
       $roomFurniture = $this->getAllFurniture($id);
        return $roomFurniture;
    }
    //create room price
    public function createRoomPrice($id, Request $request){
       $data = roomprice::create([
            'room_no' => $id,
            'status' => 'open',
            'created_by' => $request->created_by
        ]);
        $roomPrice = $this->getAllRoomPrice($id);
        return $roomPrice;
    }
    //get room
    public function getRoom(Request $request){
        $id = $request->room_no;
        $room = rooms::where('room_no',$id)->first();
        $roomFurniture = $this->getAllFurniture($id);
        $roomPrice = $this->getAllRoomPrice($id);

       return ['room' => $room,'roomfurniture' => $roomFurniture, 'roomprice' => $roomPrice];
    }

    //get all room furniture
    public function getAllFurniture($id){
        return RoomFurnitureView::where('room_no',$id)->get();
    }
    //get all room price
    public function getAllRoomPrice($id){
        return roomprice::where('room_no',$id)->get();
    }
    //get room type
    public function getRoomType(Request $request){
        return roomstype::get();
    }
    //get furniture
    public function getFurniture(){
        return furnitures::get();
    }

    //Update section
    public function updateRoom(Request $request){
        $room = rooms::where('room_no',$request->room_no)->first();
        if($room){
            $room->update([
                'roomtype_no' => $request->roomtype_no,
                'size' => $request->size,
                'room_name' => $request->room_name,
                'description' => $request->description,
                'adults' => $request->adults,
                'room_price' => $request->room_price,
                'updated_by' => $request->updated_by
            ]);
            $data = $this->getOnlyRoom($request->room_no);
            return ['status' => true, 'message' => 'Update successfully', 'room' => $data];
        }
        return ['status' => false, 'message' => 'Failed to update'];
    }

    public function updateRoomImage(Request $request){
        $data = rooms::where('room_no',$request->room_no)->first();
        if($data){
            if($data->image != '' && $data->image != null){
                $dir = $this->directory();
                File::delete($dir.$data['image']);
            }
            if($request->image != '' && $request->image != null){
                $dir = $this->directory();
                $this->makeDirectory($dir);
                $img = time().'.'.explode(';', explode('/', $request->image)[1])[0];
                $picture = Image::make($request->image)->save(public_path($dir).$img);
                $data->image = $img;
                $data->save();
            }
            return ['status' => true,'message' =>'Update successfully' , 'room'=>$this->getOnlyRoom($request->room_no)];
        }
        return ['status' => false,'message' =>'Failed to update'];

    }

    //Update room funiture
    public function updateFurniture(Request $request){
        $furniture = roomfurniture::where('id',$request->id)->first();
        if($furniture){
            $furniture->update([
                'furniture_no' => $request->furniture_no,
                'qty' => $request->qty
            ]);
            $allFurniture = $this->getAllFurniture($request->room_no);
            return ['status' => true, 'message' => 'Update successfully','furniture' => $allFurniture];
        }
        return ['status' => false, 'message' => 'Failed to update'];

    }
    //Delete furniture
    public function deleteFurniture(Request $request){
        $data = roomfurniture::where('id',$request->id)->first();
        if($data){
            $data->delete();
            $allFurniture = $this->getAllFurniture($request->room_no);
            return ['status' => true, 'message' => 'Delete successfully','furniture' => $allFurniture];
        }
        return ['status' => false, 'message' => 'Failed to Delete'];
    }

    //Update Room Price
    public function updateRoomPrice(Request $request){
        $data = roomprice::where('id',$request->id)->first();
        if($data){
            $data->update([
                'desctiption' => $request->desctiption,
                'type' => $request->type,
                'qty' => $request->qty,
                'price' => $request->price
            ]);

            $allRoomPrice = $this->getAllRoomPrice($request->room_no);
            return ['status' => true, 'message' => 'Updated successfully','roomprice' => $allRoomPrice];
        }
        return ['status' => false, 'message' => 'Failed to update'];
    }
    //Delete Room Price
    public function deleteRoomPrice(Request $request){
        $data = roomprice::where('id',$request->id)->first();
        if($data){
            $data->delete();
            $allRoomPrice = $this->getAllRoomPrice($request->room_no);
            return ['status' => true, 'message' => 'Deleted successfully','roomprice' => $allRoomPrice];
        }
        return ['status' => false, 'message' => 'Failed to delete'];
    }


    public function getOnlyRoom($id){
        return rooms::where('room_no',$id)->first();
    }

    public function makeDirectory($directory)
    {
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
    }
    public function directory()
    {
        return $dir = 'img'.DIRECTORY_SEPARATOR.'room'.DIRECTORY_SEPARATOR;
    }


}
