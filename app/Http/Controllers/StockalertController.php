<?php

namespace App\Http\Controllers;
use App\Models\setup;
use App\Models\product_reorder;
use App\Models\product_reorder_point;
use App\Models\product_exprit_date;
use App\Models\stockkeeping;

use App\Models\expire_alert;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StockalertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function checkOption_stock(Request $request)
    {
        $setup = setup::first();
        $checkStop = $setup->product_check;
        if($checkStop == "All-Check"){
            if($request->name == ''){
                $expristock = product_reorder::paginate(15);
                $print = product_reorder::get();
                return ['data' => $expristock, 'print' => $print];
            }
            else if($request->name != ''){
                $result = product_reorder::where('description','LIKE','%'.$request->name .'%')
                                        ->orwhere('reorder_point','LIKE','%'.$request->name .'%')
                                        ->orwhere('inventorys','LIKE','%'.$request->name .'%')
                                        ->paginate(15);
                $print = product_reorder::where('description','LIKE','%'.$request->name .'%')
                                            ->orwhere('reorder_point','LIKE','%'.$request->name .'%')
                                            ->orwhere('inventorys','LIKE','%'.$request->name .'%')
                                            ->get();
             return ['data' => $result, 'print' => $print];
        }
       
        }else{
            // $expristock = product_reorder::paginate(15);
            // return $expristock;
            if($request->name == ''){
                $expristock = product_reorder::paginate(15);
                $print = product_reorder::get();
                return ['data' => $expristock, 'print' => $print];
            }
            else if($request->name != ''){
                $result = product_reorder::where('description','LIKE','%'.$request->name .'%')
                                        ->orwhere('reorder_point','LIKE','%'.$request->name .'%')
                                        ->orwhere('inventorys','LIKE','%'.$request->name .'%')
                                        ->paginate(15);
                $print = product_reorder::where('description','LIKE','%'.$request->name .'%')
                                            ->orwhere('reorder_point','LIKE','%'.$request->name .'%')
                                            ->orwhere('inventorys','LIKE','%'.$request->name .'%')
                                            ->get();
             return ['data' => $result, 'print' => $print];
        }
        }

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getExpireAlert(Request $request){
        
        if($request->name == ''){
            $result = expire_alert::paginate(15);
            $print = expire_alert::get();
        }
        else{
           $result= expire_alert::where('description','LIKE',$request->name.'%')
                                ->orwhere('reorder_point','LIKE',$request->name.'%')
                                ->orwhere('amc_inventery','LIKE',$request->name.'%')
                                ->orwhere('mos','LIKE',$request->name.'%')
                                ->paginate(10);
            $print =  expire_alert::where('description','LIKE',$request->name.'%')
                                    ->orwhere('reorder_point','LIKE',$request->name.'%')
                                    ->orwhere('amc_inventery','LIKE',$request->name.'%')
                                    ->orwhere('mos','LIKE',$request->name.'%')
                                    ->get();
        }
    
         return ['stock'=>$result,'print'=>$print];
    }
    public function updateExpire($id , Request $request){
        $data = stockkeeping::where('id','=',$id)->first();
        $data->exprit_date = $request->exprit_date;
        $data->save();
        return $data;
    }
}
