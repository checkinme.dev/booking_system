<?php

namespace App\Http\Controllers;

use App\Models\exchangerate;
use App\Models\payment_amount;
use App\Models\product_vavaincode_view;
use App\Models\purchealine;
use App\Models\purcheaorder;
use App\Models\receiveorder;
use App\Models\receiveorderline;
use App\Models\stockkeeping;
use App\Models\stocktransaction;
use Illuminate\Http\Request;

class ReceiveOrderContraller extends Controller
{
    public function store(Request $request)
    {
        $receiveorder = receiveorder::create([
        'document_no' => $request['document_no'],
        'document_type' => $request['document_type'],
        'description' => $request['description'],
        'suppliyer_code' => $request['suppliyer_code'],
        'curency_code' => $request['curency_code'],
        'inactived' => $request['inactived'],
        'total_amount' => $request['total_amount'],
        'statue' => 'Saved',
        'created_by' => $request['created_by'],
        ]);

        if ($receiveorder) {
            $totall = 0;
            $exchangerate = exchangerate::where('main_curency_no', '=', $receiveorder->curency_code)->first();
            $exmant = $exchangerate->main_curency_no;
            $exrate = $exchangerate->exchange_rate;
            if ($exmant == $receiveorder->curency_code) {
                $totall = $receiveorder->total_amount;
            } else {
                $totall = $receiveorder->total_amount;
                $totall = doubleval($totall) * doubleval($exrate);
            }
            $payment_amount = payment_amount::create([
                   'document_no' => $receiveorder['document_no'],
                   'document' => 'Purchese Order Recept',
                   'decription' => $receiveorder['decription'],
                   'totale_balanec' => $totall,
                   'exchane_rate' => $exrate,
                   'sub_code' => $receiveorder->id,
                   'currency_code' => $receiveorder['curency_code'],
                   'paymant_amount' => '0',
                   'inactived' => $receiveorder['inactived'],
                   'statue' => 'open',
                   'created_by' => $request['created_by'],
               ]);
            if ($payment_amount) {
                return ['statue' => 'successful', $receiveorder];
            } else {
                return ['statue ' => 'faile'];
            }
        } else {
            return ['statue ' => 'faile'];
        }
    }

    public function receptlink(Request $request)
    {
        $totall = 0;
        $check = true;
        $statue = 'open';
        $receiveorder = receiveorderline::create([
        'document_no' => $request['document_no'],
        'document_type' => $request['document_type'],
        'product_no' => $request['product_no'],
        'description' => $request['description'],
        'issu_date' => $request['issu_date'],
        'exprit_date' => $request['exprit_date'],
        'line_no' => $request['line_no'],
        'unit_of_measure_code' => $request['unit_of_measure_code'],
        'unit_price' => $request['unit_price'],
        'inventory' => $request['inventory'],
        'inventory_order' => $request['inventory_order'],
        'inventory_recetive' => $request['inventory_recetive'],
        'inventory_recetived' => $request['inventory_recetived'],
        'qty_balance' => $request['qty_balance'],
        'amount_balance' => $request['amount_balance'],
        'total_amount' => $request['total_amount'],
        'curency_code' => $request['curency_code'],
        'remark' => $request['remark'],
        'created_by' => $request['created_by'],
        ]);
        $reslin = $receiveorder;
        if ($receiveorder) {
            if (1) {
                $purchealine = purchealine::where('document_no', '=', $request->document_no)->where('product_no', '=', $request->product_no)->first();
                $inventorynew = floatval($purchealine->inventory) - floatval($request->inventory_recetive);
                $inventoryres = $request->inventory_recetive;
                $purOrder = purcheaorder::where('document_no', '=', $request->document_no)->first();
                $purOrder->statue = 'close';
                $purOrder->save();
                $productvariantcode = product_vavaincode_view::addSelect('product_no', 'stock_unit_of_measure_code', 'purche_unit_of_measure_code', 'variant_unit_of_measure_code', 'quantity_per_unit', 'unit_price', 'curency_code')->where('product_no', '=', $request->product_no)->where('variant_unit_of_measure_code', '=', $request->unit_of_measure_code)->first();
                $PrinceLink = product_vavaincode_view::addSelect('unit_price')->where('product_no', '=', $request->product_no)->where('status', '=', 'stock')->first();
                if ($productvariantcode) {
                }
                $inventory_order = 0;
                $inventory = doubleval($request['inventory_recetive']) + doubleval($request['qty_balance']);
                $inventory = doubleval($inventory) * doubleval($productvariantcode['quantity_per_unit']);
                $unit_price = doubleval($request['unit_price']) / doubleval($productvariantcode['quantity_per_unit']);
                
                $receiveorder = stockkeeping::create([
                    'document_no' => $request['document_no'],
                    'document_type' => $request['document_type'],
                    'product_no' => $request['product_no'],
                    'description' => $request['description'],
                    'issu_date' => $request['issu_date'],
                    'exprit_date' => $request['exprit_date'],
                    'line_no' => $request['id'],
                    'unit_of_measure_code' => $productvariantcode['stock_unit_of_measure_code'],
                    'unit_price' => $unit_price,
                    'inventory' => $inventory,
                    'inventory_order' => $request['inventory_order'],
                    'inventory_new' => $inventorynew,
                    'total_amount' => $request['total_amount'],
                    'curency_code' => $request['curency_code'],
                    'remark' => $request['remark'],
                    'statuse' => 'open',
                    'created_by' => $request['created_by'],
                    ]);
                    $number = $receiveorder;
                if ($receiveorder) {
                    if ($purchealine->inventory != $request->inventory_recetive) {
                        $check = false;
                    }
                    $stocktransaction = stocktransaction::create([
                      'document_no' => $receiveorder['document_no'],
                      'document_type' => 'Order Recpt Invoince',
                      'product_no' => $receiveorder['product_no'],
                      'description' => $receiveorder['description'],
                      'unit_of_measure_code' => $receiveorder['unit_of_measure_code'],
                      'unit_price' => $receiveorder['unit_price'],
                      'inventory' => $inventory,
                      'total_amount' => $request['total_amount'],
                      'curency_code' => $receiveorder['curency_code'],
                      'remark' => $receiveorder['remark'],
                      'created_by' => $receiveorder['created_by'],
                      ]);
                }
                if (!$check) {
                    $statue = 'open';
                    $purchealines = purchealine::find($purchealine->id);
                    $purchealines->statue = $statue;
                    //add more
                    $purchealines->inventory = $inventorynew;
                    $purchealines ->inventory_recetive = 0;
                    //end add more
                    $purchealines->save();
                    $reslin ->inventory = $inventorynew;
                    $reslin->inventory_recetive = 0;
                    $reslin->line_no = $request->resId;
                    $reslin->save();
                    $number->line_no = $reslin->id;
                    $number->save();
                } else {
                    $statue = 'close';
                    $purchealines = purchealine::find($purchealine->id);
                    $purchealines->statue = $statue;
                    $purchealines->inventory = $inventorynew;
                    $purchealines->inventory_recetive = 0;
                    $purchealines->save();
                    $reslin ->inventory = $inventorynew;
                    $reslin->inventory_recetive = 0;
                    $reslin->line_no = $request->resId;
                    $reslin->save();
                    $number->line_no = $reslin->id;
                    $number->save();
                }
                $purchealine = purchealine::where('document_no', '=', $request->document_no)->get();
                foreach ($purchealine as $purche) {
                    if ($purche->statue == 'open') {
                        $purOrder = purcheaorder::where('document_no', '=', $request->document_no)->first();
                        $purOrder->statue = 'pending';
                        $purOrder->save();
                        continue;
                    }
                }
                if ($purOrder) {
                    return ['statue' => 'successful'];
                } else {
                    return ['statue ' => 'faile'];
                }
            } else {
            }
        } else {
            return ['statue' => 'fail'];
        }
    }

    public function getshowpurchea(Request $request)
    {
        $productvariantcode = purcheaorder::orderBy('id', 'asc')->where('statue', '=', 'open')->get();
        if ($productvariantcode) {
            return $productvariantcode;
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function getrecpt(Request $request)
    {
        if($request->name == ''){
            $receiveorder = receiveorder::orderBy('id','DESC')->paginate(15);
            $print = receiveorder::orderBy('id','DESC')->get();
            return ['recept'=>$receiveorder, 'print'=>$print];
        }
        else{
            $receiveorder = receiveorder::orderBy('id','DESC')
                                        ->where('document_no','LIKE',$request->name.'%')
                                        ->orwhere('document_type','LIKE',$request->name.'%')
                                        ->orwhere('description','LIKE',$request->name.'%')
                                        ->orwhere('suppliyer_code','LIKE',$request->name.'%')
                                        ->paginate(15);
            $print = receiveorder::orderBy('id','DESC')
                                ->where('document_no','LIKE',$request->name.'%')
                                ->orwhere('document_type','LIKE',$request->name.'%')
                                ->orwhere('description','LIKE',$request->name.'%')
                                ->orwhere('suppliyer_code','LIKE',$request->name.'%')
                                ->get();
            return ['recept'=>$receiveorder, 'print'=>$print];
        }
        
    }
        
    public function searchRecieve(Request $request){
        $data = receiveorder::orderBy('id','DESC')
                            ->where('document_no', 'LIKE',$request->document_no.'%' )
                            ->where('document_type', 'LIKE','%'.$request->document_type.'%')
                            ->where('description', 'LIKE','%'.$request->description.'%')
                            ->where('suppliyer_code','LIKE',$request->supplier.'%')
                            ->paginate(15);
        return $data;
    }

    //Reverse function in Recieve
    public function Reverse($id){

        $data = receiveorder::where('id','=',$id)->first();
        $data->statue = "reverse";
        $data->save();

       //reverse pay amount
        $payamount = payment_amount::where('sub_code','=',$data->id)->first();
        $payamount->statue = "reverse";
        $payamount->save();

        //get receive line
       $data_line = receiveorderline::where('line_no','=',$data->id)->get();

       //get stockkeeping line
       if(count($data_line) > 0){
            foreach($data_line as $el){
                //get variance code
                $variantcode = product_vavaincode_view::addSelect('product_no', 'stock_unit_of_measure_code','variant_unit_of_measure_code', 'quantity_per_unit')
                                                            ->where('product_no', '=', $el->product_no)
                                                            ->where('variant_unit_of_measure_code', '=', $el->unit_of_measure_code)->first();

                //reverse stock keeping
                $stock_keeping = stockkeeping::where('line_no','=',$el->id)->first();
                $stock_keeping->statuse = "reverse";
                $stock_keeping->save();

                $backQty = floatval($stock_keeping->inventory) / floatval($variantcode->quantity_per_unit);

                $purchase_line = purchealine::where('document_no','=',$data->document_no)
                                            ->where('product_no','=',$stock_keeping->product_no)
                                            ->first();
                $purchase_line->inventory_recetived = floatval($purchase_line->inventory_recetived) - floatval($backQty);
                $purchase_line->inventory = floatval($purchase_line->inventory) + floatval($backQty);
                $purchase_line->statue = "open";
                $purchase_line->save();
            }
       }
       //get purchase document
       $purchase = purcheaorder::where('document_no','=',$data->document_no)->first();
       $purchase->statue = "pending";
       $purchase->save();

       return ['status'=> true, 'message' => "Reverse successfully"];
       
   
    }
}
