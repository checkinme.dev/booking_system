<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\stock_count_line;
use App\Models\stock_count;
use App\Models\stock_store;
use App\Models\Serail;
use App\Models\viewprodoctstock;
use App\Models\setup;
use App\Models\stocktransaction;
use App\Models\stockkeeping;
use App\Models\product_vavaincode_view;



class StockCountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serail_no = Serail::where('id', '=', 'StockCount')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int)$Sart_at + (int)$End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
            $serail_no = $serail_no . "0";
        }
        $serail_no = $serail_no . $newCode;

        $setup = setup::first();

        $purchase = stock_count::create([
            'document_no'  => $serail_no,
            'document_type'=> $request->document_type,
            'curency_code' => $setup->main_Currency,
            'statue'       => 'open',
            'created_by'   => 'Chhin Pov'
        ]);
        if ($purchase) {
            $purline = stock_count_line::create([
                'document_no' => $serail_no,
                'description' => $request->description,
                'total_amount' => '1',
                'inventory'  => '1',
                'unit_price' => '1',
                'created_by' => $request->updeted_by ,
                'statue'     => 'open',
            ]);
            if ($purline) {
                $serilano = Serail::where('id', '=', 'StockCount')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
                if ($serilano) {
                    $purchase = stock_count::where('document_no', '=', $serail_no)->get();
                    return $purchase;
                } else {
                    return ['statue :' => "faile Create"];
                }
            } else {
                return ['statue :' => "faile Create"];
            }
        } else {
            return ['statue :' => "faile Create"];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function stockview($id)
    {
        $purchase_view = stock_count::where('document_no', '=', $id)->first();
        if ($purchase_view) {
            return $purchase_view;
        } else {
            return ['statue :' => "Note Date"];
        }
    }
    public function stocklineview($id)
    {
        $posts = stock_count_line::where('document_no', '=', $id)->paginate(10);
        if ($posts) {
            return $posts;
        } else {
            return ['statue :' => "Note Date"];
        }
    }
    public function editStocklineview($id)
    {
        $purline = stock_count_line::where('document_no', '=', $id)->get();
        if ($purline) {
            return $purline;
        } else {
            return ['statue :' => "Note Date"];
        }
    }
    public function updatestock($id, Request $request)
    {
        $purline = stock_count::find($id);
        $purline->document_type = $request->document_type;
        $purline->description = $request->description;
        $purline->total_amount = $request->total_amount;
        $purline->curency_code = $request->curency_code;
        $purline->statue = $request->statue;
        $purline->created_by = 'chhin pov';
        $purline->save();
        if($purline) {
            return $purline;
        } else {
            return ['statue :' => "Note Date"];
        }
    }

    public function updatestockline($id, Request $request)
    {
        $stock = viewprodoctstock::where('product_no','=',$request->product_no)->first();

        $purline = stock_count_line::find($id);
        $purline->document_type = $request->document_type;
        $purline->product_no = $request->product_no;
        $purline->description = $request->description;
        $purline->line_no = $request->line_no;
        $purline->unit_of_measure_code = $request->unit_of_measure_code;
        $purline->unit_price = $request->unit_price;
        $purline->inventory = $stock->inventory;
        $purline->inventory_count = $request->inventory_count;
        $purline->inventory_new = $request->inventory_new;
        $purline->credit_balance = $request->credit_balance; 
        $purline->debit_balance = $request->debit_balance; 
        if($request->total_amount == null){
            $purline->total_amount = 0;
        }
        else{
            $purline->total_amount = $request->total_amount;
        } 
        $purline->curency_code = $request->curency_code;
        $purline->remark = $request->remark;
        $purline->created_by = $request->created_by;
        $purline->save();
        if ($purline) {
            $purline = stock_count_line::orderBy('id', 'asc')->where('document_no', '=', $purline->document_no)->get();
            return $purline;
        } else {
            return ['statue :' => "Note Date"];
        }
    }
    public function bookingcountstock($id){
            $stock_count = stock_count::where('document_no','=',$id)->where('statue','=','open')->first();
            $stock_count->statue = 'close'; 
            $stock_count->save(); 
            

            // if($stock_count){
            //     $stock_count_lines = stock_count_line::where('document_no', '=',$id)->get();

            //     return response()->json(['stock_count' => $stock_count, 'lines' => $stock_count_lines]);
            // }else{
            //     return response()->json(['message' => "empty data"]);
            // }
            
            if($stock_count){
                $stock_count_line = stock_count_line::where('document_no', '=',$id)->get();
                foreach ($stock_count_line as $stockkeeping) {
                    $stockkeep = stockkeeping::orderBy('id', 'asc')->where('product_no','=',$stockkeeping->product_no)->where('statuse', '=','open')->first();    
                    // return $stockkeep;                     
                   $id = $stockkeep ->id;
                    $count = $stockkeeping->inventory_count;
                    $instock = $stockkeep->inventory;
                    if(doubleval($count)<doubleval($instock)){
                        $instock = doubleval($instock) - doubleval($count);
                        $stockkeep = stockkeeping::find($id);
                        $stockkeep->inventory = $count;
                        $stockkeep->save();
                    }else{
                        while (doubleval($count) >= doubleval($instock)) {
                            $instock = doubleval($count) - doubleval($instock);
                            if($instock == 0){
                                $stockkeep->inventory = $instock;
                                $stockkeep->statuse = 'close';
                                $stockkeep->save();
                            }else{
                                $stockkeep->inventory = 0;
                                $stockkeep = stockkeeping::find($id);
                                $stockkeep->statuse = 'close';
                                $stockkeep->save();
                                if($stockkeep){
                                    $count = doubleval($count) - doubleval($instock);
                                    $stockkeep = stockkeeping::orderBy('id', 'asc')->where('product_no', '=',$stockkeeping->product_no)->where('statuse', '=','open')->first(); 
                                    // return $stockkeep;                                            
                                    $id = $stockkeep ->id;
                                    $instock = $stockkeep->inventory;
                                    if(doubleval($count)<doubleval($instock)){
                                        $instock = doubleval($instock) - doubleval($count);
                                        $stockkeep = stockkeeping::find($id);
                                        $stockkeep->inventory = $count;
                                        $stockkeep->save();
                                    }
                            }
                        }
                    }
                }
                $debit = '0';
                if(doubleval($stockkeeping['credit_balance']) == 0){

                     $debit = $stockkeeping->debit_balance;

                }else{

                     $debit = $stockkeeping->credit_balance;
                }
                $stocktransaction = stocktransaction::create([
                    'document_no'          => $stockkeeping['document_no'],
                    'document_type'        => $stockkeeping['document_type'],
                    'product_no'           => $stockkeeping['product_no'],
                    'description'          => $stockkeeping['description'],
                    'unit_of_measure_code' => $stockkeeping['unit_of_measure_code'],
                    'unit_price'           => $stockkeeping['unit_price'],
                    'inventory'            => $debit,
                    'total_amount'         => $stockkeeping['total_amount'],
                    'curency_code'         => $stockkeeping['curency_code'],
                    'remark'               => $stockkeeping['document_type']
                    ]);
                   if($stocktransaction){

                   }else{

                   }
            }
        }
    }
    public function addrowstockline($id)
    {
        $purline = stock_count_line::create([
            'document_no' => $id,
            'total_amount' => '1',
            'inventory' => '1',
            'unit_price' => '1',
        ]);
        if ($purline) {
            $serilano = stock_count_line::where('document_no', '=', $id)->get();
            return $serilano;
        } else {
            return ['statue :' => "Note Date"];
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getValicode($id)
    {
        $PrinceLink = product_vavaincode_view::where('product_no','=',$id)->where('status','=','stock')->first();
        return  $PrinceLink;
    }
    public function getStock($id)
    {
        $PrinceLink = stock_store::where('product_no','=',$id)->first();
        return  $PrinceLink;
    }


    public function getviewscockcount(Request $request){
        if($request->name == ''){
            $purchase_view = stock_count::orderBy('document_no', 'desc')->paginate(15);
            $print = stock_count::orderBy('document_no', 'desc')->get();

            return ['stock'=>$purchase_view, 'print' => $print];
        }
        else if($request->name != ''){
            $result = stock_count::orderBy('id','desc')
                                ->where('document_no','LIKE',$request->name .'%')
                                ->orwhere('document_type','LIKE',$request->name .'%')
                                ->orwhere('description','LIKE',$request->name .'%')
                                ->paginate(15); 
            $print = stock_count::orderBy('id','desc')
                    ->where('document_no','LIKE',$request->name .'%')
                    ->orwhere('document_type','LIKE',$request->name .'%')
                    ->orwhere('description','LIKE',$request->name .'%')
                    ->get();
            return ['stock'=>$result, 'print' => $print];
        }
    }
    public function searchStockCount($search){
        $result = stock_count::orderBy('id','asc')
                                ->where('document_no','LIKE',$search.'%')
                                ->orwhere('document_type','LIKE',$search.'%')
                                ->orwhere('description','LIKE',$search.'%')
                                ->paginate(15);
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function deletestockcount($id){
        $item = stock_count::where('id','=',$id)->where('statue','!=','close')->first();
        if($item){
            $item->delete();
            return response()->json(['status' => "Deleted successfully"]);
        }else{
            return response()->json(['status' => "faile to delete"]); 
        }
    }
    public function deleteLine($id){
       $item = stock_count_line::where('id','=',$id)->first();
       $document_no = $item->document_no;
       $item->delete();
       return stock_count_line::where('document_no','=',$document_no)->get();
    }
    public function createcountList($id, Request $request){
        $item = stock_count_line::where('id','=',$id)->first();
        $item->product_no = $request->product_no;
        $item->description = $request->description;
        $item->unit_of_measure_code = $request->stock_unit_of_measure_code;
        $item->unit_price = $request->unit_price;
        $item->inventory = $request->inventory;

    }
    public function saveStock(Request $request){
        $mainDoc = stock_count::where('document_no','=',$request->document_no)->first();

        $document = stock_count_line::where('document_no','=',$request->document_no)->get();
        if($document){
            if(count($document) > 0){
                foreach($document as $el){
                    $over = 0;
                    $over = floatval($el->inventory_count) - floatval($el->inventory);
                    
                    $product = stockkeeping::orderBy('exprit_date','asc')
                    ->where('exprit_date','!=',null)
                    ->where('statuse','=','open')
                    ->where('product_no','=',$el->product_no)->first();

                    $product->inventory = floatval($product->inventory) + floatval($over);
                    $product->inventory_old = $over;
                    $product->save();

                    $stocktransaction = stocktransaction::create([
                        'document_no'          => $el['document_no'],
                        'document_type'        => $el['document_type'],
                        'product_no'           => $el['product_no'],
                        'description'          => $el['description'],
                        'unit_of_measure_code' => $el['unit_of_measure_code'],
                        'unit_price'           => $el['unit_price'],
                        'inventory'            => $over,
                        'total_amount'         => $el['total_amount'],
                        'curency_code'         => $el['curency_code'],
                        'remark'               => $el['remark']
                        ]);
                }
                $mainDoc->statue = 'close';
                $mainDoc->save();
            }
            return ['status' => true, 'message' => 'submit stock successfully'];
        }
        else{
            return ['status'=>false,'message' => 'faile submit stock'];
        }
    }
}