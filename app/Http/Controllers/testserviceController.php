<?php

namespace App\Http\Controllers;

use App\Models\product;
use App\Models\Serail;
use App\Models\setup;
use App\Models\testtingitem;
use Illuminate\Http\Request;
use App\Models\subtest;
use Carbon\Carbon;

class TestserviceController extends Controller
{
    public function create($created_by, Request $request)
    {
        // $photo = $this->uploadPhoto($request);
        $serail_no = Serail::where('id', '=', 'Products')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code​​​_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int) $Sart_at + (int) $End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int) $Code​​​_qty - strlen($newCode)); ++$i) {
            $serail_no = $serail_no.'0';
        }
        $serail_no = $serail_no.$newCode;
        $setup = setup::first();
        $product = product::create([
            'id' => $serail_no,
            'product_no' => $serail_no,
            'reorder_point' => '100',
            'description' => 'Test Name',
            'created_by' => $created_by,
            'type' => 'ServiceTest',
            'curency_code' => $setup->main_Currency,
            'status' => 'Open',
            'inactived' => 'No',
        ]);
        if ($product) {
            $sex = ['Men', 'Women', 'Chaildren'];
            for ($i = 0; $i < count($sex); ++$i) {
                $productvariantcode = testtingitem::create([
                    'product_no' => $serail_no,
                    'description' => 'Test Name',
                    'test_type' => $sex[$i],
                    'created_by' => $created_by,
                    'status' => 'Open',
                ]);
            }

            if ($productvariantcode) {
                $serilano = Serail::where('id', '=', 'Products')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
                if ($serilano) {
                    $product = product::where('product_no', '=', $serail_no)->get();

                    return $product;
                } else {
                    return ['statue :' => 'faile Create'];
                }
            } else {
                return ['statue :' => 'faile Create'];
            }
        } else {
            return ['statue :' => 'faile Create'];
        }
    }

    public function update($id, Request $request)
    {
        $unitcode = testtingitem::find($id);
        $unitcode->description = $request->description;
        $unitcode->normal_value = $request->normal_value;
        $unitcode->save();
        if ($unitcode) {
            return ['statue :' => 'SUCCESSE'];
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function destroy($id)
    {
        $unitcode = product::where('product_no', '=', $id);
        $unitcode->delete();
        if ($unitcode) {
            $testtingitem = testtingitem::where('product_no', '=', $id);
            $testtingitem->delete();
            if ($testtingitem) {
                return ['statue :' => 'SUCCESSE'];
            } else {
                return ['statue :' => 'Note Date'];
            }
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function gettestservice($id)
    {
        $unitcode = testtingitem::where('product_no', '=', $id)->get();

        return $unitcode;
    }

    //create sub service test
    public function createSubtest($created_by, Request $request){

        $subTestList = [];
        $genders = ['Men', 'Women', 'Chaildren'];
        $id = 'SUB'.Carbon::now()->format('his');

        if($request->product_no != null || $request->product_no != ''){
            //create subtest
            $subtest = subtest::create([
                'product_no' => $request->product_no,
                'sub_product_no' => $id,
                'description' => 'Test Name',
                'stock_unit_of_measure_code'=> '',
                'created_by' => $created_by,
            ]);
            if($subtest){
                //create subtest list
                foreach ($genders as $gender) {
                    $subtest_list =  testtingitem::create([
                                        'product_no' => $id,
                                        'description' => 'Test Name',
                                        'test_type' => $gender,
                                        'created_by' => $created_by,
                                        'normal_value' => '',
                                        'status' => 'Open',
                                    ]);
                    //push every list into array
                    array_push($subTestList, $subtest_list);
                }
                return ['subtest' => $subtest, 'list' =>$subTestList];
            }
        }
        else{
            return response()->json(['status'=>false,'message'=>'No product_no to create sub service test']);
        }
    }

    public function getOneSubtest($sub_product_no){
        $subtest = subtest::where('sub_product_no','=',$sub_product_no)->first();
        $sublist = testtingitem::where('product_no','=',$sub_product_no)->get();
        return ['subtest' => $subtest, 'list' => $sublist];
    }
    
    //get subtest with list
    public function getSubtest($id){
        $list = [];
        $subtest = subtest::where('product_no','=',$id)->get();
      //  return $subtest;
        if(count($subtest) > 0){
            foreach($subtest as $subtest_element){
                $item = testtingitem::where('product_no','=',$subtest_element->sub_product_no)->get();
                array_push($list , array('subtest' => $subtest_element, 'item' => $item));
            }
        }
        return $list;
    }
    public function getAllsubtest($id){
        $subtest = subtest::orderBy('created_at','desc')->where('product_no','=',$id)->get();
        return $subtest;
    }
    //udpate subtest
    public function updateSubtest($id,Request $request){
        $subtest = subtest::find($id);
        $subtestItem = testtingitem::where('product_no','=',$subtest->sub_product_no)->get();
        if(count($subtestItem) > 0){
            $subtest->update([
                'description' => $request->description,
                'stock_unit_of_measure_code' => $request->stock_unit_of_measure_code,
                'updated_by' => $request->updated_by
            ]);
            foreach($subtestItem as $el){
                $el->update(['description' => $request->description, 'updated_by' => $request->updated_by]);
            }
            return ['status' => true, 'message' => 'Updated successfully']; 
        }else{
            return ['status' => false, 'message' => 'Nothing to update'];
        }
    }
    public function deleteSubtest($id){
        $subtest = subtest::find($id);
        if($subtest){
            $subtest_lists = testtingitem::where('product_no','=',$subtest->sub_product_no)->get();
            if(count($subtest_lists) > 0){
                foreach($subtest_lists as $el){
                    $el->delete();
                }
            }
            $subtest->delete();
            return ['status' => true, 'message' => 'Deleted successfully'];
        }else{
            return ['status' => false, 'message' => 'cannot get any item by id to delete'];
        }
    }

}
