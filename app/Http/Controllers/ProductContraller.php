<?php

namespace App\Http\Controllers;

use App\Models\cat_produc;
use App\Models\product;
use App\Models\productbom;
use App\Models\productvariantcode;
use App\Models\Serail;
use App\Models\setup;
use App\Models\unitofmeasure;
use App\Models\viewprodoctstock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProductContraller extends Controller
{
    public function createID(Request $request)
    {
        $idp = product::create([
            'id' => $request['product_no'],
            'product_no' => $request['product_no'],
            'type' => 'Products',
            'created_by' => $request['created_by'],
            'inactived' => 'No',
        ]);

        return response()->json([$idp]);
    }

    public function create(Request $request)
    {
        // $photo = $this->uploadPhoto($request);
        $serail_no = Serail::where('id', '=', 'Products')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int) $Sart_at + (int) $End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int) $Code_qty - strlen($newCode)); ++$i) {
            $serail_no = $serail_no.'0';
        }
        $serail_no = $serail_no.$newCode;
        $Currency = setup::first();
        $product = product::create([
            'id' => $serail_no,
            'product_no' => $serail_no,
            'reorder_point' => '100',
            'created_by' => $request->created_by,
            'curency_code' => $Currency->main_Currency,
            'type' => 'Products',
            'image_url' => $request->image_url,
            'inactived' => 'No',
        ]);
        $setup = setup::first();
        if ($product) {
            for ($i = 0; $i < 2; ++$i) {
                if ($i == 0) {
                    $productvariantcode = productvariantcode::create([
                        'product_no' => $serail_no,
                        'status' => 'stock',
                        'created_by' => $request->created_by,
                        'curency_code' => $setup->main_Currency,
                        'image_url' => $request->image_url,
                        'inactived' => 'No',
                    ]);
                } else {
                    $productvariantcode = productvariantcode::create([
                        'product_no' => $serail_no,
                        'status' => 'unit',
                        'created_by' => $request->created_by,
                        'curency_code' => $setup->main_Currency,
                        'image_url' => $request->image_url,
                        'inactived' => 'No',
                    ]);
                }
            }
            if ($productvariantcode) {
                $serilano = Serail::where('id', '=', 'Products')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
                if ($serilano) {
                    $product = product::where('product_no', '=', $serail_no)->get();

                    return $product;
                } else {
                    return ['statue :' => 'faile Create'];
                }
            } else {
                return ['statue :' => 'faile Create'];
            }
        } else {
            return ['statue :' => 'faile Create'];
        }
    }

    public function crateUnite($id)
    {
        $serail_no = product::where('product_no', '=', $id)->first();
        $decrip = $serail_no->description;
        $company = setup::first();

        $checkVarcode = productvariantcode::where('product_no', '=', $id)->get();
        if (count($checkVarcode) > 0) {
            $productvariantcode = productvariantcode::create([
                'product_no' => $id,
                'description' => $decrip,
                'created_by' => $serail_no->created_by,
                'status' => 'unit',
                'curency_code' => $company['main_Currency'],
            ]);
        } else {
            $productvariantcode = productvariantcode::create([
                'product_no' => $id,
                'description' => $decrip,
                'created_by' => $serail_no->created_by,
                'status' => 'stock',
            ]);
        }
        if ($productvariantcode) {
            $serail_no = productvariantcode::where('product_no', '=', $id)->get();

            return $serail_no;
        } else {
            return ['statue :' => 'faile Create'];
        }
    }

    public function getUnit($id)
    {
        $productvariantcode = productvariantcode::where('product_no', '=', $id)->get();
        if ($productvariantcode) {
            return $productvariantcode;
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function savelink($id)
    {
        $productvariantcode = productvariantcode::where('id', '=', $id)->get();
        $productvariantcode->save();
        if ($productvariantcode) {
            return $productvariantcode;
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function viewpro(Request $request)
    {
        $product_view = viewprodoctstock::orderBy('id', 'desc')->where('type', '=', 'Products')->where('inactived', '!=', 'Yes')->paginate(15);

        return $product_view;
    }
    public function viewAllProduct(Request $request){
        $object = array();
        if($request->name == '' && $request->brand == '' && $request->unit == '' && $request->category == '' && $request->supplier == ''){
            $product_view = viewprodoctstock::orderBy('created_at', 'desc')
                                            ->where('type', '!=', 'ServiceTest')
                                            ->where('inactived', '!=', 'Yes')
                                            ->paginate(15);
            $print = viewprodoctstock::orderBy('created_at', 'desc')
                                    ->where('type', '!=', 'ServiceTest')
                                    ->where('inactived', '!=', 'Yes')
                                    ->get();
            if(count($print) > 0){
                foreach($print as $el){
                    $productvariantcode = productvariantcode::where('product_no', '=', $el->product_no)
                                                            ->get();
                    array_push($object, array('el' =>$el,'items'=>$productvariantcode ));
                }
            }

            return ['data'=>$product_view, 'print' => $print, 'print_detail'=> $object];
        }
        else if($request->name != ''){
            $product_view = viewprodoctstock::whereIn('product_no', viewprodoctstock::select('product_no')
                                        ->where('product_no','LIKE',$request->name.'%')
                                        ->orwhere('description','LIKE',$request->name.'%')
                                        ->orwhere('description_2','LIKE',$request->name.'%')
                                        ->orwhere('unit_price','LIKE',$request->name.'%')
                                        ->get()
                                    )
                                    ->Where('inactived', '!=', 'Yes')
                                    ->where('type', '!=', 'ServiceTest')
                                    ->paginate(15);
            $print = viewprodoctstock::whereIn('product_no', viewprodoctstock::select('product_no')
                                        ->where('product_no','LIKE',$request->name.'%')
                                        ->orwhere('description','LIKE',$request->name.'%')
                                        ->orwhere('description_2','LIKE',$request->name.'%')
                                        ->orwhere('unit_price','LIKE',$request->name.'%')
                                        ->get()
                                    )
                                    ->Where('inactived', '!=', 'Yes')
                                    ->where('type', '!=', 'ServiceTest')
                                    ->get();
             if(count($print) > 0){
                foreach($print as $el){
                    $productvariantcode = productvariantcode::where('product_no', '=', $el->product_no)
                                                            ->get();
                    array_push($object, array('el' =>$el,'items'=>$productvariantcode ));
                }
            }

            return ['data'=>$product_view, 'print' => $print, 'print_detail'=> $object];
        }
        else if($request->brand != ''){
            $product_view = viewprodoctstock::Where('inactived', '!=', 'Yes')
                                             ->where('type', '!=', 'ServiceTest')
                                             ->where('brand_code', 'LIKE',$request->brand.'%')
                                             ->paginate(15);
            $print = viewprodoctstock::Where('inactived', '!=', 'Yes')
                                        ->where('type', '!=', 'ServiceTest')
                                        ->where('brand_code', 'LIKE',$request->brand.'%')
                                        ->get();
            if(count($print) > 0){
                foreach($print as $el){
                    $productvariantcode = productvariantcode::where('product_no', '=', $el->product_no)
                                                            ->get();
                    array_push($object, array('el' =>$el,'items'=>$productvariantcode ));
                }
            }

            return ['data'=>$product_view, 'print' => $print, 'print_detail'=> $object];
        }
        else if($request->unit != ''){
            $product_view = viewprodoctstock::Where('inactived', '!=', 'Yes')
                                             ->where('type', '!=', 'ServiceTest')
                                             ->where('stock_unit_of_measure_code', 'LIKE',$request->unit.'%')
                                             ->paginate(15);
            $print = viewprodoctstock::Where('inactived', '!=', 'Yes')
                                             ->where('type', '!=', 'ServiceTest')
                                             ->where('stock_unit_of_measure_code', 'LIKE',$request->unit.'%')
                                             ->get();
            if(count($print) > 0){
                foreach($print as $el){
                    $productvariantcode = productvariantcode::where('product_no', '=', $el->product_no)
                                                            ->get();
                    array_push($object, array('el' =>$el,'items'=>$productvariantcode ));
                }
            }

            return ['data'=>$product_view, 'print' => $print, 'print_detail'=> $object];
        }
        else if($request->category != ''){
            $product_view = viewprodoctstock::Where('inactived', '!=', 'Yes')
                                            ->where('type', '!=', 'ServiceTest')
                                            ->where('cat_code', 'LIKE',$request->category.'%')
                                            ->paginate(15);
            $print = viewprodoctstock::Where('inactived', '!=', 'Yes')
                                            ->where('type', '!=', 'ServiceTest')
                                            ->where('cat_code', 'LIKE',$request->category.'%')
                                            ->get();
            if(count($print) > 0){
                foreach($print as $el){
                    $productvariantcode = productvariantcode::where('product_no', '=', $el->product_no)
                                                           ->get();
                     array_push($object, array('el' =>$el,'items'=>$productvariantcode ));
                }
            }
            return ['data'=>$product_view, 'print' => $print, 'print_detail'=> $object];
        }
        else if($request->supplier != ''){
                $product_view = viewprodoctstock::Where('inactived', '!=', 'Yes')
                                                ->where('type', '!=', 'ServiceTest')
                                                ->where('sup_code', 'LIKE',$request->supplier.'%')
                                                ->paginate(15);
                $print = viewprodoctstock::Where('inactived', '!=', 'Yes')
                                                ->where('type', '!=', 'ServiceTest')
                                                ->where('sup_code', 'LIKE',$request->supplier.'%')
                                                ->get();
                if(count($print) > 0){
                    foreach($print as $el){
                        $productvariantcode = productvariantcode::where('product_no', '=', $el->product_no)
                                                            ->get();
                        array_push($object, array('el' =>$el,'items'=>$productvariantcode ));
                    }
                }
                return ['data'=>$product_view, 'print' => $print, 'print_detail'=> $object];
        }


    }

    public function edit($id)
    {
        $currency = setup::first();
        $posts = product::where('product_no', '=', $id)->first();
        if ($posts) {
            $posts->curency_code = $currency->main_Currency;
            $posts->save();
            return $posts;
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function editlink($id)
    {
        $productvariantcode = productvariantcode::where('product_no', '=', $id)->get();

        if ($productvariantcode) {
            return $productvariantcode;
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function update($id, Request $request)
    {
        $product = product::find($id);
        $dir = $this->directory();
        if ($request->image_url != $product['image_url']) {
            File::delete($dir.$product['image_url']);
            $photo = $this->uploadPhoto($request);
        } else {
            $photo = $request->image_url;
        }
        $product->update([
            'description' => $request->description,
            'description_2' => $request->description_2,
            'image_url' => $photo,
            'type' => $request->type,
            'stock_unit_of_measure_code' => $request->stock_unit_of_measure_code,
            'purche_unit_of_measure_code' => $request->purche_unit_of_measure_code,
            'sup_code' => $request->sup_code,
            'brand_code' => $request->brand_code,
            'group_code' => $request->group_code,
            'cat_code' => $request->cat_code,
            'reorder_point' => $request->reorder_point,
            'unit_price' => $request->unit_price,
            'variant_code' => $request->variant_code,
            'inactived' => $request->inactived,
            'updeted_by' => $request->updeted_by,
        ]);
        if ($product) {
            $productvariantcode = productvariantcode::where('product_no', '=', $product->product_no);
            $productvariantcode->update([
                'description' => $product->description,
            ]);
        }

        return $product;
    }

    public function updateProductLinke($id, Request $request)
    {
        $pro = productvariantcode::find($id);
        $dir = $this->directory();
        if ($request->image_url != $pro['image_url']) {
            File::delete($dir.$pro['image_url']);
            $photo = $this->uploadPhoto($request);
        } else {
            $photo = $request->image_url;
        }
        $pro->update([
            'id' => $request->id,
            'product_no' => $request->product_no,
            'description' => $request->description,
            'description_2' => $request->description_2,
            'image_url' => $photo,
            'variant_unit_of_measure_code' => $request->variant_unit_of_measure_code,
            'stock_unit_of_measure_code' => $request->stock_unit_of_measure_code,
            'quantity_per_unit' => $request->quantity_per_unit,
            'unit_price' => $request->unit_price,
            'curency_code' => $request->curency_code,
            'inactived' => $request->inactived,
            'updeted_by' => $request->updeted_by,
        ]);

        return $pro;
    }

    public function destLink($id)
    {
        $productvariantcode = productvariantcode::where('id', '=', $id)->where('status', '=', 'unit')->first();
        $product_no = $productvariantcode->product_no;
        $productvariantcode->delete();
        if ($productvariantcode) {
            $productvariantcode = productvariantcode::where('product_no', '=', $product_no)->get();
            if ($productvariantcode) {
                return $productvariantcode;
            } else {
                return ['statue :' => 'Note Date'];
            }
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function saveDataFformexcel(Request $request)
    {
        $product = product::create([
            'id' => $request['product_no'],
            'product_no' => $request['product_no'],
            'description' => $request['description'],
            'product_barcode' => $request['product_barcode'],
            'description_2' => $request['description_2'],
            'stock_unit_of_measure_code' => $request['stock_unit_of_measure_code'],
            'purche_unit_of_measure_code' => $request['purche_unit_of_measure_code'],
            'bom_no' => $request['bom_no'],
            'reorder_point' => $request['reorder_point'],
            'sup_code' => $request['sup_code'],
            'brand_code' => $request['brand_code'],
            'group_code' => $request['group_code'],
            'cat_code' => $request['cat_code'],
            'unit_price' => $request['unit_price'],
            'type' => 'Products',
            'inactived' => $request['inactived'],
            'created_by' => $request['created_by'],
        ]);
        if ($product) {
            return ['statue :' => 'success'];
        } else {
            return ['statue :' => 'notsuccess'];
        }
    }

    public function saveDataFformexcel1(Request $request)
    {
        $product = product::create([
            'id' => $request['product_no'],
            'product_no' => $request['product_no'],
            'description' => $request['description'],
            'product_barcode' => $request['product_barcode'],
            'description_2' => $request['description_2'],
            'stock_unit_of_measure_code' => $request['stock_unit_of_measure_code'],
            'purche_unit_of_measure_code' => $request['purche_unit_of_measure_code'],
            'bom_no' => $request['bom_no'],
            'reorder_point' => $request['reorder_point'],
            'sup_code' => $request['sup_code'],
            'brand_code' => $request['brand_code'],
            'group_code' => $request['group_code'],
            'cat_code' => $request['cat_code'],
            'unit_price' => $request['unit_price'],
            'type' => 'ServiceTest',
            'inactived' => $request['inactived'],
            'created_by' => $request['created_by'],
        ]);
        if ($product) {
            return ['statue :' => 'success'];
        } else {
            return ['statue :' => 'notsuccess'];
        }
    }

    public function SaveDataLinkexcel(Request $request)
    {
        $productvariantcode = productvariantcode::create([
            'product_no' => $request['product_no'],
            'description' => $request['description'],
            'description_2' => $request['description_2'],
            'variant_unit_of_measure_code' => $request['variant_unit_of_measure_code'],
            'stock_unit_of_measure_code' => $request['stock_unit_of_measure_code'],
            'quantity_per_unit' => $request['quantity_per_unit'],
            'unit_price' => $request['unit_price'],
            'curency_code' => $request['curency_code'],
            'status' => $request['status'],
            'inactived' => $request['inactived'],
            'created_by' => $request['created_by'],
        ]);
        if ($productvariantcode) {
            return ['statue :' => 'success'];
        } else {
            return ['statue :' => 'notsuccess'];
        }
    }

    public function makeDirectory($directory)
    {
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    public function directory()
    {
        return $dir = 'img'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR;
    }

    public function uploadPhoto(Request $request)
    {
        $dir = $this->directory();
        $file = $request->image_url;
        $this->makeDirectory($dir);
        $photo = time().'.'.explode(';', explode('/', $file)[1])[0];
        Image::make($file)->save(public_path($dir).$photo);

        return $photo;
    }

    public function getboomline($id)
    {
        $productvariantcode = productvariantcode::groupBy('variant_unit_of_measure_code')
            ->addSelect('variant_unit_of_measure_code')
            ->whereNotIn('variant_unit_of_measure_code', productbom::select('bom_unit_of_measure_code')->where('product_no', '=', $id)->where('bom_unit_of_measure_code', '=', '')->get())
            ->where('product_no', '=', $id)
            ->get();

        return $productvariantcode;
    }

    public function getlinkunit($id)
    {
        $productvariantcode = unitofmeasure::whereNotIn('unit_code', productvariantcode::select('variant_unit_of_measure_code')
            ->where('product_no', '=', $id)->get())
            ->where('type', '=', 'Size')->where('inactived', '!=', 'Yes')
            ->get();
        if (count($productvariantcode) < 1) {
            $productvariantcode = unitofmeasure::where('type', '=', 'Size')->where('inactived', '!=', 'Yes')->get();

            return $productvariantcode;
        }

        return $productvariantcode;
    }

    public function productsearch(Request $request)
    {
        $product_view = product::orderBy('product_no', 'asc')
            ->addSelect('id', 'product_no', 'product_barcode', 'description', 'description_2', 'image_url', 'stock_unit_of_measure_code', 'purche_unit_of_measure_code', 'bom_no', 'reorder_point', 'sup_code', 'brand_code', 'group_code', 'cat_code', 'variant_code', 'unit_price', 'inactived', 'is_deleted', 'created_by', 'updete_by', 'delete_by', 'created_at', 'updated_at')
            ->whereIn('product_no', product::select('product_no')
                ->where('description', 'LIKE', "%{$request->description}%")
                ->where('description_2', 'LIKE', "%{$request->description_2}%")
                ->where('stock_unit_of_measure_code', 'LIKE', "%{$request->stock_unit_of_measure_code}%")
                ->where('purche_unit_of_measure_code', 'LIKE', "%{$request->purche_unit_of_measure_code}%")
                ->where('group_code', 'LIKE', "%{$request->group_code}%")
                ->where('cat_code', 'LIKE', "%{$request->cat_code}%")
                ->where('inactived', 'LIKE', "%{$request->inactived}%")
                ->get())->paginate(9);

        return $product_view;
    }

    public function getProductCAt($id)
    {
        $productvariantcode = cat_produc::where('product_no', '=', $id)->first();
        if ($productvariantcode) {
            return $productvariantcode;
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function productSell(Request $request)
    {
        $pro = viewprodoctstock::orderBy('id', 'desc')->where('type', '=', 'Products')->where('inactived', '!=', 'Yes')->paginate(50);
        foreach ($pro as $key) {
          $productvariantcode = productvariantcode::addSelect("id","variant_unit_of_measure_code","unit_price","curency_code")->where("product_no","=",$key->product_no)->get();
          $key["size"] = $productvariantcode;
        }
        return $pro;
    }

    public function Productprescriton()
    {
        $productvariantcode = product::get();
        if ($productvariantcode) {
            return $productvariantcode;
        } else {
            return ['statue :' => 'Note Date'];
        }
    }

    public function ProductDiagnos(Request $request)
    {
        $product = viewprodoctstock::orderBy('product_no', 'desc')
            ->where('type', '=', 'Products')
            ->where('inactived', '!=', 'Yes')
            ->whereIn(
                'product_no',
                viewprodoctstock::select('product_no')
                    ->where('product_no', 'LIKE', '%'.$request->search.'%')
                    ->orWhere('description', 'LIKE', '%'.$request->search.'%')
                    ->orWhere('group_code', 'LIKE', '%'.$request->search.'%')
                    ->get()
            )->Where('inactived', '!=', 'Yes')
            ->paginate(15);

        return $product;
    }

    public function SearchMedicine($search)
    {
        $result = viewprodoctstock::orderBy('id', 'desc')
            ->where('type', '=', 'Products')
            ->whereIn(
                'product_no',
                viewprodoctstock::select('product_no')
                    ->where('product_no', 'LIKE', $search.'%')
                    ->orwhere('description', 'LIKE', $search.'%')
                    ->orwhere('group_code', 'LIKE', '%'.$search.'%')
                    ->get()
            )->Where('inactived', '!=', 'Yes')
            ->paginate(20);

        return $result;
    }

    public function searchProductByDiagnosis($search)
    {
        $myArray = explode(',', $search);

        $product = [];
        for ($i = 0; count($myArray) >= $i; ++$i) {
            if (count($myArray) == 1) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 2) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 3) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 4) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 5) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 6) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 7) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 8) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 9) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[8].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 10) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[8].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[9].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 11) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[8].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[9].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[10].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 12) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[8].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[9].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[10].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[11].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 13) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[8].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[9].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[10].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[11].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[12].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 14) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[8].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[9].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[10].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[11].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[12].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[13].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 15) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[8].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[9].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[10].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[11].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[12].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[13].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[14].'%')
                    ->paginate(50);
            } elseif (count($myArray) == 16) {
                $product = viewprodoctstock::select('*')
                    ->where('type', '=', 'Products')
                    ->where('group_code', 'LIKE', '%'.$myArray[0].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[1].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[2].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[3].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[4].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[5].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[6].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[7].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[8].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[9].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[10].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[11].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[12].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[13].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[14].'%')
                    ->orwhere('group_code', 'LIKE', '%'.$myArray[15].'%')
                    ->paginate(50);
            }
        }

        return $product;
    }

    public function getTypeProduct(Request $request)
    {
        // return $datav = viewprodoctstock::orderBy('id', 'desc')->Where('inactived', '=', 'Yes')->get();
        $data = viewprodoctstock::orderBy('id', 'desc')->where('type', '=', 'Products')
        ->Where('inactived', '!=', 'Yes')->paginate(15);

        return $data;
    }



    public function Show($id)
    {
        $data = product::where('type', '=', 'ServiceTest')
            ->where('product_no', '=', $id)->first();

        return $data;
    }

    public function getMetariels(Request $request)
    {
        $result = product::orderBy('id', 'desc')
            ->where('type', '=', 'Materials')
            ->where('inactived','!=','Yes')
            ->get();

        return $result;
    }

    public function getMetarialUnit($id, Request $request)
    {
        $result = productvariantcode::where('product_no', '=', $id)
            ->get(['variant_unit_of_measure_code', 'unit_price']);

        return $result;
    }

    public function searchMetarial($search, Request $request)
    {
        if ($search != '') {
            $result = product::where('type', '=', 'Materials')
                ->where('product_no', 'LIKE', $search.'%')
                ->orwhere('description', 'LIKE', $search.'%')
                ->orwhere('description_2', 'LIKE', $search.'%')
                ->get();

            return $result;
        }
    }

    public function searchService(Request $request)
    {
        if ($request->name != '') {
            $result = product::orderBy('product_no', 'DESC')
                ->where('type', '=', 'ServiceTest')
                ->whereIn(
                    'product_no',
                    product::select('product_no')
                        ->where('product_no', 'LIKE', $request->name.'%')
                        ->orwhere('description', 'LIKE', $request->name.'%')
                        ->orwhere('group_code', 'LIKE', $request->name.'%')
                        ->get()
                )->paginate(12);

            return $result;
        }
        if ($request->unit_price != '' || $request->unit_price != null) {
            $check1 = explode(',', $request->unit_price);
            $result2 = product::where('type', '=', 'ServiceTest')->where('unit_price', $check1[0], $check1[1])
                ->orderBy('product_no', 'desc')->paginate(12);

            return $result2;
        }
    }

    public function getProduct(Request $request)
    {
        $result = product::where('type', '=', 'Products')->where('inactived', '!=', 'Yes')->get();

        return $result;
    }

    public function searchProductType($search)
    {
        $result = product::where('type', '!=', 'ServiceTest')
            ->whereIn(
                'product_no',
                product::select('product_no')
                    ->where('product_no', 'LIKE', $search.'%')
                    ->orwhere('description', 'LIKE', $search.'%')
                    ->get()
            )
            ->Where('inactived', '!=', 'Yes')->get();

        return $result;
    }

    public function getProductStockCount(Request $request)
    {
        $product = viewprodoctstock::orderBy('id', 'DESC')
            ->where('type', '!=', 'ServiceTest')
            ->where('inactived','!=','Yes')
            ->get();

        return $product;
    }

    public function SearchProductStockCount($search)
    {
        $result = product::orderBy('id', 'DESC')
            ->where('type', '!=', 'ServiceTest')
            ->where('description', 'LIKE', $search.'%')
            ->get();

        return $result;
    }

    public function searchViewProduct(Request $request)
    {
        return $product = product::orderBy('prodcut_no', 'desc')->whereIn('product_no', product::select('product_no')
        ->Where('product_no', 'LIKE', '%'.$request->search.'%')
             ->orWhere('description', 'LIKE', '%'.$request->search.'%')
              ->orWhere('description_2', 'LIKE', '%'.$request->search.'%')
              ->orWhere('unit_price', 'LIKE', '%'.$request->search.'%')
              ->Where('inactived', '!=', 'Yes')
              ->get()
        )->paginate(15);
    }

    public function status_update(Request $request)
    {
        $active = product::select('inactived')->where('product_no', '=', $request->id)->first();
        if ($active->inactived == '1') {
            $inactived = '0';
        } else {
            $inactived = '1';
        }

        $val = product::where('product_no', $id)->update([
'inactived' => $request['inactived'],
        ]);

        return $val;
    }

    public function posSearch(Request $request){
        if(count($request->cate_list) > 0){
            $data = viewprodoctstock::orderBy('product_no','desc')->where('type','=','Products')->where('inactived','!=','Yes')
                                    ->whereIn('cat_code',$request->cate_list)->paginate(50);
            foreach ($data as $key) {
                $productvariantcode = productvariantcode::addSelect("id","variant_unit_of_measure_code","unit_price","curency_code")->where("product_no","=",$key->product_no)->get();
                $key["size"] = $productvariantcode;
            }
                                    return $data;
        }
        else{
            $data = viewprodoctstock::orderBy('product_no','desc')->where('type','=','Products')
                                    ->where('inactived','!=','Yes')
                                    ->paginate(50);
                                    foreach ($data as $key) {
                                        $productvariantcode = productvariantcode::addSelect("id","variant_unit_of_measure_code","unit_price","curency_code")->where("product_no","=",$key->product_no)->get();
                                        $key["size"] = $productvariantcode;
                                    }
                                    return $data;
        }

    }

}
