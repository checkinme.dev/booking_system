<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\product;
use App\Models\servicetest_view;
use App\Models\testtingitem;

class ServiceTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->name == '' && $request->unit_price == ''){
            $product_ServiceTest = product::orderBy('id', 'DESC')
                                            ->where('type' , '=', 'ServiceTest')
                                         ->paginate(15);
            return $product_ServiceTest;
        }
        else if ($request->name != '') {
            $result = product::orderBy('product_no', 'DESC')
                ->where('type', '=', 'ServiceTest')
                ->whereIn(
                    'product_no',
                    product::select('product_no')
                        ->where('product_no', 'LIKE', $request->name.'%')
                        ->orwhere('description', 'LIKE', $request->name.'%')
                        ->orwhere('group_code', 'LIKE', $request->name.'%')
                        ->get()
                )->paginate(15);

            return $result;
        }
        else if ($request->unit_price != '' || $request->unit_price != null) {
            $check1 = explode(',', $request->unit_price);
            $result2 = product::where('type', '=', 'ServiceTest')->where('unit_price', $check1[0], $check1[1])
                ->orderBy('product_no', 'desc')->paginate(15);

            return $result2;
        }

        
    }
    public function getServicetestview()
    {
        $servicetest_view = servicetest_view::orderBy('id', 'DESC')
                                            ->paginate(12);
        return $servicetest_view;

    }
    public function getAllViewProduct(Request $request)
    {   
        $data = array();


        if($request->name == '' && $request->unit_price == ''){
            $product = product::orderBy('id', 'DESC')
            ->where('type' , '=', 'ServiceTest')
           ->get();
        }
       
        else if ($request->name != '') {
           $product = product::orderBy('product_no', 'DESC')
                            ->where('type', '=', 'ServiceTest')
                            ->whereIn('product_no',product::select('product_no')
                                                        ->where('product_no', 'LIKE', $request->name . '%')
                                                        ->orwhere('description', 'LIKE', $request->name . '%')
                                                        ->orwhere('group_code', 'LIKE', $request->name . '%')
                                                        ->get()
                                    )->get();
                           
                            }
       else if ($request->unit_price != '' || $request->unit_price != null) {
                $check1 = explode(",", $request->unit_price);
                $product = product::where('type', '=', 'ServiceTest')->where('unit_price', $check1[0], $check1[1])
                                ->orderBy('product_no', 'desc')->get();
                                
        }else{

        }

      foreach($product as $el){
        $result = testtingitem::where('product_no', '=', $el->product_no)
                                ->get();
        array_push($data, array('el'=>$el,'items'=>$result));
      }
      return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
