<?php

namespace App\Http\Controllers;

use App\Models\StockOnHand;
use Illuminate\Http\Request;

class StockOnHandController extends Controller
{
    public function getIndex(Request $request)
    {
        if($request->product_no == '' && $request->mos == '' && $request->amc_amount == '' && $request->unit_price == '' && $request->stock_onhand == '')
        {
            $get = StockOnHand::orderBy('product_no', 'desc')->paginate(15);
            $print = StockOnHand::orderBy('product_no', 'desc')->get();
            return ['stock'=> $get, 'print' => $print];
        }
        else if ($request->product_no != null) {
            $result = StockOnHand::where('product_no', 'LIKE', '%'.$request->product_no.'%')
                                ->orwhere('description', 'LIKE', '%'.$request->product_no.'%')
                                ->orderBy('product_no', 'desc')->paginate(15);
            $print = StockOnHand::where('product_no', 'LIKE', '%'.$request->product_no.'%')
                                ->orwhere('description', 'LIKE', '%'.$request->product_no.'%')
                                ->orderBy('product_no', 'desc')->get();

            return ['stock'=> $result, 'print' => $print];
        }

        else if ($request->mos != null) {
            $check = explode(',', $request->mos);
            $result = StockOnHand::where('mos', $check[0], $check[1])
            ->orderBy('product_no', 'desc')->paginate(15);
            $print = StockOnHand::where('mos', $check[0], $check[1])
            ->orderBy('product_no', 'desc')->get();

            return ['stock'=> $result, 'print' => $print];
        }
        else if ($request->amc_amount != null) {
            $check1 = explode(',', $request->amc_amount);
            $result1 = StockOnHand::where('amc_amount', $check1[0], $check1[1])
                        ->orderBy('product_no', 'desc')->paginate(15);
           $print = StockOnHand::where('amc_amount', $check1[0], $check1[1])
                                ->orderBy('product_no', 'desc')->get();

            return ['stock'=> $result1, 'print' => $print];
        }
        else if ($request->unit_price != null) {
            $result = StockOnHand::where('unit_price', 'LIKE', '%'.$request->unit_price.'%')
                                    ->orderBy('product_no', 'desc')->paginate(15);
            $print = StockOnHand::where('unit_price', 'LIKE', '%'.$request->unit_price.'%')
                                ->orderBy('product_no', 'desc')->get();

            return ['stock'=> $result, 'print' => $print];
        }

        else if ($request->stock_onhand != null) {
            $check2 = explode(',', $request->stock_onhand);
            $result2 = StockOnHand::where('stock_onhand', $check2[0], $check2[1])
                                ->orderBy('product_no', 'desc')->paginate(15);
            $print = StockOnHand::where('stock_onhand', $check2[0], $check2[1])
                                ->orderBy('product_no', 'desc')->get();

           return ['stock'=> $result2, 'print' => $print];
          //  return $result2;
        }
        
    
        
    }

    public function index(Request $request)
    {
        if ($request->product_no != null) {
            $result = StockOnHand::where('product_no', 'LIKE', '%'.$request->product_no.'%')
                                ->orwhere('description', 'LIKE', '%'.$request->product_no.'%')
            ->orderBy('product_no', 'desc')->paginate(15);

            return $result;
        }

        if ($request->mos != null) {
            $check = explode(',', $request->mos);
            $result = StockOnHand::where('mos', $check[0], $check[1])
            ->orderBy('product_no', 'desc')->paginate(15);

            return $result;
        }
        if ($request->amc_amount != null) {
            $check1 = explode(',', $request->amc_amount);
            $result1 = StockOnHand::where('amc_amount', $check1[0], $check1[1])
           ->orderBy('product_no', 'desc')->paginate(15);

            return $result1;
        }
        if ($request->unit_price != null) {
            $result = StockOnHand::where('unit_price', 'LIKE', '%'.$request->unit_price.'%')
            ->orderBy('product_no', 'desc')->paginate(15);

            return $result;
        }

        if ($request->stock_onhand != null) {
            $check2 = explode(',', $request->stock_onhand);
            $result2 = StockOnHand::where('stock_onhand', $check2[0], $check2[1])
           ->orderBy('product_no', 'desc')->paginate(15);

            return $result2;
        }
    }
}
