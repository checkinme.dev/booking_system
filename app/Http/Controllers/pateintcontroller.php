<?php

namespace App\Http\Controllers;

use App\Models\diagnosis_list;
use App\Models\pateint;
use App\Models\prescriptions;
use App\Models\prescription_view;
use App\Models\Serail;
use Illuminate\Http\Request;
use App\Models\product_vavaincode_view;
use App\Models\StockOnHand;
use App\Models\productvariantcode;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class pateintcontroller extends Controller
{
    public function create(Request $request)
    {

        $serail_no = Serail::where('id', '=', 'Patient')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int)$Sart_at + (int)$End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
            $serail_no = $serail_no . "0";
        }
        $serail_no = $serail_no . $newCode;
       if($request['pateinid'] != '' && $request['pateinid'] != null){
            $pateint = pateint::create([
                'pateinid' => $request['pateinid'],
                'lstname' => $request['lstname'],
                'firstname' => $request['firstname'],
                'birstdate' => $request['birstdate'],
                'sex' => $request['sex'],
                'age' => $request['age'],
                'allergies' => $request['allergies'],
                'history_disease' => $request['history_disease'],
                'phone1' => $request['phone1'],
                'phone2' => $request['phone2'],
                'email' => $request['email'],
                'address' => $request['address'],
                'address2' => $request['address2'],
                'village' => $request['village'],
                'commune' => $request['commune'],
                'district' => $request['district'],
                'pro_city' => $request['pro_city'],
                'region' => $request['region'],
                'remark' => $request['remark'],
                'status' => $request['status'],
                'created_by' => $request['created_by'],
            ]);
       }else{
        $pateint = pateint::create([
            'pateinid' => $serail_no,
            'lstname' => $request['lstname'],
            'firstname' => $request['firstname'],
            'birstdate' => $request['birstdate'],
            'sex' => $request['sex'],
            'age' => $request['age'],
            'allergies' => $request['allergies'],
            'history_disease' => $request['history_disease'],
            'phone1' => $request['phone1'],
            'phone2' => $request['phone2'],
            'email' => $request['email'],
            'address' => $request['address'],
            'address2' => $request['address2'],
            'village' => $request['village'],
            'commune' => $request['commune'],
            'district' => $request['district'],
            'pro_city' => $request['pro_city'],
            'region' => $request['region'],
            'remark' => $request['remark'],
            'status' => $request['status'],
            'created_by' => $request['created_by'],
        ]);
       }

       $serilano = Serail::where('id', '=', 'Patient')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
        if ($pateint) {
            return pateint::orderBy('id', 'DESC')->get();
        } else {
            return ['statue :' => "faile"];
        }
    }

    public function index()
    {
        $pateint = pateint::orderBy('id', 'DESC')->paginate(10);
        $print = pateint::orderBy('id', 'DESC')->get();
        return response()->json(['data' => $pateint, 'print' => $print]);
    }

    public function delete($id)
    {
        $employee = pateint::find($id);
        $employee->delete();
        if ($employee) {
            return pateint::orderBy('id', 'DESC')->get();
        } else {
            return ['statue :' => "faile"];
        }
    }

    public function update($id, Request $request)
    {
        $employee = pateint::findOrFail($id);
        $employee->pateinid = $request->pateinid;
        $employee->lstname = $request->lstname;
        $employee->firstname = $request->firstname;
        $employee->birstdate = $request->birstdate;
        $employee->sex = $request->sex;
        $employee->age = $request->age;
        $employee->phone1 = $request->phone1;
        $employee->phone2 = $request->phone2;
        $employee->allergies = $request->allergies;
        $employee->history_disease = $request->history_disease;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->address2 = $request->address2;
        $employee->village = $request->village;
        $employee->commune = $request->commune;
        $employee->district = $request->district;
        $employee->pro_city = $request->pro_city;
        $employee->region = $request->region;
        $employee->remark = $request->remark;
        $employee->status = $request->status;
        $employee->save();
        if($request->preid != ''){
            $data = prescriptions::where('preid','=', $request->preid)->first();
            $data->age = $request->age;
            $data->save();
        }

        if ($employee) {
            return pateint::orderBy('id', 'DESC')->get();
        } else {
            return ['statue :' => "faile"];
        }
    }
    public function test(Request $request)
    {
        $employee = pateint::find($request->id);
        $employee->pateinid = $request->pateinid;
        $employee->lstname = $request->lstname;
        $employee->firstname = $request->firstname;
        $employee->birstdate = $request->birstdate;
        $employee->sex = $request->sex;
        $employee->age = $request->age;
        $employee->phone1 = $request->phone1;
        $employee->phone2 = $request->phone2;
        $employee->allergies = $request->allergies;
        $employee->history_disease = $request->history_disease;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->address2 = $request->address2;
        $employee->village = $request->village;
        $employee->commune = $request->commune;
        $employee->district = $request->district;
        $employee->pro_city = $request->pro_city;
        $employee->region = $request->region;
        $employee->remark = $request->remark;
        $employee->status = $request->status;
        $employee->save();
        if ($employee) {
            return pateint::orderBy('id', 'DESC')->get();
        } else {
            return ['statue :' => "faile"];
        }
//    $pre = pateint::findOrFail($vcs);
//         $pre->update([
//             'pateinid' => $request->pateinid,
//             'lstname' => $request->lstname,
        // 'visit_date' => $request->visit_date,
        // 'BP' => $request->BP,
        // 'Pr' => $request->Pr,
        // 'rr' => $request->rr,
        // 'Spo2' => $request->Spo2,
        // 'T' => $request->T,
        // 'status' => $request->status,
        // 'appointment_date' => $request->appointment_date,
        // 'remark' => $request->remark,
        // 'updated_at' => $request->updated_at,
        // ]);
        // return $pre;
    }

    public function pdiagnosis(Request $request)
    {
        $pateint = pateint::where('pateinid', '=', $request->pateinid)->first();
        // if ($pateint == null) {$pat = 0;} else { $pat = 1;}
        $serail_no = Serail::where('id', '=', 'Prescriptions')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        if ($pateint) {
            $newCode = (int) $Sart_at + (int) $End_code;
            $serail_no = $Prifixcode;
            for ($i = 0; $i < ((int) $Code_qty - strlen($newCode)); $i++) {
                $serail_no = $serail_no . "0";
            }
            $serail_no = $serail_no . $newCode;

            $date = Carbon::now();
            $product = prescriptions::create([
                'preid' => $serail_no,
                'pateinid' => $pateint['pateinid'],
                'created_by' => $request['created_by'],
                'type' => $request['type'],
                'age' => $pateint->age,
                'status' => 'open',
                'visit_date' => $date
            ]);
            if ($product) {
                if ($product) {
                    $serilano = Serail::where('id', '=', 'Prescriptions')->first();
                    $serilano->end_code = $newCode;
                    $serilano->save();
                    if ($serilano) {
                        $product = prescription_view::where('preid', '=', $serail_no)->first();
                        return $product;
                    } else {
                        return ['statue :' => "faile Create"];
                    }
                } else {
                    return ['statue :' => "faile Create"];
                }
            } else {
                return ['statue :' => "faile Create"];
            }
        } else {
            $pateint2 = pateint::create([
                'pateinid' => $request['pateinid'],
                'lstname' => $request['lstname'],
                'firstname' => $request['firstname'],
                'birstdate' => $request['birstdate'],
                'sex' => $request['sex'],
                'age' => $request['age'],
                'allergies' => $request['allergies'],
                'history_disease' => $request['history_disease'],
                'phone1' => $request['phone1'],
                'phone2' => $request['phone2'],
                'email' => $request['email'],
                'address' => $request['address'],
                'address2' => $request['address2'],
                'village' => $request['village'],
                'commune' => $request['commune'],
                'district' => $request['district'],
                'pro_city' => $request['pro_city'],
                'region' => $request['region'],
                'remark' => $request['remark'],
                'status' => $request['status'],
                'created_by' => $request['created_by'],
            ]);
            if ($pateint2) {

                $newCode = (int) $Sart_at + (int) $End_code;
                $serail_no = $Prifixcode;
                for ($i = 0; $i < ((int) $Code_qty - strlen($newCode)); $i++) {
                    $serail_no = $serail_no . "0";
                }
                $serail_no = $serail_no . $newCode;
                $date = Carbon::now();
                $product = prescriptions::create([
                    'preid' => $serail_no,
                    'pateinid' => $pateint2['pateinid'],
                    'created_by' => $request['created_by'],
                    'type' => $request['type'],
                    'age' => $pateint2->age,
                    'visit_date' => $date,
                    'status' => 'open',
                ]);
                if ($product) {
                    if ($product) {
                        $serilano = Serail::where('id', '=', 'Prescriptions')->first();
                        $serilano->end_code = $newCode;
                        $serilano->save();
                        if ($serilano) {
                            $prescription_view = prescription_view::where('preid', '=', $serail_no)->first();
                            return $prescription_view;
                        } else {
                            return ['statue :' => "faile Create"];
                        }
                    } else {
                        return ['statue :' => "faile Create"];
                    }
                } else {
                    return ['statue :' => "faile Create"];
                }
            } else {
                return ['statue :' => "faile"];
            }
        }

    }

    public function getDataPateint($id)
    {
        $pateint = pateint::where('pateinid', '=', $id)->get();
        return $pateint;
    }
    public function diagnosisCreate(Request $request)
    {
        $product = diagnosis_list::where('preid','=',$request['preid'])
                                ->where('product_no','=',$request->product_no)
                                ->get();
        
        if(count($product) == 0){
            $vavaincode = product_vavaincode_view::where('product_no', '=',$request->product_no)
                                            ->where('variant_unit_of_measure_code', '=',$request->unit)
                                            ->first();
        if(!$vavaincode)  return  ['status' => false,'inventory' => 0]; 
         $inven = $vavaincode->quantity_per_unit;
         $StockOnHand = StockOnHand::where('product_no', '=',$request->product_no)->first();
         $stock_onhand = $StockOnHand->stock_onhand;
         $stock_unit = $StockOnHand->stock_uint;
         $inventory = doubleval($inven)* doubleval($request['qty']);
         if(doubleval($inventory)<=doubleval($stock_onhand) && $vavaincode && $stock_onhand){
                $diagnosis = diagnosis_list::create([
                    'preid' => $request['preid'],
                    'product_no' => $request['product_no'],
                    'description' => $request['description'],
                    'dose' => $request['dose'],
                    'morning' => $request['morning'],
                    'afternoon' => $request['afternoon'],
                    'evening' => $request['evening'],
                    'night' => $request['night'],
                    'day' => $request['day'],
                    'qty' => $request['qty'],
                    'qty_instock' => $request['inventory'],
                    'unit' => $request['unit'],
                    'unit_price'=>$vavaincode->unit_price,
                    'amount' => $request['amount'],
                    'created_by' => $request['created_by'],
                ]);
                return ['status' => true, 'inventory' => $stock_onhand.' '.$stock_unit];

       
         }else{
            return ['status' => false,'inventory' => $stock_onhand.' '.$stock_unit];
         }
        }else{
            return ['status' => false,'inventory' => "no", 'message' => 'product already exist'];
        }
    }
    public function diagnosisSelect($id)
    {
        $diagnosis = diagnosis_list::where('preid', '=',$id)->get();
        if ($diagnosis) {
            return $diagnosis;
        } else {
            return ['statue :' => "faile Create"];
        }
    }
    public function diagnosisUpdate($id,Request $request)
    {
        $employee = diagnosis_list::find($id);

        $vavaincode = product_vavaincode_view::where('product_no', '=',$employee->product_no)->where('variant_unit_of_measure_code', '=',$request->unit)->first();
        $inven = $vavaincode->quantity_per_unit;
        $StockOnHand = StockOnHand::where('product_no', '=',$employee->product_no)->first();
        $stock_uint = $StockOnHand->stock_uint;
        $stock_onhand = $StockOnHand->stock_onhand;
        $inventory = doubleval($inven)* doubleval($request['qty']);
        if(doubleval($inventory) <= doubleval($stock_onhand)){
            $employee->product_no = $request->product_no;
            $employee->description = $request->description;
            $employee->dose = $request->dose;
            $employee->morning = $request->morning;
            $employee->afternoon = $request->afternoon;
            $employee->evening = $request->evening;
            $employee->night = $request->night;
            $employee->day = $request->day;
            $employee->qty = $request->qty;
            $employee->unit = $request->unit;
            $employee->unit_price = $request->unit_price;
            $employee->amount = $request->amount;
            $employee->updated_by = $request->updated_by;
            $employee->save();
            return ['status' => true, 'inventory' => $stock_onhand.' '.$stock_uint];
        }
        else {
            return ['status' => false,'inventory' => $stock_onhand.' '.$stock_uint];
        }
    }
    public function searchPatient(Request $request)
    {
        $data = pateint::orderBy('id','DESC')
                                ->where('pateinid','LIKE',$request->pateinid.'%')
                                ->where('lstname','LIKE',$request->lstname.'%')
                                ->where('firstname','LIKE',$request->firstname.'%')
                                ->paginate(10);
        return $data;
    }
    public function createPrescription(Request $request){
        $prescripton = prescription_view::where('type','=','prescription')
                                    ->where('pateinid','=',$request->pateinid)
                                    ->where('status','=','open')->get();
        if(count($prescripton) != 0){
            $medicine = diagnosis_list::where('preid','=',$prescripton[0]->preid)->get();
            return ['status'=>true,'message'=>'Prescription already created!','data'=>$prescripton,'medicine'=>$medicine];
        }else{
            $patient = pateint::where('pateinid','=',$request->pateinid)->first();
            if($patient && $patient != null){
                $serail_no = Serail::where('id', '=', 'Prescriptions')->first();
                $Prifixcode = $serail_no->prefix_code;
                $Code_qty = $serail_no->qty_code;
                $Sart_at = $serail_no->start_code;
                $End_code = $serail_no->end_code;
                $newCode = (int)$Sart_at + (int)$End_code;
                $serail_no = $Prifixcode;
                for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
                    $serail_no = $serail_no . "0";
                }
                 $serail_no = $serail_no . $newCode;
                 
                 $product = prescriptions::create([
                        'preid'      => $serail_no,
                        'pateinid'   => $request['pateinid'],
                        'created_by' => $request['created_by'], 
                        'status'     => 'open',
                        'type' =>'prescription'
                ]);
                if ($product) {
                    if ($product) {
                        $serilano = Serail::where('id', '=', 'Prescriptions')->first();
                        $serilano->end_code = $newCode;
                        $serilano->save();
                        if ($serilano) {
                            $product = prescriptions::where('preid', '=', $serail_no)->first();
                            return $product;
                        } else {
                            return ['status' => false,'message'=> "Not successfully."];
                        }
                    } else {
                        return ['status' => false,'message'=> "Not successfully."];
                    }
                } else {
                    return ['status' => false,'message'=> "Not successfully."];
                }  

                
                // return ['status'=>true,'message'=>]
            }else{
                return ['status' => false,'message'=> "Patient does not exist. Do you want to register new Patient?"];
            }
        }
        
    }
    public function createPatientWithPrescription(Request $request){

             $request->validate([
                    'pateinid'=>'required',
                    'firstname'=>'required',
                    'lstname'=>'required',
                    'sex'=>'required'
                ]);

                $create_patient = pateint::create(
                    ['pateinid'=>$request->pateinid,
                    'firstname'=>$request->firstname,
                    'lstname'=>$request->lstname,
                    'sex'=>$request->sex,
                     'created_by'=>$request->created_by
                    ]
                );
                    $serail_no = Serail::where('id', '=', 'Prescriptions')->first();
                $Prifixcode = $serail_no->prefix_code;
                $Code_qty = $serail_no->qty_code;
                $Sart_at = $serail_no->start_code;
                $End_code = $serail_no->end_code;
                $newCode = (int)$Sart_at + (int)$End_code;
                $serail_no = $Prifixcode;
                for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
                    $serail_no = $serail_no . "0";
                }
                 $serail_no = $serail_no . $newCode;
                 $date = Carbon::now();
                 $product = prescriptions::create([
                        'preid'      => $serail_no,
                        'pateinid'   => $request['pateinid'],
                        'created_by' => $request['created_by'], 
                        'status'     => 'open',
                        'visit_date' => $date,
                        'type' =>'prescription'
                ]);
                if ($product) {
                    if ($product) {
                        $serilano = Serail::where('id', '=', 'Prescriptions')->first();
                        $serilano->end_code = $newCode;
                        $serilano->save();
                        if ($serilano) {
                            $product = prescriptions::where('preid', '=', $serail_no)->first();
                            return $product;
                        } else {
                            return ['status' => false,'message'=> "Not successfully."];
                        }
                    } else {
                        return ['status' => false,'message'=> "Not successfully."];
                    }
                } else {
                    return ['status' => false,'message'=> "Not successfully."];
                } 

                
    }



    public function searchEnterPatient(Request $request){
        if($request->pateinid != ''){

        $prescription = pateint::where('pateinid','=',$request->pateinid)->get();
        if(count($prescription) != 0){
            return ['status'=>1,'message'=>"Pateint already!",'data'=>$prescription[0]];
        }
        if(count($prescription) == 0){

                $requestData = $request->all();
                $validator = Validator::make($requestData, [
                    'pateinid'=>'required',
                    'firstname'=>'required',
                    'lstname'=>'required',
                    'sex'=>'required'
                ]);
                if ($validator->fails()) {
                    return ['status'=>0,'errors'=> $validator->errors()];
                }
                $product = pateint::create([
                    'pateinid'   => $request['pateinid'],
                    'firstname'   => $request['firstname'],
                    'lstname'   => $request['lstname'],
                    'created_by' => $request['created_by'], 
                    'age' => $request->age,
                    'visit_date' =>$date,
                    'status'     => 'open',
                    'type' =>'prescription'
            ]); 
            if( $product){
                return ['status'=>1,'message'=>"Pateint already!",'data'=>$product];
            }
        }
    }else{
        return "Field required";
    }
    }


    public function searchPatientLabo(Request $request){
        if($request->pateinid != ''){

            $prescription = prescriptions::where('type','=','labo')
                                        ->where('pateinid','=',$request->pateinid)
                                        ->where('status','=','open')->get();
            if(count($prescription) != 0){
                $prescriptionview = prescription_view::where('type','=','labo')
                                                    ->where('pateinid','=',$request->pateinid)
                                                    ->where('status','=','open')->get();
                return ['status'=>1,'message'=>"Prescription already created for this patient!",'data'=>$prescriptionview[0]];
            }
            if(count($prescription) == 0){
                $patient = pateint::where('pateinid','=',$request->pateinid)
                                    ->orwhere('lstname', '=', $request->lstname)
                                    ->orwhere('firstname','=', $request->firstname)
                                    ->orwhere('phone1','=',$request->pateinid)
                                    ->first();
                if($patient != null){
                    $serail_no = Serail::where('id', '=', 'Prescriptions')->first();
                    $Prifixcode = $serail_no->prefix_code;
                    $Code_qty = $serail_no->qty_code;
                    $Sart_at = $serail_no->start_code;
                    $End_code = $serail_no->end_code;
                    $newCode = (int)$Sart_at + (int)$End_code;
                    $serail_no = $Prifixcode;
                    for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
                        $serail_no = $serail_no . "0";
                    }

                    $date = Carbon::now();

                    $serail_no = $serail_no . $newCode;
                    
                    $product = prescriptions::create([
                            'preid'      => $serail_no,
                            'pateinid'   => $request['pateinid'],
                            'created_by' => $request['created_by'], 
                            'status'     => 'open',
                            'visit_date' => $date,
                            'type' =>'labo'
                    ]);
                        if ($product) {
                            $serilano = Serail::where('id', '=', 'Prescriptions')->first();
                            $serilano->end_code = $newCode;
                            $serilano->save();
                        }
                    $new_prescription =  prescription_view::where('preid','=',$product->preid)->first();
                    return ['status'=>1,'message'=>"This patient is already exist!",'data'=>$new_prescription];
                }
                else{
                    $object = array('paId'=>'','lstname'=>'','firstname'=>'','sex'=>'','birstdate'=>'','allergies'=>'','history_disease'=>'',
                    'phone1'=>'','phone2'=>'','email'=>'','address'=>'','address2'=>'','village'=>'','commune'=>'','district'=>'','pro_city'=>'',
                    'region'=>'','countday'=>'','visit_days'=>'','visit_month'=>''
                
                );
                    return ['status'=>0,'message'=>"This patient doesn't exist. Do you want to register?",'data'=>$object];
                }
                
            }
        }else{
            return "Field required";
        }

    }



}