<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\testtingitem;
use App\Models\productbom;
use App\Models\prescriptions;
use App\Models\product;
use App\Models\prescription_view;
use App\Models\Serail;
use App\Models\pateint;
use App\Models\prescription_detail;
use App\Models\payment_voucher_list;
use App\Models\payment_voucher;
use App\Models\stock_store;
use App\Models\stockkeeping;
use App\Models\stocktransaction;
use App\Models\Sale;
use App\Models\Saleline;
use Illuminate\Support\Facades\Date;
use Carbon\Carbon;
use App\Models\result_labo_view;
use App\Models\diagnosis_list;

class PrescriptionsController extends Controller
{
   public function index()
    {
       $prescriptions = prescription_view::get();
       return $prescriptions;  
    }
    
   public function indexConvert()
    {
       $prescriptions = prescription_view::where('status','=','Invoice')->get();
       return $prescriptions;  
    }

   public function indexPayment()
    {
       $prescriptions = prescription_view::where('status','=','close')->get();
       return $prescriptions;  
    }

 public function preselectData($id)
    {
       $prescriptions = prescription_view::where('id','=',$id)->first();
       return $prescriptions;  
    }
    public function checkstock(Request $request){
           $stock_store = stock_store::where('product_no','=',$request->product_no)->first();
           $Amount = $stock_store->inventorys;
           $productvariantcode = productvariantcode::where('product_no','=',$request->product_no)
           ->where('variant_unit_of_measure_code','=',$request->unit_code)->first();
           $unit = $productvariantcode->quantity_per_unit;
           $instock = (int)$Amount/(int)$unit;
           return ['instock'=>$instock];
    }
    
    public function prescriptionEdit($id,Request $request)
    {
        $prescriptions = prescriptions::find($id);
        $prescriptions ->Age = $request -> Age;
        $prescriptions ->visit_date = $request -> visit_date;
        $prescriptions ->BP = $request -> BP;
        $prescriptions ->Pr = $request -> Pr;
        $prescriptions ->rr = $request -> rr;
        $prescriptions ->Spo2 = $request -> Spo2;
        $prescriptions ->T = $request -> T;
        $prescriptions ->status = $request -> status;
        $prescriptions ->appointment_date = $request -> appointment_date;
        $prescriptions ->remark = $request -> remark;
        $prescriptions ->description = $request -> description;
        $prescriptions ->save();
        return ['statue :'=>"SUCCESSE"];
        if($prescriptions){

        }else{

        }
        
    }

   public function testservice($id,Request $request)
    {
        $prescriptions = prescriptions::find($id);
        $prescriptions ->Age = $request -> Age;
        $prescriptions ->visit_date = $request -> visit_date;
        $prescriptions ->BP = $request -> BP;
        $prescriptions ->Pr = $request -> Pr;
        $prescriptions ->rr = $request -> rr;
        $prescriptions ->Spo2 = $request -> Spo2;
        $prescriptions ->T = $request -> T;
        $prescriptions ->height = $request -> height;
        $prescriptions ->weight = $request -> weight;
        $prescriptions ->status = $request -> status;
        $prescriptions ->appointment_date = $request -> appointment_date;
        $prescriptions ->remark = $request -> remark;
        $prescriptions ->description = $request -> description;
        $prescriptions ->updated_by = $request -> updated_by;
        $prescriptions ->save();
       
        if($prescriptions){
               return $prescriptions; 
        }else{
             return 'not sussuccesful';
        }
        
    }

    public function createvisit( Request $request){
        $date = Carbon::now();
        // $photo = $this->uploadPhoto($request);
        $serail_no = Serail::where('id', '=', 'Prescriptions')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int)$Sart_at + (int)$End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
            $serail_no = $serail_no . "0";
        }
         $serail_no = $serail_no . $newCode;
         
         $product = prescriptions::create([
                'preid'      => $serail_no,
                'pateinid'   => $request['pateinid'],
                'created_by' => $request['created_by'], 
                'age' => $request['age'],
                'status'     => 'open',
                'type' => $request['type'],
                'visit_date' => $date
        ]);
        if ($product) {
            if ($product) {
                $serilano = Serail::where('id', '=', 'Prescriptions')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
                if ($serilano) {
                    $product = prescriptions::where('preid', '=', $serail_no)->first();
                    return $product;
                } else {
                    return ['statue :' => "faile Create"];
                }
            } else {
                return ['statue :' => "faile Create"];
            }
        } else {
            return ['statue :' => "faile Create"];
        }  
    }
    
      public function prescriptionDelete($id)
    {
      $prescriptions = prescription_detail::where('id', '=', $id);
        $prescriptions -> delete();
        if($prescriptions){
            return ['statue :'=> "Succesfull"];
        }else{
           return ['statue :'=>"faile Delete"];
        } 
    }
      public function prescriptionDetail(Request $request)
    {
       $prescriptions = prescription_detail::get();
       return $prescriptions;  
    }
    
      public function getprecriptoin($id)
    {
        return prescription_view::orderBy('id', 'DESC')
                                    ->where('preid', '=',$id)->first();
    }
       public function prescriptionDetailcreatet($id)
    {
      
       $prescriptions = prescription_detail::create([
             'prescription_id' => $id,
             'del_flg'         => '0',
            ]);
        if($prescriptions){
            return prescription_detail::orderBy('id', 'DESC')->where('preid', '=',$prescriptions->preid)->get();
        }else{
           return ['statue :'=>"faile Delete"];
        } 
    }
    
      public function prescriptionDetailDelete(Request $request)
    {
        $prescriptions = prescription_detail::where('id','=',$request->id)->first();
        $prescriptions -> delete();
        if($prescriptions){
            return ['statue :'=> "Succes full"];
        }else{
           return ['statue :'=>"faile Delete"];
        } 
    }

      public function prescriptionDetailview()
    {
       $prescriptions = prescription_detailview::get();
       return $prescriptions;  
    }
       public function GetprescriptionDetail($id)
    {
       $prescriptions =  prescription_detailview::orderBy('id', 'DESC')->where('prescription_id', '=',$id)->get();
       return $prescriptions;  
    }

       public function GetJusines()
    {
       //$edid = prescriptions::addSelect('jusin_id')->get();
       $jusins =  jusins::orderBy('id', 'DESC')
      //->whereNotIn('jusins_id',$edid)
       ->get();
       return $jusins;  
    }
    // payment
     public function paymentVocher($id,Request $request)
    {
        $serail_no = Serail::where('id', '=', 'Invoice')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int)$Sart_at + (int)$End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
            $serail_no = $serail_no . "0";
        }
        $serail_no = $serail_no . $newCode;
        $statute = $request->paymentMethod;
        if($statute == 'cash') $statute= 'save'; else $statute= 'close';
        $purchase = payment_voucher::create([
            'document' =>"Sell-Invoice",
            'document_type' => 'Prescritions',
            'totale_balanec'=> $request['totale_balanec'],
            'exchane_rate'=> $request['exchane_rate'],
            'currency_code'=> $request['currency_code'],
            'paymentMethod'=> $request['paymentMethod'],
            'exchane_currency'=> $request['exchane_currency'],
            'tax'=> $request['tax'],
            'dicount'=> $request['dicount'],
            'document_no'=> $serail_no,
            'created_by'=>  $request['created_by'],
        ]);
        if ($purchase) {
            $purline = payment_voucher_list::create([
                'document_no' => $serail_no,
                'exchane_rate'=> $request['exchane_rate'],
                'totale_balanec'=> $request['totale_balanec'],
                'currency_code'=> $request['currency_code'],
                'created_by' => $request['created_by'],
                'payment_amount'=>'0',
                'payment_no'=> $id,
                'dicount'=>'0',
                'statue'=>$statute,
            ]);
            if ($purline) {
                $serilano = Serail::where('id', '=', 'Invoice')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
                if ($serilano) {
                     $prescriptions = prescriptions::where('id', '=', $id)->first();
                     $prescriptions ->status = 'close';
                     $prescriptions->save();
                     if($prescriptions){
                        return prescriptions::orderBy('id', 'DESC')->get();
                     }
                } else {
                    return ['statue :' => "faile Create"];
                }
            } else {
                return ['statue :' => "faile Create"];
            }
        } else {
            return ['statue :' => "faile Create"];
        }
    }
    public function reversinvoin($id)
    {
       $prescriptions =  prescription_detailview::orderBy('id', 'DESC')->where('prescription_id', '=',$id)->get();
       return $prescriptions;  
    }
      public function getdatavisit()
    {
       $prescriptions =  prescription_view::orderBy('id', 'DESC')->get();
       return $prescriptions;  
    }
    public function updatPrescription( Request $request ){
    $pre = prescriptions::find($request->id);
        $pre->update([
            'id' => $request->id,
            'age' => $request->age,
            'group_code'=> $request->group_code,
            'visit_date' => $request->visit_date,
            'bp' => $request->bp,
            'pr' => $request->pr,
            'rr' => $request->rr,
            'spo2' => $request->spo2,
            't' => $request->t,
            'width' => $request->width,
            'height' => $request->height,
            'status' => $request->status,
            'appointment_date' => $request->appointment_date,
            'remark' => $request->remark,
            'updated_by' => $request->updated_by,
        ]);
        return $pre;
    }
    public function selectPrescription(Request $request){
        if($request->name == '' && $request->startDate == '' && $request->endDate == ''){
            $items = prescription_view::orderBy('preid','DESC')
                                ->where('type','=','prescription')
                                ->paginate(15);
            $print = prescription_view::orderBy('preid','DESC')
                                    ->where('type','=','prescription')->get();
            return response()->json(['data' => $items, 'print' => $print]);
        }
       else if($request->name != ''){
            $search =explode(' ',$request->name);
            if(count($search) == 1){
                $data = prescription_view::orderBy('preid','DESC')
                                        ->where('type','=','prescription')
                                        ->whereIn('preid',prescription_view::select('preid')
                                            ->where('lstname','LIKE',$search[0].'%')
                                            ->orwhere('group_code','LIKE',$search[0].'%')
                                            ->orwhere('firstname','LIKE',$search[0].'%')
                                            ->orwhere('pateinid','LIKE',$search[0].'%')
                                            ->orwhere('phone1','LIKE',$search[0].'%')
                                            ->orwhere('age','LIKE',$search[0].'%')
                                            ->orwhere('sex','LIKE',$search[0].'%')
                                            ->orwhere('village','LIKE',$search[0].'%')
                                            ->orwhere('address2','LIKE',$search[0].'%')
                                            ->orwhere('commune','LIKE',$search[0].'%')
                                            ->orwhere('district','LIKE',$search[0].'%')
                                            ->orwhere('pro_city','LIKE',$search[0].'%')
                                            ->orwhere('region','LIKE',$search[0].'%')
                                            ->get()
                                        )->paginate(15);
                $print = prescription_view::orderBy('preid','DESC')
                                            ->where('type','=','prescription')
                                            ->whereIn('preid',prescription_view::select('preid')
                                                ->where('lstname','LIKE',$search[0].'%')
                                                ->orwhere('group_code','LIKE',$search[0].'%')
                                                ->orwhere('firstname','LIKE',$search[0].'%')
                                                ->orwhere('pateinid','LIKE',$search[0].'%')
                                                ->orwhere('phone1','LIKE',$search[0].'%')
                                                ->orwhere('age','LIKE',$search[0].'%')
                                                ->orwhere('sex','LIKE',$search[0].'%')
                                                ->orwhere('village','LIKE',$search[0].'%')
                                                ->orwhere('address2','LIKE',$search[0].'%')
                                                ->orwhere('commune','LIKE',$search[0].'%')
                                                ->orwhere('district','LIKE',$search[0].'%')
                                                ->orwhere('pro_city','LIKE',$search[0].'%')
                                                ->orwhere('region','LIKE',$search[0].'%')
                                                ->get()
                                            )->get();
                return response()->json(['data' => $data, 'print' => $print]);
            }
            if(count($search) == 2){
                $data = prescription_view::orderBy('preid','DESC')
                            ->where('type','=','prescription')
                            ->whereIn('preid',prescription_view::select('preid')
                                ->where('firstname','LIKE',$search[0].'%')
                                ->where('lstname','LIKE',$search[1].'%')
                                ->get()
                            )->paginate(15);
                $print = prescription_view::orderBy('preid','DESC')
                            ->where('type','=','prescription')
                            ->whereIn('preid',prescription_view::select('preid')
                                ->where('firstname','LIKE',$search[0].'%')
                                ->where('lstname','LIKE',$search[1].'%')
                                ->get()
                            )->get();
               return response()->json(['data' => $data, 'print' => $print]);
            }
        }
        else if($request->startDate != '' && $request->endDate != '')
        {
            $items = prescription_view::orderBy('preid','DESC')
                                    ->whereBetween('visit_date',[$request->startDate, $request->endDate])
                                    ->where('type','=','prescription')
                                    ->paginate(15);
            $print = prescription_view::orderBy('preid','DESC')
                                    ->whereBetween('visit_date',[$request->startDate, $request->endDate])
                                    ->where('type','=','prescription')
                                    ->get();
           return response()->json(['data' => $items, 'print' => $print]);
        }


        
    }

    public function selectLabo(Request $request)
    {
        if($request->name == '' && $request->startDate == '' && $request->endDate == ''){
            $datas = prescription_view::orderBy('preid','DESC')
                                ->where('type','=','labo')
                                ->paginate(15);
            $print = prescription_view::orderBy('preid','DESC')
                                    ->where('type','=','labo')
                                    ->get();
            return response()->json(['data' => $datas, 'print' => $print]);
        }
        else if($request->name != ''){
            $search =explode(' ',$request->name);
            if(count($search) == 1){
                $data = prescription_view::orderBy('preid','DESC')
                                        ->where('type','=','labo')
                                        ->whereIn('preid',prescription_view::select('preid')
                                            ->where('lstname','LIKE',$search[0].'%')
                                            ->orwhere('group_code','LIKE',$search[0].'%')
                                            ->orwhere('firstname','LIKE',$search[0].'%')
                                            ->orwhere('pateinid','LIKE',$search[0].'%')
                                            ->orwhere('phone1','LIKE',$search[0].'%')
                                            ->orwhere('age','LIKE',$search[0].'%')
                                            ->orwhere('sex','LIKE',$search[0].'%')
                                            ->orwhere('village','LIKE',$search[0].'%')
                                            ->orwhere('address2','LIKE',$search[0].'%')
                                            ->orwhere('commune','LIKE',$search[0].'%')
                                            ->orwhere('district','LIKE',$search[0].'%')
                                            ->orwhere('pro_city','LIKE',$search[0].'%')
                                            ->orwhere('region','LIKE',$search[0].'%')
                                            ->get()
                                        )->paginate(15);

                $print = prescription_view::orderBy('preid','DESC')
                                            ->where('type','=','labo')
                                            ->whereIn('preid',prescription_view::select('preid')
                                                ->where('lstname','LIKE',$search[0].'%')
                                                ->orwhere('group_code','LIKE',$search[0].'%')
                                                ->orwhere('firstname','LIKE',$search[0].'%')
                                                ->orwhere('pateinid','LIKE',$search[0].'%')
                                                ->orwhere('phone1','LIKE',$search[0].'%')
                                                ->orwhere('age','LIKE',$search[0].'%')
                                                ->orwhere('sex','LIKE',$search[0].'%')
                                                ->orwhere('village','LIKE',$search[0].'%')
                                                ->orwhere('address2','LIKE',$search[0].'%')
                                                ->orwhere('commune','LIKE',$search[0].'%')
                                                ->orwhere('district','LIKE',$search[0].'%')
                                                ->orwhere('pro_city','LIKE',$search[0].'%')
                                                ->orwhere('region','LIKE',$search[0].'%')
                                                ->get()
                                            )->get();
                  return response()->json(['data' => $data, 'print' => $print]);
            }
            if(count($search) == 2){
                $data = prescription_view::orderBy('preid','DESC')
                                            ->where('type','=','labo')
                                            ->whereIn('preid',prescription_view::select('preid')
                                                ->where('firstname','LIKE',$search[0].'%')
                                                ->where('lstname','LIKE',$search[1].'%')
                                                ->get()
                                            )->paginate(15);
                $print =  prescription_view::orderBy('preid','DESC')
                                            ->where('type','=','labo')
                                            ->whereIn('preid',prescription_view::select('preid')
                                                ->where('firstname','LIKE',$search[0].'%')
                                                ->where('lstname','LIKE',$search[1].'%')
                                                ->get()
                                            )->get();
                return response()->json(['data' => $data, 'print' => $print]);
            }
        }
        else if($request->startDate != '' && $request->endDate != ''){
            $items = prescription_view::orderBy('preid','DESC')
                                    // ->whereBetween('visit_date',[$request->startDate, $request->endDate])
                                    ->whereDate('visit_date','>=',$request->startDate)
                                    ->whereDate('visit_date','<=',$request->endDate)
                                    ->where('type','=','labo')
                                    ->paginate(15);
            $print = prescription_view::orderBy('preid','DESC')
                                    // ->whereBetween('visit_date',[$request->startDate, $request->endDate])
                                    ->whereDate('visit_date','>=',$request->startDate)
                                    ->whereDate('visit_date','<=',$request->endDate)
                                    ->where('type','=','labo')
                                    ->get();
            return response()->json(['data' => $items, 'print' => $print]);
        }

        
    }
    public function getlaboUnsuccess(Request $request)
    {
        $data = prescription_view::orderBy('preid','DESC')
                                    ->where('type','=','labo')
                                    ->where('status', '!=', 'close')
                                    ->where('status', '!=', 'waiting')
                                    ->paginate(15);
        return $data;
    }
    public function searchPrescription(Request $request)
    {
        if($request->name != ''){
            $search =explode(' ',$request->name);
            if(count($search) == 1){
                $data = prescription_view::orderBy('preid','DESC')
                                        ->where('type','=','prescription')
                                        ->whereIn('preid',prescription_view::select('preid')
                                            ->where('lstname','LIKE','%'.$search[0].'%')
                                            ->orwhere('group_code','LIKE','%'.$search[0].'%')
                                            ->orwhere('firstname','LIKE','%'.$search[0].'%')
                                            ->orwhere('pateinid','LIKE',$search[0].'%')
                                            ->orwhere('phone1','LIKE',$search[0].'%')
                                            ->orwhere('age','LIKE',$search[0].'%')
                                            ->orwhere('sex','LIKE',$search[0].'%')
                                            ->orwhere('village','LIKE',$search[0].'%')
                                            ->orwhere('address2','LIKE',$search[0].'%')
                                            ->orwhere('commune','LIKE',$search[0].'%')
                                            ->orwhere('district','LIKE',$search[0].'%')
                                            ->orwhere('pro_city','LIKE',$search[0].'%')
                                            ->orwhere('region','LIKE',$search[0].'%')
                                            ->get()
                                        )->paginate(15);
                return $data;
            }
            if(count($search) == 2){
                $data = prescription_view::orderBy('preid','DESC')
                ->where('type','=','prescription')
                ->whereIn('preid',prescription_view::select('preid')
                    ->where('firstname','LIKE','%'.$search[0].'%')
                    ->where('lstname','LIKE','%'.$search[1].'%')
                    ->get()
                )->paginate(15);
                return $data;
            }
        }
    
        if($request->startDate != '' && $request->endDate != '')
        {
            $items = prescription_view::orderBy('preid','DESC')
                                    ->whereBetween('visit_date',[$request->startDate, $request->endDate])
                                    ->where('type','=','prescription')
                                    ->paginate(15);
            return $items;
        }
    }
    public function getAllPrescription(Request $request)
    {
        $data = prescription_view::orderBy('preid','DESC')
                                    ->where('type','=','prescription')
                                    ->get();
        return $data;
    }

    public function searchLabo(Request $request)
    {
        if($request->name != ''){
            $search =explode(' ',$request->name);
            if(count($search) == 1){
                $data = prescription_view::orderBy('preid','DESC')
                                        ->where('type','=','labo')
                                        ->whereIn('preid',prescription_view::select('preid')
                                            ->where('lstname','LIKE','%'.$search[0].'%')
                                            ->orwhere('group_code','LIKE','%'.$search[0].'%')
                                            ->orwhere('firstname','LIKE','%'.$search[0].'%')
                                            ->orwhere('pateinid','LIKE',$search[0].'%')
                                            ->orwhere('phone1','LIKE',$search[0].'%')
                                            ->orwhere('age','LIKE',$search[0].'%')
                                            ->orwhere('sex','LIKE',$search[0].'%')
                                            ->orwhere('village','LIKE',$search[0].'%')
                                            ->orwhere('address2','LIKE',$search[0].'%')
                                            ->orwhere('commune','LIKE',$search[0].'%')
                                            ->orwhere('district','LIKE',$search[0].'%')
                                            ->orwhere('pro_city','LIKE',$search[0].'%')
                                            ->orwhere('region','LIKE',$search[0].'%')
                                            ->get()
                                        )->paginate(15);
                return $data;
            }
            if(count($search) == 2){
                $data = prescription_view::orderBy('preid','DESC')
                ->where('type','=','labo')
                ->whereIn('preid',prescription_view::select('preid')
                    ->where('firstname','LIKE','%'.$search[0].'%')
                    ->where('lstname','LIKE','%'.$search[1].'%')
                    ->get()
                )->paginate(15);
                return $data;
            }
        }
    

        if($request->startDate != '' && $request->endDate != '')
        {
            $items = prescription_view::orderBy('preid','DESC')
                                    ->whereBetween('visit_date',[$request->startDate, $request->endDate])
                                    ->where('type','=','labo')
                                    ->paginate(15);
            return $items;
        }
    }
    public function searchAll(Request $request)
    {
        if($request->name != ''){
            $search =explode(' ',$request->name);
            if(count($search) == 1){
                $data = prescription_view::orderBy('preid','DESC')               
                                        ->where('lstname','LIKE','%'.$search[0].'%')
                                        ->orwhere('firstname','LIKE','%'.$search[0].'%')
                                        ->orwhere('group_code','LIKE','%'.$search[0].'%')
                                        ->orwhere('pateinid','LIKE',$search[0].'%')
                                        ->orwhere('age','LIKE',$search[0].'%')
                                        ->orwhere('sex','LIKE',$search[0].'%')
                                        ->orwhere('width','LIKE',$search[0].'%')
                                        ->orwhere('phone1','LIKE',$search[0].'%')
                                        ->orwhere('village','LIKE',$search[0].'%')
                                        ->orwhere('address2','LIKE',$search[0].'%')
                                        ->orwhere('commune','LIKE',$search[0].'%')
                                        ->orwhere('district','LIKE',$search[0].'%')
                                        ->orwhere('pro_city','LIKE',$search[0].'%')
                                        ->orwhere('region','LIKE',$search[0].'%')
                                        ->paginate(15);
                return $data;
            }
            if(count($search) == 2){
                $data = prescription_view::orderBy('preid','DESC')
                                        ->orwhere('firstname','LIKE','%'.$search[0].'%')
                                        ->where('lstname','LIKE','%'.$search[1].'%')
                                        ->paginate(15);
                return $data;
            }
        }
        if($request->startDate != '' && $request->endDate != '')
        {
            $data = prescription_view::orderBy('preid','DESC')
                                    ->whereBetween('visit_date',[$request->startDate,$request->endDate])
                                    ->paginate(15);
            return $data;
        }
        if($request->type != ''){
            $data = prescription_view::orderBy('preid','DESC')
                                    ->where('type','LIKE',$request->type.'%')
                                    ->paginate(15);
            return $data;
        }                
    }
    public function getAlldata(Request $request)
    {
        if($request->name == '' && $request->startDate == '' && $request->endDate == '' && $request->type == ''){
            $data = prescription_view::orderBy('preid','DESC')
                                ->paginate(15);
            $print = prescription_view::orderBy('preid','DESC')->get();
            return ['data' => $data , 'print' => $print];
        }
        else if($request->name != ''){
            $search =explode(' ',$request->name);
            if(count($search) == 1){
                $data = prescription_view::orderBy('preid','DESC')               
                                        ->where('lstname','LIKE',$search[0].'%')
                                        ->orwhere('firstname','LIKE',$search[0].'%')
                                        ->orwhere('group_code','LIKE',$search[0].'%')
                                        ->orwhere('pateinid','LIKE',$search[0].'%')
                                        ->orwhere('age','LIKE',$search[0].'%')
                                        ->orwhere('sex','LIKE',$search[0].'%')
                                        ->orwhere('width','LIKE',$search[0].'%')
                                        ->orwhere('phone1','LIKE',$search[0].'%')
                                        ->orwhere('village','LIKE',$search[0].'%')
                                        ->orwhere('address2','LIKE',$search[0].'%')
                                        ->orwhere('commune','LIKE',$search[0].'%')
                                        ->orwhere('district','LIKE',$search[0].'%')
                                        ->orwhere('pro_city','LIKE',$search[0].'%')
                                        ->orwhere('region','LIKE',$search[0].'%')
                                        ->paginate(15);
                $print = prescription_view::orderBy('preid','DESC')               
                                                    ->where('lstname','LIKE',$search[0].'%')
                                                    ->orwhere('firstname','LIKE',$search[0].'%')
                                                    ->orwhere('group_code','LIKE',$search[0].'%')
                                                    ->orwhere('pateinid','LIKE',$search[0].'%')
                                                    ->orwhere('age','LIKE',$search[0].'%')
                                                    ->orwhere('sex','LIKE',$search[0].'%')
                                                    ->orwhere('width','LIKE',$search[0].'%')
                                                    ->orwhere('phone1','LIKE',$search[0].'%')
                                                    ->orwhere('village','LIKE',$search[0].'%')
                                                    ->orwhere('address2','LIKE',$search[0].'%')
                                                    ->orwhere('commune','LIKE',$search[0].'%')
                                                    ->orwhere('district','LIKE',$search[0].'%')
                                                    ->orwhere('pro_city','LIKE',$search[0].'%')
                                                    ->orwhere('region','LIKE',$search[0].'%')
                                                    ->get();
                return ['data' => $data , 'print' => $print];
            }
           else if(count($search) == 2){
                $data = prescription_view::orderBy('preid','DESC')
                                        ->orwhere('firstname','LIKE',$search[0].'%')
                                        ->where('lstname','LIKE',$search[1].'%')
                                        ->paginate(15);
                $print =prescription_view::orderBy('preid','DESC')
                                            ->orwhere('firstname','LIKE',$search[0].'%')
                                            ->where('lstname','LIKE',$search[1].'%')
                                            ->get(); 
                return ['data' => $data, 'print' => $print];
            }
        }
       else if($request->startDate != '' && $request->endDate != '')
        {
            $data = prescription_view::orderBy('preid','DESC')
                                    // ->whereBetween('visit_date',[$request->startDate,$request->endDate])
                                    ->whereDate('visit_date','>=',$request->startDate)
                                    ->whereDate('visit_date','<=',$request->endDate)
                                    ->paginate(15);
            $print = prescription_view::orderBy('preid','DESC')
                                    ->whereDate('visit_date','>=',$request->startDate)
                                    ->whereDate('visit_date','<=',$request->endDate)
                                    ->get();
                 return ['data' => $data, 'print' => $print];
        }
       else if($request->type != ''){
            $data = prescription_view::orderBy('preid','DESC')
                                    ->where('type','LIKE',$request->type.'%')
                                    ->paginate(15);
            $print = prescription_view::orderBy('preid','DESC')
                                        ->where('type','LIKE',$request->type.'%')
                                        ->get();
            return ['data' => $data, 'print' => $print];
        }   
        
    }
    public function getAppointment(Request $request)
    {
        $currentDate = Carbon::now();
        $formattedDate = $currentDate->format('Y-m-d H:i:s');
        $data = prescription_view::orderBy('appointment_date','DESC')
                                ->where('type','=','labo')
                                ->where('status','=','waiting')
                                ->where('appointment_date','<=',$formattedDate)
                                ->get();
        return $data;
    }

    public function getAppointmentPrescription(Request $request)
    {
        $currentDate = Carbon::now();
        $formattedDate = $currentDate->format('Y-m-d H:i:s');

        $data = prescription_view::orderBy('appointment_date','DESC')
                                ->where('status','=','waiting')
                                ->where('type','=','prescription')
                                ->where('appointment_date','<=',$formattedDate)
                                ->get();

        return $data;
    }

    public function getResult($id)
    {
        $data = result_labo_view::where('labId','=',$id)->get();
        return $data;
    }
    public function createNewAppointment(Request $request)
    {
       // return $request->preid;
       $old_data = prescriptions::where('preid','=',$request->preid)->first();
       $old_data->status = 'close';
       $old_data->save();

        $serail_no = Serail::where('id', '=', 'Prescriptions')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int)$Sart_at + (int)$End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
            $serail_no = $serail_no . "0";
        }
         $serail_no = $serail_no . $newCode;
         $date = Carbon::now();
         
         $product = prescriptions::create([
                'preid'      => $serail_no,
                'pateinid'   => $request['pateinid'],
                'created_by' => $request['created_by'],
                'age' => $request->age,
                'visit_date' => $date,
                'bp' => $request['bp'],
                'pr' => $request['pr'],
                'rr' => $request['rr'],
                'height' => $request['height'],
                'width' => $request['width'],
                'spo2' => $request['spo2'],
                't' => $request['t'],
                'status'     => 'open',
                'type' => $request['type']
        ]);
        if ($product) {
            if ($product) {
                $serilano = Serail::where('id', '=', 'Prescriptions')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
                if ($serilano) {
                    $product = prescriptions::where('preid', '=', $serail_no)->first();
                    return $product;
                } else {
                    return ['statue :' => "faile Create"];
                }
            } else {
                return ['statue :' => "faile Create"];
            }
        } else {
            return ['statue :' => "faile Create"];
        }  
    }
    public function deletePrescription($id){
        $data = prescriptions::where('preid','=',$id)
                                ->where('status','=','open')->first();
        $data->delete();
        if($data){
            $items = diagnosis_list::where('preid','=',$data->preid)->get();
            foreach ($items as $el) {
                $el->delete();
            }
        }
        return "deleted successfully.";
    }
    public function getalertPrescription(Request $request){
        $data = prescription_view::orderBy('preid','DESC')
                                ->where('type','=','prescription')
                                ->where('status','=','open')
                                ->get();
        return $data;
    }

    public function removeAppointment($id,Request $request){
        $data = prescriptions::where('preid','=',$id)
                            ->where('status','=','waiting')
                            ->first();
        $data->status = 'close';
        $data->updated_by = $request->updated_by;
        $data->save();
        if($data){
            return ['message'=>'Remove successfully.'];
        }else{
            return ['message'=>'failed remove'];
        }
    }
     public function addNewAppointment($id,Request $request){
        $data = prescriptions::where('preid','=',$id)
                                ->where('status','=','waiting')
                                ->first();
        $data->status = 'close';
        $data->updated_by = $request->updated_by;
        $data->save();


        $serail_no = Serail::where('id', '=', 'Prescriptions')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int)$Sart_at + (int)$End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int)$Code_qty - strlen($newCode)); $i++) {
            $serail_no = $serail_no . "0";
        }
         $serail_no = $serail_no . $newCode;

         $date = Carbon::now();
         
         $new = prescriptions::create([
                'preid'      => $serail_no,
                'pateinid'   => $data->pateinid,
                'age' => $data->age,
                'created_by' => $request['created_by'],
                'bp' => $data->bp,
                'pr' => $data->pr,
                'rr' => $data->rr,
                'height' => $data->height,
                'width' => $data->width,
                'spo2' => $data->spo2,
                't' => $data->t,
                'visit_date' => $date,
                'status'     => 'open',
                'type' => $request['type']
        ]);

        if($new){
            $serilano = Serail::where('id', '=', 'Prescriptions')->first();
            $serilano->end_code = $newCode;
            $serilano->save();
            return ['message'=>"Created succesffully",'data'=>$new];
        }else{
            return ['message'=>'failed to create'];
        }
     }

     public function getAllAppointment(Request $request){
        if($request->name == '' && $request->startDate == '' && $request->endDate == '' && $request->type == ''){
            $data = prescription_view::orderBy('appointment_date','asc')->where('status','=','waiting')->paginate(15);

            $print = prescription_view::orderBy('appointment_date','asc')->where('status','=','waiting')->get();

            return ['data' => $data, 'print' => $print];
        }
        else if($request->name != ''){
            $search =explode(' ',$request->name);
            if(count($search) == 1){
                $data = prescription_view::orderBy('appointment_date','asc')
                                        ->where('status','=','waiting')
                                        ->whereIn('preid',prescription_view::select('preid')
                                            ->where('lstname','LIKE','%'.$search[0].'%')
                                            ->orwhere('group_code','LIKE','%'.$search[0].'%')
                                            ->orwhere('firstname','LIKE','%'.$search[0].'%')
                                            ->orwhere('pateinid','LIKE',$search[0].'%')
                                            ->orwhere('phone1','LIKE',$search[0].'%')
                                            ->orwhere('age','LIKE',$search[0].'%')
                                            ->orwhere('sex','LIKE',$search[0].'%')
                                            ->orwhere('village','LIKE',$search[0].'%')
                                            ->orwhere('address2','LIKE',$search[0].'%')
                                            ->orwhere('commune','LIKE',$search[0].'%')
                                            ->orwhere('district','LIKE',$search[0].'%')
                                            ->orwhere('pro_city','LIKE',$search[0].'%')
                                            ->orwhere('region','LIKE',$search[0].'%')
                                            ->get()
                                        )->paginate(15);
                $print = prescription_view::orderBy('appointment_date','asc')
                                            ->where('status','=','waiting')
                                            ->whereIn('preid',prescription_view::select('preid')
                                                ->where('lstname','LIKE','%'.$search[0].'%')
                                                ->orwhere('group_code','LIKE','%'.$search[0].'%')
                                                ->orwhere('firstname','LIKE','%'.$search[0].'%')
                                                ->orwhere('pateinid','LIKE',$search[0].'%')
                                                ->orwhere('phone1','LIKE',$search[0].'%')
                                                ->orwhere('age','LIKE',$search[0].'%')
                                                ->orwhere('sex','LIKE',$search[0].'%')
                                                ->orwhere('village','LIKE',$search[0].'%')
                                                ->orwhere('address2','LIKE',$search[0].'%')
                                                ->orwhere('commune','LIKE',$search[0].'%')
                                                ->orwhere('district','LIKE',$search[0].'%')
                                                ->orwhere('pro_city','LIKE',$search[0].'%')
                                                ->orwhere('region','LIKE',$search[0].'%')
                                                ->get()
                                            )->get();
                return response()->json(['data' => $data, 'print' => $print]);
            }
            if(count($search) == 2){
                $data = prescription_view::orderBy('appointment_date','asc')
                            ->where('status','=','waiting')
                            ->whereIn('preid',prescription_view::select('preid')
                                ->where('firstname','LIKE','%'.$search[0].'%')
                                ->where('lstname','LIKE','%'.$search[1].'%')
                                ->get()
                            )->paginate(15);
                $print = prescription_view::orderBy('appointment_date','asc')
                            ->where('status','=','waiting')
                            ->whereIn('preid',prescription_view::select('preid')
                                ->where('firstname','LIKE','%'.$search[0].'%')
                                ->where('lstname','LIKE','%'.$search[1].'%')
                                ->get()
                            )->get();
               return response()->json(['data' => $data, 'print' => $print]);
            }
        }
        else if($request->startDate != '' && $request->endDate != ''){
            $data = prescription_view::orderBy('appointment_date','asc')
                                        ->where('status','=','waiting')
                                        ->whereDate('appointment_date','>=',$request->startDate)
                                        ->whereDate('appointment_date','<=',$request->endDate)
                                        ->paginate(15);
            $print = prescription_view::orderBy('appointment_date','asc')
                                        ->where('status','=','waiting')
                                        ->whereDate('appointment_date','>=',$request->startDate)
                                        ->whereDate('appointment_date','<=',$request->endDate)
                                        ->get();
            return response()->json(['data' => $data, 'print' => $print]);        
        }
        else if($request->type != ''){
            $data = prescription_view::orderBy('appointment_date','asc')
                                        ->where('status','=','waiting')
                                        ->where('type','LIKE','%'.$request->type.'%')
                                        ->paginate(15);
            $print = prescription_view::orderBy('appointment_date','asc')
                                        ->where('status','=','waiting')
                                        ->where('type','LIKE','%'.$request->type.'%')
                                        ->get();
            return response()->json(['data' => $data, 'print' => $print]);                                  
        }
        
     }



}