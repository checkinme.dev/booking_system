<?php

namespace App\Http\Controllers;

use App\Models\close_shift;
use App\Models\pos_closeshift;
use App\Models\Sale;
use App\Models\saleuser;
use App\Models\Saleline;
use App\Models\expenses;
use App\Models\expenreport;
use App\Models\setup;
use App\Models\payment_amount;
use App\Models\role;
use App\Models\Serail;
use App\Models\User;
use App\Models\sale_line_view;
use App\Models\stockkeeping;
use Illuminate\Http\Request;
use App\Models\exchangerate;
use App\Models\expenses_type;

class CloseShiftController extends Controller
{
    public function index()
    {
        $employee = close_shift::get();

        return $employee;
    }
    public function getposcloseshift($created_by)
    {

        $pos = array();
        $prescription = array();
        $labo = array();

        $sum_pos = 0;
        $sum_pres = 0;
        $sum_labo = 0;
        $sum_pos_usd = 0;
        $sum_pres_usd = 0;
        $sum_labo_usd = 0;
        $total = 0;
        $total_usd = 0;

        $close_shift = close_shift::where('created_by', '=', $created_by)->where('statue', '=', 'open')->first();
        if ($close_shift) {
            $pos_sale = pos_closeshift::where('id', '=', $close_shift->id)->where('type', '=', 'pos')->get();
            $prescription_sale = pos_closeshift::where('id', '=', $close_shift->id)->where('type', '=', 'prescription')->get();
            $labo_sale = pos_closeshift::where('id', '=', $close_shift->id)->where('type', '=', 'labo')->get();
            //pos
            if ($pos_sale) {
                foreach ($pos_sale as $el) {
                    $sum_pos += floatval($el->total_amount);
                    $sum_pos_usd += floatval($el->total_amount_usd);
                }
                array_push($pos, array('element' => $pos_sale, 'total' => $sum_pos));
            }

            //prescription
            if ($prescription_sale) {
                foreach ($prescription_sale as $el) {
                    $sum_pres += floatval($el->total_amount);
                    $sum_pres_usd += floatval($el->total_amount_usd);
                }
                array_push($prescription, array('element' => $prescription_sale, 'total' => $sum_pres));
            }
            //Laboratory
            if ($labo_sale) {
                foreach ($labo_sale as $el) {
                    $sum_labo += floatval($el->total_amount);
                    $sum_labo_usd += floatval($el->total_amount_usd);
                }
                array_push($labo, array('element' => $labo_sale, 'total' => $sum_labo));
            }

            $total = $sum_pos + $sum_pres + $sum_labo;
            $total_usd = $sum_pos_usd + $sum_pres_usd + $sum_labo_usd;
            return ['pos' => $pos, 'pre' => $prescription, 'labo' => $labo, 'total' => $total, 'total_usd' => $total_usd];
        } else {
            return ['statue' => false];
        }
    }
    public function detailcloseshif(Request $request)
    {

        $pos = array();
        $prescription = array();
        $labo = array();

        $sum_pos = 0;
        $sum_pres = 0;
        $sum_labo = 0;
        $sum_pos_usd = 0;
        $sum_pres_usd = 0;
        $sum_labo_usd = 0;
        $total = 0;
        $total_usd = 0;
        $close_shift = close_shift::where('id', '=', $request->id)->first();
        if ($close_shift) {
            $pos_sale = pos_closeshift::where('id', '=', $request->id)->where('type', '=', 'pos')->get();
            $prescription_sale = pos_closeshift::where('id', '=', $request->id)->where('type', '=', 'prescription')->get();
            $labo_sale = pos_closeshift::where('id', '=', $request->id)->where('type', '=', 'labo')->get();
            //pos
            if ($pos_sale) {
                foreach ($pos_sale as $el) {
                    $sum_pos += floatval($el->total_amount);
                    $sum_pos_usd += floatval($el->total_amount_usd);
                }
                array_push($pos, array('element' => $pos_sale, 'total' => $sum_pos));
            }

            //prescription
            if ($prescription_sale) {
                foreach ($prescription_sale as $el) {
                    $sum_pres += floatval($el->total_amount);
                    $sum_pres_usd += floatval($el->total_amount_usd);
                }
                array_push($prescription, array('element' => $prescription_sale, 'total' => $sum_pres));
            }
            //Laboratory
            if ($labo_sale) {
                foreach ($labo_sale as $el) {
                    $sum_labo += floatval($el->total_amount);
                    $sum_labo_usd += floatval($el->total_amount_usd);
                }
                array_push($labo, array('element' => $labo_sale, 'total' => $sum_labo));
            }

            $total = $sum_pos + $sum_pres + $sum_labo;
            $total_usd = $sum_pos_usd + $sum_pres_usd + $sum_labo_usd;
            return ['pos' => $pos, 'pre' => $prescription, 'labo' => $labo, 'total' => $total, 'total_usd' => $total_usd];
        } else {
            return ['statue' => false];
        }
    }
    public function detailsaleshif(Request $request)
    {
        $result = Saleline::where('document_no', '=', $request->document_no)
            ->get();
        return $result;
    }

    public function getpriceDatail($id){
        $data = sale::where('document_no','=',$id)->first();
        if($data != null){
            $user = User::where('id','=',$data->created_by)->first();
        }
        return ['data'=>$data, 'user' => $user];
    }



    public function getcheck(Request $request)
    {
        $employee = close_shift::orderBy('id', 'DESC')
            ->where('created_by', '=', $request->created_by)
            ->where('statue', '=', 'open')
            ->first();

        return $employee;
    }

    public function create(Request $request)
    {
        $te = close_shift::where('created_by', '=', $request['created_by'])
            ->where('statue', '=', 'open')
            ->get();
        if (count($te) > 0 || $te == null) {
            return ['statue' => false];
        } else {
            $employee = close_shift::create([
                'description' => $request['description'],
                'deposit' => $request['deposit'],
                'deposit2' => $request['deposit2'],
                'curency_code' => 'USD',
                'curency_code2' => 'Riel',
                'statue' => 'open',
                'created_by' => $request['created_by'],
            ]);
            if ($employee) {
                return response()->json(['statue' => true, 'data' => close_shift::where('statue', '=', 'open')->get()]);
            } else {
                return ['statue' => false];
            }
        }
    }

    public function update($id, Request $request)
    {
        $employee = close_shift::find($id);
        $employee->statue = 'close';
        $employee->deposit = $request->deposit;
        $employee->deposit2 = $request->deposit2;
        $employee->updated_by = $request->updated_by;
        $employee->save();

        return $employee;
    }
    public function getsale($id, Request $request)
    {
        $allData = array();
      
        if($request->name == '' && $request->start == '' && $request->end == ''){
            $result = saleuser::orderBy('created_at','desc')->where('created_by', '=', $id)
                                ->paginate(15);
            $print = saleuser::orderBy('created_at','desc')->where('created_by', '=', $id)
                                 ->get();
            foreach ($print as $el) {
                $data = Saleline::where('document_no', '=', $el->document_no)->get();
                array_push($allData, array("element" => $el, "items" => $data));
            }
            return ['data' => $result, 'print' => $allData];
        }
        else if($request->name != '') {
            $result = saleuser::orderBy('created_at','desc')->where('created_by', '=', $id)
                ->whereIn('document_no', saleuser::select('document_no')
                    ->where('document_no', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('document_type', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('user_name', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('paymeny_method', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('exchane_rate', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('discount', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('total_amount', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('total_amount_usd', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('amoun', 'LIKE', '%' . $request->name . '%')
                    ->get())
                ->paginate(15);
            $print = saleuser::orderBy('created_at','desc')->where('created_by', '=', $id)
            ->whereIn('document_no', saleuser::select('document_no')
                ->where('document_no', 'LIKE', '%' . $request->name . '%')
                ->orwhere('document_type', 'LIKE', '%' . $request->name . '%')
                ->orwhere('user_name', 'LIKE', '%' . $request->name . '%')
                ->orwhere('paymeny_method', 'LIKE', '%' . $request->name . '%')
                ->orwhere('exchane_rate', 'LIKE', '%' . $request->name . '%')
                ->orwhere('discount', 'LIKE', '%' . $request->name . '%')
                ->orwhere('total_amount', 'LIKE', '%' . $request->name . '%')
                ->orwhere('total_amount_usd', 'LIKE', '%' . $request->name . '%')
                ->orwhere('amoun', 'LIKE', '%' . $request->name . '%')
                ->get())
            ->get();
            foreach ($print as $el) {
                $data = Saleline::where('document_no', '=', $el->document_no)->get();
                array_push($allData, array("element" => $el, "items" => $data));
            }
            return ['data' => $result, 'print' => $allData];
        }
        else if ($request->start != '' && $request->end != '') {
            $result = saleuser::orderBy('created_at','desc')->where('created_by', '=', $id)
                ->whereIn('document_no', saleuser::select('document_no')
                    ->whereDate('created_at','>=',$request->start)
                    ->whereDate('created_at','<=',$request->end)
                    ->get())
                ->paginate(15);
            $print = saleuser::orderBy('created_at','desc')->where('created_by', '=', $id)
                            ->whereIn('document_no', saleuser::select('document_no')
                                ->whereDate('created_at','>=',$request->start)
                                ->whereDate('created_at','<=',$request->end)
                                ->get())
                            ->get();
            foreach ($print as $el) {
                $data = Saleline::where('document_no', '=', $el->document_no)->get();
                array_push($allData, array("element" => $el, "items" => $data));
            }
            return ['data' => $result, 'print' => $allData];
        }

    }
    public function searchsale(Request $request, $id)
    {
        if ($request->name != '') {
            $result = saleuser::where('created_by', '=', $id)
                ->whereIn('document_no', saleuser::select('document_no')
                    ->where('document_no', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('document_type', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('user_name', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('paymeny_method', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('exchane_rate', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('discount', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('total_amount', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('total_amount_usd', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('amoun', 'LIKE', '%' . $request->name . '%')
                    ->get())
                ->paginate(12);
            return $result;
        }
        if ($request->start != '' && $request->end != '') {
            $result = saleuser::where('created_by', '=', $id)
                ->whereIn('document_no', saleuser::select('document_no')
                    ->whereBetween('created_at', [$request->start, $request->end])
                    ->get())
                ->paginate(12);
            return $result;
        }
    }
    public function printsale(Request $request, $id)
    {
        $allData = array();
        if ($request->name == '' && $request->start == '' & $request->end == '') {
            $result = saleuser::where('created_by', '=', $id)->get();
        } else if ($request->name != '') {
            $result = saleuser::where('created_by', '=', $id)
                ->whereIn('document_no', saleuser::select('document_no')
                    ->where('document_no', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('document_type', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('id', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('user_name', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('paymeny_method', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('exchane_rate', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('discount', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('total_amount', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('total_amount_usd', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('amoun', 'LIKE', '%' . $request->name . '%')
                    ->get())
                ->get();
        } else if ($request->start != '' && $request->end != '') {
            $result = saleuser::where('created_by', '=', $id)
                ->whereIn('document_no', saleuser::select('document_no')
                    ->whereBetween('created_at', [$request->start, $request->end])
                    ->get())
                ->get();
        }
        foreach ($result as $el) {
            $data = Saleline::where('document_no', '=', $el->document_no)->get();
            array_push($allData, array("element" => $el, "items" => $data));
        }
        return $allData;
    }

    public function createexpen(Request $request)
    {
        $setup = setup::first();
        $serail_no = Serail::where('id', '=', 'Expenses')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int) $Sart_at + (int) $End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int) $Code_qty - strlen($newCode)); $i++) {
            $serail_no = $serail_no . "0";
        }
        $serail_no = $serail_no . $newCode;
        $result = expenses::create([
            'document_no' => $serail_no,
            'document' => $request['document'],
            'description' => $request['description'],
            'Payment_method' => $request['Payment_method'],
            'totat_exspan' => $request['totat_exspan'],
            'statue' => $request['statue'],
            'created_by' => $request->created_by,
            'updated_by' => $request->updated_by,
            'exspan_date' => $request['exspan_date'],

        ]);
        if ($result) {
            $serilano = Serail::where('id', '=', 'Expenses')->first();
            $serilano->end_code = $newCode;
            $serilano->save();
            $payment_amount = payment_amount::create([
                'document_no' => $serail_no,
                'document' => $request['document'],
                'document_type' => 'exspan',
                'decription' => 'exspan for ' . $request['description'],
                'totale_balanec' => $request['totat_exspan'],
                'currency_code' => $setup->main_Currency,
                'paymant_amount' => $request['totat_exspan'],
                'statue' => 'open',
                'created_by' => $request['created_by'],
            ]);
        }
        if ($payment_amount) {
            $result = expenses::orderBy('id', 'DESC')
                ->paginate(15);
            return ['statue' => true, 'data' => $result];
        } else {
            return ['statue' => false];
        }
    }
    public function getexpen()
    {
        $result = expenses::orderBy('id', 'DESC')
            ->paginate(15);
        return $result;
    }
    public function updateexpen(Request $request, $id)
    {
        $result = expenses::find($id);
        $result->update($request->all());
        $docno = $result->document_no;
        $payment_amount = payment_amount::where('document_no', '=', $docno)->first();
        $payment_amount->document = $request->document;
        $payment_amount->document_type = $request->description;
        $payment_amount->decription = 'exspan for ' . $request['description'];
        $payment_amount->totale_balanec = $request->totat_exspan;
        $payment_amount->paymant_amount = $request->totat_exspan;
        $payment_amount->save();
        return $result;
    }
    public function deleteexpen($id)
    {
        $result = expenses::find($id);
        $docno = $result->document_no;
        $payment_amount = payment_amount::where('document_no', '=', $docno)->first();
        $payment_amount->delete();
        $result->delete();
        if ($result) {
            return ['result' => 'Record has been deleted'];
        }
    }
    public function searchex(Request $request)
    {
        if ($request->document_no != null) {
            $result = expenses::where('document_no', 'LIKE', '%' . $request->document_no . '%')
                ->paginate(15);
            return $result;
        }
        if ($request->document != null) {
            $result = expenses::where('document', 'LIKE', '%' . $request->document . '%')
                ->paginate(15);
            return $result;
        }
        if ($request->description != null) {
            $result = expenses::where('description', 'LIKE', '%' . $request->description . '%')
                ->paginate(15);
            return $result;
        }
        if ($request->Payment_method != null) {
            $result = expenses::where('Payment_method', 'LIKE', '%' . $request->Payment_method . '%')
                ->paginate(15);
            return $result;
        }
        if ($request->totat_exspan != null) {
            $result = expenses::where('totat_exspan', 'LIKE', '%' . $request->totat_exspan . '%')
                ->paginate(15);
            return $result;
        }
        if ($request->exspan_date != null) {
            $result = expenses::where('exspan_date', 'LIKE', '%' . $request->exspan_date . '%')
                ->paginate(15);
            return $result;
        }
    }
    // expen report
    public function viewex()
    {
        $result = expenreport::orderBy('id', 'DESC')
            ->paginate(15);
        return $result;
    }
    public function shex(Request $request)
    {
        if ($request->Pay == null || $request->Pay == 'ALL') {
            $result = expenreport::whereBetween('exspan_date', [$request->start, $request->end])
                ->paginate(15);
            return $result;
        } else {
            $result = expenreport::whereBetween('exspan_date', [$request->start, $request->end])
                ->Where('Payment_method', 'LIKE', '%' . $request->Pay . '%')
                ->paginate(15);
            return $result;
        }
    }
    // search  sale report
    public function salsh(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->role == 'Admin' || $user->role == 'System') {
            if ($request->name != null && $request->Pay != null) {
                $result = saleuser::orderBy('id','DESC')
                    ->whereDate('created_at','>=',$request->start)
                    ->whereDate('created_at','<=',$request->end)
                    ->where('user_name', 'LIKE', '%' . $request->name . '%')
                    ->where('paymeny_method', 'LIKE', '%' . $request->Pay . '%')
                    ->paginate(15);
            } else if ($request->name != null && $request->Pay == null) {
                $result = saleuser::orderBy('id','DESC')
                    ->whereDate('created_at','>=',$request->start)
                    ->whereDate('created_at','<=',$request->end)
                    ->where('user_name', 'LIKE', '%' . $request->name . '%')
                    ->paginate(15);
            } else if ($request->name == null && $request->Pay != null) {
                $result = saleuser::orderBy('id','DESC')
                    ->whereDate('created_at','>=',$request->start)
                    ->whereDate('created_at','<=',$request->end)
                    ->where('paymeny_method', 'LIKE', '%' . $request->Pay . '%')
                    ->paginate(15);
            } else {
                $result = saleuser::orderBy('id','DESC')
                    ->whereDate('created_at','>=',$request->start)
                    ->whereDate('created_at','<=',$request->end)
                    ->paginate(15);
            }
            return ['status' => true, 'data' => $result];
        } else {
            return ['status' => false, 'message' => "You have not permission!"];
        }
    }
    // print sale report 
    public function slprint(Request $request, $id)
    {
        $user = User::find($id);
        $allData = array();
        $sumAmount = array();
        if ($user->role == 'Admin' || $user->role == 'System') {
            if ($request->name == '' && $request->start == '' & $request->end == '') {
                $result = saleuser::get();
            } else if ($request->name != '') {
                $result = saleuser::where('user_name', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('document_no', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('document_type', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('description', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('discount', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('paymeny_method', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('total_amount', 'LIKE', '%' . $request->name . '%')
                    ->orwhere('total_amount_usd', 'LIKE', '%' . $request->name . '%')
                    ->get();
            } else if ($request->start != '' && $request->end != '') {
                $result = saleuser::whereDate('created_at','>=',$request->start)
                                ->whereDate('created_at','<=',$request->end)
               // whereBetween('created_at', [$request->start, $request->end])
                    ->get();
            }
        }
        $total_amount = 0;
        $total = 0;

        foreach ($result as $el) {
            $total_amount += doubleval($el->total_amount);
            $total += doubleval($el->amoun);
            $data = Saleline::where('document_no', '=', $el->document_no)->get();
            array_push($allData, array("element" => $el, "items" => $data));
        }
        array_push($sumAmount, array("total_amount" => $total_amount, "total" => $total));
        return ["data" => $allData, "amount" => $sumAmount];
    }
    // end print sale report 
    // sale line view 
    public function getsale_line($id)
    {
        $user = User::find($id);
        if ($user->role == 'Admin' || $user->role == 'System') {
            $result = sale_line_view::orderBy('id', 'DESC')
                ->paginate(15);
            $result1 = sale_line_view::orderBy('id', 'DESC')
                ->get();
            return ['status' => true, 'data' => $result, 'print' => $result1];
        } else {
            return ['status' => false, 'message' => 'you have no permission!'];
        }
    }
    public function searchitem(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->role == 'Admin' || $user->role == 'System') {
            if ($request->name != null && $request->no != null && $request->des != null && $request->type != null && $request->pro != null) { // true for 4
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                       // ->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            } 
            else if ($request->name == null && $request->no != null && $request->des != null && $request->type != null && $request->pro != null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                       // ->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            } 
            else if ($request->name != null && $request->no == null && $request->des != null && $request->type != null && $request->pro != null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            } 
            else if ($request->name != null && $request->no != null && $request->des == null && $request->type != null && $request->pro != null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            } 
            else if ($request->name != null && $request->no != null && $request->des != null && $request->type == null && $request->pro != null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            } 
            else if ($request->name != null && $request->no != null && $request->des != null && $request->type != null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                       // ->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->get();
            } 

            else if ($request->name != null && $request->no == null && $request->des == null && $request->type == null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                       // ->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->get();
            }
            else if ($request->name == null && $request->no != null && $request->des == null && $request->type == null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->get();
            }
            else if ($request->name == null && $request->no == null && $request->des != null && $request->type == null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                       ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                       ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->get();
            }
            else if ($request->name == null && $request->no == null && $request->des == null && $request->type != null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                                        ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->get();
            }
            else if ($request->name == null && $request->no == null && $request->des == null && $request->type == null && $request->pro != null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                       ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                       ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            }
            else if ($request->name != null && $request->no != null && $request->des == null && $request->type == null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->get();
            }
            else if ($request->name != null && $request->no == null && $request->des != null && $request->type == null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                       // ->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->get();
            }
            else if ($request->name != null && $request->no == null && $request->des == null && $request->type != null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->get();
            }
            else if ($request->name != null && $request->no == null && $request->des == null && $request->type == null && $request->pro != null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        //->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('user_name', 'LIKE', '%' . $request->name . '%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            }
            else if ($request->name == null && $request->no != null && $request->des != null && $request->type == null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                       // ->whereBetween('created_at', [$request->startDate, $request->endDate])
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->get();
            }
            else if ($request->name == null && $request->no != null && $request->des == null && $request->type != null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                         ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                         ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->get();
            }
            else if ($request->name == null && $request->no != null && $request->des == null && $request->type == null && $request->pro != null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_no', 'LIKE', '%' . $request->no . '%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            }
            else if ($request->name == null && $request->no == null && $request->des != null && $request->type != null && $request->pro == null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('document_type','LIKE','%'.$request->type.'%')
                                        ->get();
            }
            else if ($request->name == null && $request->no == null && $request->des != null && $request->type == null && $request->pro != null) {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('description', 'LIKE', '%' . $request->des . '%')
                                        ->where('product_no','LIKE','%'.$request->pro.'%')
                                        ->get();
            }
            else {
                $result = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                    ->paginate(15);
                $result1 = sale_line_view::orderBy('id', 'DESC')
                ->whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                    ->get();
            }
            return ['status' => true, 'data' => $result, 'print' => $result1];
        } else {
            return ['status' => false, 'message' => 'data not found!'];
        }
    }
    // end sale line view 
    // stock inventory
    public function getinven($id)
    {
        $user = User::find($id);
        if ($user->role == 'Admin' || $user->role == 'System') {
            $result = stockkeeping::orderBy('id', 'DESC')
                ->paginate(15);
            $result1 = stockkeeping::orderBy('id', 'DESC')
                ->get();
            return ['status' => true, 'data' => $result, 'print' => $result1];
        } else {
            return ['status' => false, 'message' => 'you have no permission!'];
        }
    }
    public function shinven(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->role == 'Admin' || $user->role == 'System') {
            if ($request->doc != null && $request->no != null && $request->des != null) {
                $result = stockkeeping::whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
              //  whereBetween('exprit_date', [$request->startDate, $request->endDate])
                                        ->where('document_type','LIKE','%'.$request->doc.'%')
                                        ->where('product_no','LIKE','%'.$request->no.'%')
                                        ->where('description','LIKE','%'.$request->des.'%')
                                        ->paginate(15);
                $result1 = stockkeeping::whereDate('created_at','>=',$request->startDate)
                                        ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_type','LIKE','%'.$request->doc.'%')
                                        ->where('product_no','LIKE','%'.$request->no.'%')
                                        ->where('description','LIKE','%'.$request->des.'%')
                                        ->get();
            } 
            else if($request->doc == null && $request->no != null && $request->des != null){
                $result = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('product_no','LIKE','%'.$request->no.'%')
                                        ->where('description','LIKE','%'.$request->des.'%')
                                        ->paginate(15);
                $result1 = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('product_no','LIKE','%'.$request->no.'%')
                                        ->where('description','LIKE','%'.$request->des.'%')
                                        ->get();
            }
            else if($request->doc != null && $request->no == null && $request->des != null){
                $result = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_type','LIKE','%'.$request->doc.'%')
                                        ->where('description','LIKE','%'.$request->des.'%')
                                        ->paginate(15);
                $result1 = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_type','LIKE','%'.$request->doc.'%')
                                        ->where('description','LIKE','%'.$request->des.'%')
                                        ->get();
            }
            else if($request->doc != null && $request->no != null && $request->des == null){
                $result = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_type','LIKE','%'.$request->doc.'%')
                                        ->where('product_no','LIKE','%'.$request->no.'%')
                                        ->paginate(15);
                $result1 = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_type','LIKE','%'.$request->doc.'%')
                                        ->where('product_no','LIKE','%'.$request->no.'%')
                                        ->get();
            }
            else if($request->doc != null && $request->no == null && $request->des == null){
                $result = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_type','LIKE','%'.$request->doc.'%')
                                        ->paginate(15);
                $result1 = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('document_type','LIKE','%'.$request->doc.'%')
                                        ->get();
            }
            else if($request->doc == null && $request->no != null && $request->des == null){
                $result = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                       ->where('product_no','LIKE','%'.$request->no.'%')
                                        ->paginate(15);
                $result1 = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                       ->where('product_no','LIKE','%'.$request->no.'%')
                                        ->get();
            }
            else if($request->doc == null && $request->no == null && $request->des != null){
                $result = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('description','LIKE','%'.$request->des.'%')
                                        ->paginate(15);
                $result1 = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->where('description','LIKE','%'.$request->des.'%')
                                        ->get();
            }
            else {
                $result = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->paginate(15);
                $result1 = stockkeeping::whereDate('created_at','>=',$request->startDate)
                ->whereDate('created_at','<=',$request->endDate)
                                        ->get();
            }
            return ['status' => true, 'data' => $result, 'print' => $result1];
        } else {
            return ['status' => false, 'message' => 'data not found!'];
        }
    }
    public function expensDetial(Request $request,$id){
        $party = array();

        $user = User::find($id);
        if($user->role == 'Admin' || $user->role  == 'System'){
          
            $expendtype = expenses_type::get();
            // $count ='';
            if(count($expendtype) >= 0){
                foreach ($expendtype as $el) {
                    $total = 0;
                   $sub_expendtype = expenses::orderBy('id','DESC')
                                            ->where('document','=',$el->expenses_name)
                                           // ->whereBetween('exspan_date',[$request->start,$request->end])
                                            ->whereDate('exspan_date','>=',$request->start)
                                            ->whereDate('exspan_date','<=',$request->end)    
                                            ->get();
                    if(count($sub_expendtype) >= 0){
                        foreach ($sub_expendtype as $subEl) {
                            $total += floatval($subEl->totat_exspan);
                        }
                    }else{
                        $total = 0;
                    }
                    array_push($party, array('el'=>$el, 'detail'=>$sub_expendtype,'total'=>$total));
                }
            }
            $exchange = exchangerate::first();
            $rate = $exchange->exchange_rate;
            return ['data'=>$party,'rate'=>$rate];
        }else{
            return "No rights to view!";
        }

    }
}
