<?php

namespace App\Http\Controllers;

use App\Models\exchangerate;
use App\Models\payment_amount;
use App\Models\productvariantcode;
use App\Models\Serail;
use App\Models\setup;
use App\Models\startstock;
use App\Models\startstock_list;
use App\Models\stockkeeping;
use App\Models\stocktransaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\product_vavaincode_view;

class BeginingStockController extends Controller
{
    public function index(Request $request)
    {
        if ($request['created_by'] != '') {
            if ($request->name == '') {
                $beginingstock = startstock::orderBy('id', 'desc')->paginate(15);
                $print = startstock::orderBy('id', 'desc')->get();

                return ['beg' => $beginingstock, 'print' => $print];
            } elseif ($request->name != '') {
                $data = startstock::orderBy('id', 'DESC')
                ->where('document_no', 'LIKE', $request->name.'%')
                ->orwhere('document_type', 'LIKE', $request->name.'%')
                ->orwhere('description', 'LIKE', $request->name.'%')
                ->orwhere('paymentmethod_code', 'LIKE', $request->name.'%')
                ->paginate(15);

                $print = startstock::orderBy('id', 'DESC')
                  ->where('document_no', 'LIKE', $request->name.'%')
                  ->orwhere('document_type', 'LIKE', $request->name.'%')
                  ->orwhere('description', 'LIKE', $request->name.'%')
                  ->orwhere('paymentmethod_code', 'LIKE', $request->name.'%')
                  ->get();

                return ['beg' => $data, 'print' => $print];
            }
        } else {
            return ['statue' => 'Reqest date fail.'];
        }
    }

    public function getbyId(Request $request)
    {
        if ($request['created_by'] != '' && $request['document_no'] != '') {
            $beginingstock = startstock::where('document_no', '=', $request->document_no)->orderBy('id', 'desc')->first();

            return $beginingstock;
        } else {
            return ['statue' => 'Reqest date fail.'];
        }
    }

    public function createtb(Request $request)
    {
        // $photo = $this->uploadPhoto($request);
        $setup = setup::first();

        if ($request['created_by'] != '') {
            $serail_no = Serail::where('id', '=', 'BegStoct')->first();
            $Prifixcode = $serail_no->prefix_code;
            $Code​​​_qty = $serail_no->qty_code;
            $Sart_at = $serail_no->start_code;
            $End_code = $serail_no->end_code;
            $newCode = (int) $Sart_at + (int) $End_code;
            $serail_no = $Prifixcode;
            for ($i = 0; $i < ((int) $Code​​​_qty - strlen($newCode)); ++$i) {
                $serail_no .= '0';
            }
            $serail_no .= $newCode;
            $product = startstock::create([
                'document_no' => $serail_no,
                'paymentmethod_code' => $request->paymentmethod_code,
                'created_by' => $request['created_by'],
                'statue' => 'open',
                'total_amount' => 0,
                'discount' => 0,
                'curency_code' => $setup->main_Currency,
            ]);
            if ($product) {
                $serilano = Serail::where('id', '=', 'BegStoct')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
                if ($serilano) {
                    $product = startstock::where('document_no', '=', $serail_no)->get();

                    return $product;
                } else {
                    return ['statue :' => 'faile Create'];
                }
            } else {
                return ['statue :' => 'faile Create'];
            }
        } else {
            return ['statue :' => 'faile Create'];
        }
    }

    public function create_list(Request $request)
    {
        $date = Carbon::tomorrow();
        $date_format = date_format($date, 'Y-m-d');

        $setup = setup::first();
        if ($request['created_by'] != '' && $request['document_no'] != '') {
            $product = startstock_list::create([
                'document_no' => $request['document_no'],
                'created_by' => $request['created_by'],
                'curency_code' => $setup->main_Currency,
                'exprit_date' => $date_format,
            ]);
            if ($product) {
                return startstock_list::where('document_no', '=', $request->document_no)->orderBy('id', 'desc')->get();
            } else {
                return ['statue' => 'Reqest  update date fail.'];
            }
        } else {
            return ['statue' => 'Reqest  update date fail.'];
        }
    }

    public function get_list(Request $request)
    {
        if ($request->created_by != '' && $request->document_no != '') {
            $list = startstock_list::where('document_no', '=', $request->document_no)
                                    ->orderBy('id', 'desc')->get();

            return $list;
        } else {
            return ['message' => 'This api need parameter(document_no,created_by)'];
        }
    }

    public function update(Request $request)
    {
        if ($request['updated_by'] != '' && $request['document_no'] != '') {
            $beginingstock = startstock::where('document_no', '=', $request->document_no)->first();
            if($request->discount == '' && $request->discount == null) 
            {
                $request->discount = 0;
            }
            $price = floatval($request->total_amount) -  floatval($request->total_amount)*(intval($request->discount)/100);
            $beginingstock->update([
             'document_type' => $request->document_type,
             'description' => $request->description,
             'total_amount' => $price,
             'discount' => $request->discount,
             'paymentmethod_code' => $request->paymentmethod_code,
            //  'curency_code' => $request->curency_code,
             'inactived' => $request->inactived,
             'statue' => $request->statue,
             'updated_by' => $request->updated_by,
        ]);
            if ($beginingstock) {
                return $beginingstock;
            } else {
                return ['statue' => 'Reqest  update date fail.'];
            }
        } else {
            return ['statue' => 'Reqest update date fail.'];
        }
    }

    public function update_list(Request $request)
    {
        if ($request['updated_by'] != '' && $request['document_no'] != '') {
            $beginingstock = startstock_list::find($request->id);
            $beginingstock->update([
            'document_type' => $request->document_type,
            'description' => $request->description,
            'product_no' => $request->product_no,
            'issu_date' => $request->issu_date,
            'exprit_date' => $request->exprit_date,
            'unit_of_measure_code' => $request->unit_of_measure_code,
            'unit_price' => $request->unit_price,
            'inventory' => $request->inventory,
            'total_amount' => $request->total_amount,
            'remark' => $request->remark,
            'updated_by' => $request->updated_by,
        ]);

        $stockKeeping = stockkeeping::where('document_no','=',$beginingstock->document_no)
                                    ->where('product_no','=',$beginingstock->product_no)->first();
        if($stockKeeping){
            $variantcode = product_vavaincode_view::addSelect('product_no', 'stock_unit_of_measure_code','variant_unit_of_measure_code', 'quantity_per_unit')
                            ->where('product_no', '=', $stockKeeping->product_no)
                            ->where('variant_unit_of_measure_code', '=', $request->unit_of_measure_code)->first();
            $inventory = floatval($request->inventory) * floatval($variantcode->quantity_per_unit);

            $stockKeeping->exprit_date = $request->exprit_date;
            // $stockKeeping->unit_of_measure_code = $request->unit_of_measure_code;
            $stockKeeping->unit_price =$request->unit_price;
            $stockKeeping->inventory = $inventory;
            $stockKeeping->inventory_new = $inventory;
            $stockKeeping->save();
        }

            $stock = startstock::where('document_no', '=', $request->document_no)->first();
            $items = startstock_list::where('document_no', '=', $stock->document_no)->get();

            $price = 0;
            if (count($items) > 0) {
                foreach ($items as $el) {
                    if ($el->total_amount == null) {
                        $price += 0;
                    } else {
                        $price += floatval($el->total_amount);
                    }
                }
                $after_discount = $price - ((intval($stock->discount)/100) * $price) ;
                $stock->total_amount = $after_discount;
                $stock->save();
            }


            if ($beginingstock) {
                return $beginingstock;
            } else {
                return ['statue' => 'Reqest update date fail.'];
            }
        } else {
            return ['statue' => 'Reqest update date fail.'];
        }
    }

    public function delete(Request $request)
    {
        if ($request['created_by'] != '' && $request['document_no'] != '') {
            $beginingstock = startstock::where('document_no', '=', $request->document_no)->where('statue', '!=', 'close')->first();
            $beginingstock->delete();
            if ($beginingstock) {
                $beginingstock = startstock_list::where('document_no', '=', $request->document_no);
                $beginingstock->delete();
                if ($beginingstock) {
                    return startstock::orderBy('id', 'desc')->paginate(15);
                }
            } else {
                return ['statue' => 'Reqest delete date fail.'];
            }
        }
    }

    public function delete_list(Request $request)
    {
        if ($request['created_by'] != '' && $request['document_no'] != '') {
            $beginingstock = startstock_list::find($request->id);
            $beginingstock->delete();
            $stock = startstock::where('document_no', '=', $request->document_no)->first();
            $items = startstock_list::where('document_no', '=', $request->document_no)->get();
            $price = 0;

            if (count($items) > 0) {
                foreach ($items as $el) {
                    if ($el->total_amount == null) {
                        $price += 0;
                    } else {
                        $price += floatval($el->total_amount);
                    }
                }
                $after_discount = $price - (( intval($stock->discount) / 100) * $price);
                $stock->total_amount = $after_discount;
                $stock->save();
            }

            if ($beginingstock) {
                return startstock_list::where('document_no', '=', $request->document_no)->get();
            } else {
                return ['statue' => 'Reqest delete date fail.'];
            }
        } else {
            return ['statue' => 'Reqest delete date fail.'];
        }
    }

    public function submitbegining(Request $request)
    {
        if ($request['created_by'] != '' && $request['document_no'] != '') {
            $startstocklist = startstock_list::where('document_no', '=', $request->document_no)->get();
            $startstock = startstock::where('document_no', '=', $request->document_no)->first();

            foreach ($startstocklist as $st) {
                $prid = $st->product_no;
                $vunit = $st->unit_of_measure_code;
                $vavaincode = productvariantcode::where('product_no', '=', $prid)->where('variant_unit_of_measure_code', '=', $vunit)->first();
                $stoctunit = $vavaincode->stock_unit_of_measure_code;
                $unit_price = 0;
                $unit_price = doubleval($st->unit_price) / doubleval($vavaincode->quantity_per_unit);
                $unit = $st->inventory;
                $inventory = doubleval($unit) * doubleval($vavaincode->quantity_per_unit);
                $receiveorder = stockkeeping::create([
                           'document_no' => $st['document_no'],
                           'document_type' => 'Begining Stock',
                           'product_no' => $st['product_no'],
                           'description' => $st['description'],
                           'issu_date' => $st['issu_date'],
                           'exprit_date' => $st['exprit_date'],
                           'line_no' => $st['line_no'],
                           'unit_of_measure_code' => $stoctunit,
                           'unit_price' => $unit_price,
                           'inventory' => $inventory,
                           'inventory_order' => $inventory,
                           'inventory_new' => $inventory,
                           'total_amount' => $st['total_amount'],
                           'curency_code' => $st['curency_code'],
                           'remark' => $st['remark'],
                           'statuse' => 'open',
                           'created_by' => $st['created_by'],
                           ]);
                if ($receiveorder) {
                    $stocktransaction = stocktransaction::create([
                      'document_no' => $receiveorder['document_no'],
                      'document_type' => 'Begining Stock',
                      'product_no' => $receiveorder['product_no'],
                      'description' => $receiveorder['description'],
                      'unit_of_measure_code' => $stoctunit,
                      'unit_price' => $receiveorder['unit_price'],
                      'inventory' => $receiveorder['inventory'],
                      'total_amount' => $st['total_amount'],
                      'curency_code' => $receiveorder['curency_code'],
                      'remark' => $receiveorder['remark'],
                      'created_by' => $receiveorder['created_by'],
                          ]);
                }
                
            }
            if ($startstock) {
                $payment_amount = payment_amount::create([
                       'document_no' => $startstock['document_no'],
                       'document' => 'Begining Stock',
                       'document_type' => 'expense',
                       'decription' => 'Begining on Invoince'.$startstock['document_no'],
                       'totale_balanec' => $startstock['total_amount'],
                       'exchane_rate' => $startstock['exchane_rate'],
                       'currency_code' => $startstock['curency_code'],
                       'paymant_amount' => $startstock['total_amount'],
                       'statue' => 'open',
                       'created_by' => $request['created_by'],
            ]);
            }
            if ($payment_amount) {
                $startstock = startstock::where('document_no', '=', $request->document_no)->first();
                $startstock->update(['statue' => 'close']);
                if ($startstock) {
                    return ['statue' => 'Reqest date succesful.'];
                } else {
                    return ['statue' => 'Reqest date fail.'];
                }
            }
        } else {
            return ['statue' => 'Reqest date fail.'];
        }
    }

    public function searchStockBeginning($search, Request $request)
    {
        $data = startstock::orderBy('id', 'DESC')
                            ->where('document_no', 'LIKE', $search.'%')
                            ->orwhere('document_type', 'LIKE', $search.'%')
                            ->orwhere('description', 'LIKE', $search.'%')
                            ->orwhere('paymentmethod_code', 'LIKE', $search.'%')
                            ->paginate(15);

        return $data;
    }

    public function stockStart($id, Request $request)
    {
        $array = [];
        $user = User::find($id);
        if ($user->role == 'Admin' || $user->role == 'System') {
            $result = startstock::orderBy('id', 'DESC')
                                ->whereDate('created_at', '>=', $request->start)
                                ->whereDate('created_at', '<=', $request->end)
                                ->where('statue', '=', 'close')
                                ->get()
                                ->groupBy('paymentmethod_code');
            $total = 0;
            if (count($result) > 0) {
                foreach ($result as $el) {
                    $sum = 0;
                    if (count($el) > 0) {
                        foreach ($el as $key) {
                            $sum += floatval($key->total_amount);
                        }
                    }
                    $total += floatval($sum);
                    array_push($array, ['paymentmethod_code' => $el[0]->paymentmethod_code, 'items' => $el, 'sum' => $sum]);
                }
            }
            $exchange = exchangerate::first();
            $rate = $exchange->exchange_rate;

            return ['data' => $array, 'total' => $total, 'rate' => $rate];
        } else {
            return ['status' => false, 'message' => 'you have not permission!'];
        }
    }

    public function reverse($id)
    {
        $begin = startstock::where('document_no', '=', $id)->first();
        if ($begin->statue == 'close') {
            $begin->statue = 'open';
            $begin->save();

            return ['status' => true];
        } else {
            return ['status' => false];
        }
    }

    public function updateReverse(Request $request)
    {
        $startstock = startstock::where('document_no', '=', $request->document_no)->first();
        $startstocklist = startstock_list::where('document_no', '=', $request->document_no)->get();

        foreach ($startstocklist as $st) {
            $prid = $st->product_no;
            $vunit = $st->unit_of_measure_code;
            $vavaincode = productvariantcode::where('product_no', '=', $prid)->where('variant_unit_of_measure_code', '=', $vunit)->first();
            $stoctunit = $vavaincode->stock_unit_of_measure_code;
            $unit_price = 0;
            $unit_price = doubleval($st->unit_price) / doubleval($vavaincode->quantity_per_unit);
            $unit = $st->inventory;
            $inventory = doubleval($unit) * doubleval($vavaincode->quantity_per_unit);

            $receiveorder = stockkeeping::orderBy('id', 'ASC')->where('document_no', '=', $st['document_no'])->where('product_no', '=', $st['product_no'])->first();
            $receiveorder->update([
                      'document_type' => 'Begining Stock',
                      'product_no' => $st['product_no'],
                      'description' => $st['description'],
                      'issu_date' => $st['issu_date'],
                      'exprit_date' => $st['exprit_date'],
                      'line_no' => $st['line_no'],
                      'unit_of_measure_code' => $stoctunit,
                      'unit_price' => $unit_price,
                      'inventory' => $inventory,
                      'inventory_order' => $inventory,
                      'inventory_new' => $inventory,
                      'total_amount' => $st['total_amount'],
                      'curency_code' => $st['curency_code'],
                      'remark' => $st['remark'],
                      'statuse' => 'open',
                      'created_by' => $st['created_by'],
            ]);
            if ($receiveorder) {
                // get stock transection
                $stocktransaction = stocktransaction::orderBy('id', 'ASC')->where('document_no', '=', $startstock->document_no)
                                                    ->where('product_no', '=', $st['product_no'])->first();

                $stocktransaction->update([
                  'document_type' => 'Begining Stock',
                  'product_no' => $receiveorder['product_no'],
                  'description' => $receiveorder['description'],
                  'unit_of_measure_code' => $vunit,
                  'unit_price' => $receiveorder['unit_price'],
                  'inventory' => $receiveorder['inventory'],
                  'total_amount' => $st['total_amount'],
                  'curency_code' => $receiveorder['curency_code'],
                  'remark' => $receiveorder['remark'],
                  'created_by' => $receiveorder['created_by'],
                   ]);
            }
            if ($stocktransaction) {
                $payment_amount = payment_amount::where('document_no', '=', $startstock->document_no)->get();
                foreach ($payment_amount as $el) {
                    $el->update([
                      'document' => 'Begining Stock',
                      'document_type' => 'expense',
                      'decription' => 'Begining on Invoince'.$startstock['document_no'],
                      'totale_balanec' => $startstock['total_amount'],
                      'exchane_rate' => $startstock['exchane_rate'],
                      'currency_code' => $startstock['curency_code'],
                      'paymant_amount' => $startstock['total_amount'],
                      'statue' => 'open',
                      'created_by' => $request['created_by'],
                    ]);
                }
            }
        }
    }

    public function btnReverse($id)
    {
        $x = stockkeeping::where('document_no', $id);
        $x->update([
                   'statuse' => 'reverse',
                   ]);

        if ($x) {
            $y = payment_amount::where('document_no', $id);
            $y->update([
                   'statue' => 'reverse',
            ]);
        }
        if ($y) {
            $startstock = startstock::where('document_no', $id)->first();
            $startstock->update(['statue' => 'reverse']);
            if ($startstock) {
                return $startstock;
            } else {
                return ['statue' => 'Request data fail.'];
            }
        }
    }

    public function btnCopy(Request $request)
    {
       $x = $this->createtb($request);

        $oldDocument = startstock::where('document_no','=',$request->document_no)->first();
        $z = [];
        foreach ($x as $y) {
            $y->document_type = $oldDocument->document_type;
            $y->description = $oldDocument->description;
            $y->total_amount = $oldDocument->total_amount;
            $y->paymentmethod_code = $oldDocument->paymentmethod_code;
            $y->save();

            $copy = startstock_list::where('document_no', '=', $request->document_no)->get();
            foreach ($copy as $copys) {
                $product = startstock_list::create([
                     'document_no' => $y['document_no'],
                     'document_type' => $y['document_type'],
                     'product_no' => $copys['product_no'],
                     'description' => $copys['description'],
                     'issu_date' => $copys['issu_date'],
                     'exprit_date' => $copys['exprit_date'],
                     'unit_of_measure_code' => $copys['unit_of_measure_code'],
                      'unit_price' => $copys['unit_price'],
                     'inventory' => $copys['inventory'],
                     'total_amount' => $copys['total_amount'],
                     'curency_code' => $copys['curency_code'],
                     'remark' => $copys['remark'],
                     'created_by' => $y['created_by'],
                ]);
                array_push($z, $product);
            }
        }

       return response()->json(['view' => $y, 'pro' => $copy, 'copy' => $z]);
    }

    public function getOpenBeginningstock(){
        $data = startstock::orderBy('created_at','desc')->where('statue','=','open')->get();
        return $data;
    }
    public function Addmorelist($document_no, Request $request){

       // return $request->has('product_no');
        $issue_date =  date_format(Carbon::now(), 'Y-m-d');

        $date = Carbon::tomorrow();
        $date_format = date_format($date, 'Y-m-d');
        $setup = setup::first();
        if ( $document_no != '') {
            $list = startstock_list::where('document_no','=',$document_no)->where('product_no','=',$request->product_no)->get();
            if(count($list) == 0 && $request->has('product_no')){
                $product = startstock_list::create([
                    'document_no' => $document_no,
                    'product_no' => $request->product_no,
                    'description' => $request->description,
                    'created_by' => $request['created_by'],
                    'curency_code' => $setup->main_Currency,
                    'issu_date' => $issue_date,
                    'created_by' => $request->created_by,
                    'exprit_date' => $date_format,
                ]);
                return ['statue' => 'created successfully'];
            }
            else{
                return ['statue' => 'have created already'];
            }
          
        } else {
            return ['statue' => 'Reqest  update date fail.'];
        }

    }
    public function searchList(Request $request){
        $data = startstock_list::where('document_no','=',$request->document_no)
                                ->whereIn('product_no', startstock_list::select('product_no')
                                ->where('product_no','LIKE',$request->name.'%')
                                ->orwhere('description','LIKE',$request->name.'%')
                                ->get()
                            )
                            ->get();
        return $data;
                            
    }

}
