<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\rooms;
use App\Models\setup;
use App\Models\Serail;
use App\Models\roomstype;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class Roomcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
         $room = rooms::get();
         return $room;
     }


     public function createID(Request $request)
     {
          $Currency = setup::first();
          $idp = rooms::create([
              'room_no' => $request['room_no'],
              'currency' => $Currency->main_Currency,
              'description' => $request['description'],
              'status' => 'Open',
          ]);
          return response()->json([$idp]);

        }
    public function findID($id){
        return rooms::where('room_no','=', $id)->first();
    }

    public function createroom(Request $request)
    {
        // $photo = $this->uploadPhoto($request);
        $serail_no = Serail::where('id', '=', 'Room')->first();
        $Prifixcode = $serail_no->prefix_code;
        $Code_qty = $serail_no->qty_code;
        $Sart_at = $serail_no->start_code;
        $End_code = $serail_no->end_code;
        $newCode = (int) $Sart_at + (int) $End_code;
        $serail_no = $Prifixcode;
        for ($i = 0; $i < ((int) $Code_qty - strlen($newCode)); ++$i) {
            $serail_no = $serail_no.'0';
        }
        $serail_no = $serail_no.$newCode;

        $Currency = setup::first();

        $create_rooms = rooms::create([
            'room_no' => $serail_no,
            'room_number' => $request->room_number,
            'roomtype_no' => $request->roomtype_no,
            'image_url' => $request->image_url,
            'room_name' => $request->room_name,
            'currency' => $Currency->main_Currency,
            'description' => $request->description,
            'adults' => $request->adults,
            'room_price' => $request->room_price,
            'status' => 'Open',
        ]);
            if ($create_rooms) {
                $serilano = Serail::where('id', '=', 'Room')->first();
                $serilano->end_code = $newCode;
                $serilano->save();
                if ($serilano) {
                    $room = rooms::where('room_no', '=', $serail_no)->get();
                    return $room;
                } else {
                    return ['statue :' => 'faile Create'];
                }
            }
    }

     // how to create updateroom //
    public function updateroom($id, Request $request){
        $room = rooms::find($id);
        $Currency = setup::first();
        $dir = $this->directory();
        if ($request->image_url != $room['image_url']) {
            File::delete($dir.$room['image_url']);
            $photo = $this->uploadPhoto($request);
        } else {
            $photo = $request->image_url;
        }
        $room->update([
            $room->roomtype_no  = $request->roomtype_no,
            $room->room_number  = $request->room_number,
            $room->room_name    = $request->room_name,
            $room->description  = $request->description,
            $room->image_url    = $photo,
            $room->currency     = $Currency,
            $room->room_price   = $request->room_price,
            $room->updated_by   = $request->updated_by,
        ]);
        return $room;
    }


    public function directory()
     {
         return $dir = 'img'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR;
     }

     public function uploadPhoto(Request $request)
     {
         $dir = $this->directory();
         $file = $request->image_url;
         $this->makeDirectory($dir);
         $photo = time().'.'.explode(';', explode('/', $file)[1])[0];
         Image::make($file)->save(public_path($dir).$photo);

         return $photo;
     }


    // how to create deleteroom //
    public function deleteroom($id){
        $data = rooms::find($id);
        if($data != null){
            $data->delete();
            return "Deleted successfully";
        }else {
            return "No data to delete";
        }
    }


    // how to create searchroom //
    public function searchroom($id)
    {
        $room = rooms::where("roomtype_no", 'LIKE', "%$id%")
                        ->orwhere("room_no", 'LIKE', "%$id%")
                        ->orwhere("room_name", 'LIKE', "%$id%")
                        ->orwhere("description", 'LIKE', "%$id%")
                        ->get();
        if ($room) {
            return $room;
        } else {
            return ['statue :' => "Note Date"];
        }
    }

    //  TBALE ROOM //


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showroomtype()
    {
          $roomstype = roomstype::paginate(15);
         return $roomstype;
    }


    //   TBALE ROOMTYPE //


    // how to create createroomtype //
    public function createroomtype(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData, [
            'roomtype'          => 'required',
            'description'       => 'required',
            'created_by'        => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $roomstype= roomstype::create([
            'roomtype'          => $request->roomtype,
            'description'       => $request->description,
            'created_by'        => $request->created_by,
        ]);
        if($roomstype){
             $roomstype = roomstype::get();
             return $roomstype;
        }
        else{
            return false;
        }
    }


     // how to create updateroomtype //
    public function updateroomtype($id, Request $request){
        $requestData = $request->all();
        $validator   = Validator::make($requestData, [
            'roomtype'           => 'required',
            'description'        => 'required',
            'created_by'         => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors'         => $validator->errors(),
            ], 422);
        }
        $data = roomstype::find($id);
        if($data != null){
            $data->roomtype         = $request->roomtype;
            $data->description      = $request->description;
            $data->created_by       = $request->created_by;
            $data->save();
            if($data){
                return "Update successfully";
            }
        }else {
            return "No data to update";
        }
    }

    // how to create deleteroomtype //
    public function deleteroomtype($id){
        $data = roomstype::find($id);
        if($data != null){
            $data->delete();
            return "Deleted successfully";
        }else {
            return "No data to delete";
        }
    }

    // how to create deleteroomtype //
    public function searchroomtype(Request $request)
    {
        $roomstype = roomstype::where("roomtype", 'LIKE', "%$request->roomtype%")
                        ->where("description", 'LIKE', "%$request->description%")
                        ->get();
        if ($roomstype) {
            return $roomstype;
        } else {
            return ['statue :' => "Note Date"];
        }
    }
}
