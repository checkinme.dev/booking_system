<?php

use App\Http\Controllers\BeginingStockController;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\CategorysController;
use App\Http\Controllers\CloseShiftController;
use App\Http\Controllers\CurencyController;
use App\Http\Controllers\employeecontroller;
use App\Http\Controllers\ExchangeRateController;
use App\Http\Controllers\jusinsController;
use App\Http\Controllers\LaboController;
use App\Http\Controllers\laboservicecontroller;
use App\Http\Controllers\pateintcontroller;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\paymentVoucherController;
use App\Http\Controllers\Poscontroller;
use App\Http\Controllers\PrescriptionsController;
use App\Http\Controllers\ProductModelController;
use App\Http\Controllers\ProductUnitTypeController;
use App\Http\Controllers\ProductBomContraller;
use App\Http\Controllers\ProductContraller;
use App\Http\Controllers\ProductgroupController;
use App\Http\Controllers\PurcheaOrderContraller;
use App\Http\Controllers\ReceiveOrderContraller;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SerailController;
use App\Http\Controllers\ServiceTestController;
use App\Http\Controllers\StockalertController;
use App\Http\Controllers\StockCountController;
use App\Http\Controllers\StockExpireController;
use App\Http\Controllers\StockOnHandController;
use App\Http\Controllers\StockTransactionContraller;
use App\Http\Controllers\SuppliyerController;
use App\Http\Controllers\testserviceController;
use App\Http\Controllers\UnitofMeasureController;
use App\Http\Controllers\UnitTypeMeasureController;
use App\Http\Controllers\User_viewController;
use App\Http\Controllers\Roomcontroller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\furnitureController;
use App\Http\Controllers\RoompriceController;

use App\Http\Controllers\MyRoomController;

/*php
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// test api
    Route::group(['prefix' => 'v1'], function () {

    // room price
    Route::get('/roomprice/index', [RoompriceController::class, 'showroomprice']);
    Route::post('/roomprice/createroomprice', [RoompriceController::class, 'createroomprice']);
    Route::post('/roomprice/updateroomprice/{id}', [RoompriceController::class, 'updateroomprice']);
    Route::get('/roomprice/deleteroomprice/{id}', [RoompriceController::class, 'deleteroomprice']);
    Route::post('/roomprice/searchroomprice/', [RoompriceController::class, 'searchroomprice']);


     //room
     Route::get('/room/index', [Roomcontroller::class, 'index']);
     Route::post('/room/createID', [Roomcontroller::class, 'createID']);
     Route::get('/room/findID/{id}', [Roomcontroller::class, 'findID']);
     Route::post('/room/create', [Roomcontroller::class, 'createroom']);
     Route::post('/room/updateroom/{id}', [Roomcontroller::class, 'updateroom']);
     Route::get('/room/delete/{id}', [Roomcontroller::class, 'deleteroom']);
     Route::get('/room/searchroom/{id}', [Roomcontroller::class, 'searchroom']);

         //vichhai
    Route::post('/room/generateid', [MyRoomController::class, 'generateId']);
    Route::post('/room/getroom', [MyRoomController::class, 'getRoom']);
    Route::get('/getroomtype', [MyRoomController::class, 'getRoomType']);
    Route::get('/getFurniture', [MyRoomController::class, 'getFurniture']);
    
    Route::post('/updateRoom', [MyRoomController::class, 'updateRoom']);
    Route::post('/updateRoomImage', [MyRoomController::class, 'updateRoomImage']);

   // createFurniture
    Route::post('/createFurniture/{id}', [MyRoomController::class, 'createFurniture']);
    Route::post('/updateFurniture', [MyRoomController::class, 'updateFurniture']);
    Route::post('/deleteFurniture', [MyRoomController::class, 'deleteFurniture']);
    //Room price
    Route::post('/updateRoomPrice', [MyRoomController::class, 'updateRoomPrice']);
    Route::post('/createRoomPrice/{id}', [MyRoomController::class, 'createRoomPrice']);
    Route::post('/deleteRoomPrice', [MyRoomController::class, 'deleteRoomPrice']);
    
    

    


    // roomtype
    Route::get('/roomtype/index', [Roomcontroller::class, 'showroomtype']);
    Route::post('/roomtype/createroomtype', [Roomcontroller::class, 'createroomtype']);
    Route::post('/roomtype/updateroomtype/{id}', [Roomcontroller::class, 'updateroomtype']);
    Route::get('/roomtype/deleteroomtype/{id}', [Roomcontroller::class, 'deleteroomtype']);
    Route::post('/roomtype/searchroomtype', [Roomcontroller::class, 'searchroomtype']);

    // furniture
    Route::get('getfurniture/index', [furnitureController::class, 'showfurniture']);
    Route::post('/furniture/createfurniture', [furnitureController::class, 'createfurniture']);
    Route::post('/furniture/update/{id}', [furnitureController::class, 'updatefurniture']);
    Route::get('/furniture/deletefurniture/{id}', [furnitureController::class, 'deletefurniture']);
    Route::post('/furniture/searchfurniture', [furnitureController::class, 'searchfurniture']);


    // stockexpire
    Route::post('/se/index', [StockExpireController::class, 'index']);

    Route::post('/services/search/{id}', [laboservicecontroller::class, 'SearchTestService']);
    // stcokonhand
    Route::post('/soh/index', [StockOnHandController::class, 'index']);
    Route::post('/soh/getIndex', [StockOnHandController::class, 'getIndex']);

    // expire Alert

    Route::get('/expireAlert', [StockalertController::class, 'getExpireAlert']);
    Route::post('/update/Expire/{id}', [StockalertController::class, 'updateExpire']);
    // brand api
    Route::post('login/', [UserController::class, 'login']);

    Route::post('brands/update/{brand}', [BrandsController::class, 'update']);
    Route::get('brands/', [BrandsController::class, 'index']);
    Route::get('brands/getdata/', [BrandsController::class, 'getdata']);
    Route::get('Activetread/get/', [BrandsController::class, 'getActivetread']);

    Route::get('brand/searchBrand/{search}', [BrandsController::class, 'searchBrand']);

    // category
    Route::post('categorys/create', [CategorysController::class, 'store']);
    Route::post('categorys/updete/{category}', [CategorysController::class, 'update']);
    Route::get('category/', [CategorysController::class, 'index']);
    Route::get('category/getcat', [CategorysController::class, 'getcat']);
    Route::post('category/excel', [CategorysController::class, 'storeExcel']);
    Route::get('category/searchCategory/{search}', [CategorysController::class, 'searchCategory']);

    // Group product
    Route::post('progroups/create', [ProductgroupController::class, 'create']);
    Route::post('progroups/updete/{progroups}', [ProductgroupController::class, 'update']);
    Route::get('progroups/', [ProductgroupController::class, 'index']);
    Route::get('progroups/getdatagroup', [ProductgroupController::class, 'getdatagroup']);
    Route::get('progroups/searchGroup', [ProductgroupController::class, 'searchGroup']);


    // group service
    Route::get('group/getgroupser/', [ProductgroupController::class, 'Groupservice']);
    Route::get('group/SelectGroupservice', [ProductgroupController::class, 'SelectGroupservice']);
    Route::post('group/createGroupservice', [ProductgroupController::class, 'createGroupservice']);
    // end group service

   // Model product
   Route::post('promodel/create', [ProductModelController::class, 'create']);
   Route::post('promodel/updete/{promood}', [ProductModelController::class, 'update']);
   Route::get('promodel/', [ProductModelController::class, 'index']);
   Route::get('promodel/getdatagroup', [ProductModelController::class, 'getdatagroup']);
   Route::get('promodel/searchGroup', [ProductModelController::class, 'searchGroup']);


// unit type product
   Route::post('prounittype/create', [ProductUnitTypeController::class, 'create']);
   Route::post('prounittype/updete/{prounittype}', [ProductUnitTypeController::class, 'update']);
   Route::get('prounittype/', [ProductUnitTypeController::class, 'index']);
   Route::get('prounittype/getdatagroup', [ProductUnitTypeController::class, 'getdatagroup']);
   Route::get('prounittype/searchGroup', [ProductUnitTypeController::class, 'searchGroup']);

    // group service
    Route::get('model/getgroupser/', [ProductModelController::class, 'Groupservice']);
    Route::get('model/SelectGroupservice', [ProductModelController::class, 'SelectGroupservice']);
    Route::post('model/createGroupservice', [ProductModelController::class, 'createGroupservice']);
    // end group service

    // Product
    Route::post('products/create/', [ProductContraller::class, 'create']);
    Route::post('products/create/{product}', [ProductContraller::class, 'crateUnite']);
    Route::post('products/createID/', [ProductContraller::class, 'createID']);
    Route::get('products/units/{product}', [ProductContraller::class, 'getUnit']);
    Route::get('products/viewpro', [ProductContraller::class, 'viewpro']);
    Route::get('products/edite/{product}', [ProductContraller::class, 'edit']);
    Route::post('products/viewallproduct', [ProductContraller::class, 'viewAllProduct']);
    Route::get('products/editlink/{product}', [ProductContraller::class, 'editlink']);
    Route::get('products/destLink/{product}', [ProductContraller::class, 'destLink']);
    Route::get('products/getProductCAt/{product}', [ProductContraller::class, 'getProductCAt']);
    Route::post('products/update/{product}', [ProductContraller::class, 'update']);
    Route::post('products/updateProductLinke/{product}', [ProductContraller::class, 'updateProductLinke']);
    Route::post('products/getlinkunit/{product}', [ProductContraller::class, 'getlinkunit']);
    Route::post('products/getboomline/{product}', [ProductContraller::class, 'getboomline']);
    Route::get('products/productsearch/', [ProductContraller::class, 'productsearch']);
    Route::post('purchase/delete/getPrulinkUnit/{purchase}', [PurcheaOrderContraller::class, 'getPrulinkUnit']);
    Route::post('products/saveDataFformexcel/', [ProductContraller::class, 'saveDataFformexcel']);
    Route::post('products/SaveDataLinkexcel/', [ProductContraller::class, 'SaveDataLinkexcel']);
    Route::post('products/saveDataFformexcel1/', [ProductContraller::class, 'saveDataFformexcel1']);
    Route::get('products/Productprescriton/', [ProductContraller::class, 'Productprescriton']);
    Route::post('products/active/', [ProductContraller::class, 'activeBtn']);
    Route::get('products/diagnos/', [ProductContraller::class, 'ProductDiagnos']);
    Route::get('product/getTypeproduct', [ProductContraller::class, 'getTypeProduct']);
    Route::get('products/productSell', [ProductContraller::class, 'productSell']);
    Route::get('products/searchMedicine/{search}', [ProductContraller::class, 'SearchMedicine']);
    Route::get('products/searchMedicineByDiagnosis/{search}', [ProductContraller::class, 'searchProductByDiagnosis']);
    Route::get('products/getProductServicetest/{id}', [ProductContraller::class, 'Show']);
    Route::get('products/searchViewProduct/', [ProductContraller::class, 'searchViewProduct']);

    Route::get('category/', [CategorysController::class, 'index']);
    Route::post('categorys/store', [CategorysController::class, 'store']);
    Route::post('categorys/update/{category}', [CategorysController::class, 'updates']);
    Route::post('category/delete/{category}', [CategorysController::class, 'destroy']);
    Route::get('category/getCatName', [CategorysController::class, 'getCatName']);

    // Brands
    Route::get('brands/', [BrandsController::class, 'index']);
    Route::post('brands/store', [BrandsController::class, 'store']);

    // suppliyers
    Route::get('suppliyers/getdatasub', [SuppliyerController::class, 'getdatasub']);
    Route::get('suppliyers/', [SuppliyerController::class, 'index']);
    Route::post('suppliyers/store/', [SuppliyerController::class, 'store']);
    Route::post('suppliyers/update/{suppliyers}', [SuppliyerController::class, 'update']);
    Route::post('suppliyers/SearchSup', [SuppliyerController::class, 'SearchSup']);
    Route::get('suppliyers/searchSupplier/{search}', [SuppliyerController::class, 'searchSupplier']);

    // Units code
    Route::get('getunitservice/', [UnitofMeasureController::class, 'getunitservice']);
    Route::get('getunite/', [UnitofMeasureController::class, 'getunite']);
    Route::get('units/', [UnitofMeasureController::class, 'index']);
    Route::post('units/store', [UnitofMeasureController::class, 'store']);
    Route::post('units/update/{units}', [UnitofMeasureController::class, 'update']);
    Route::post('units/delete/{units}', [UnitofMeasureController::class, 'destroy']);
    Route::get('units/getunitsproduct', [UnitofMeasureController::class, 'Getunitforproduct']);
    Route::get('units/allUnite', [UnitofMeasureController::class, 'allUnite']);
    Route::post('units/searchUnit', [UnitofMeasureController::class, 'searchUnit']);

    // Units type
    Route::get('getunittypeservice/', [UnitTypeMeasureController::class, 'getunitservice']);
    Route::get('getunitype/', [UnitTypeMeasureController::class, 'getunite']);
    Route::get('unittype/', [UnitTypeMeasureController::class, 'index']);
    Route::post('unittype/storeUitType', [UnitTypeMeasureController::class, 'store']);
    Route::post('unittype/update/{unittype}', [UnitTypeMeasureController::class, 'update']);
    Route::post('unittype/delete/{unittype}', [UnitTypeMeasureController::class, 'destroy']);
    Route::get('unittype/getunitType', [UnitTypeMeasureController::class, 'Getunitforproduct']);
    Route::get('unittype/allUnitType', [UnitTypeMeasureController::class, 'allUnite']);
    Route::post('unittype/searchUnitType', [UnitTypeMeasureController::class, 'searchUnit']);

    Route::get('serail/', [SerailController::class, 'index']);
    Route::post('serail/store/', [SerailController::class, 'store']);
    Route::post('serail/update/{serail}', [SerailController::class, 'update']);
    Route::post('serail/delete/{serail}', [SerailController::class, 'destroy']);

    Route::get('boomproduct/', [ProductBomContraller::class, 'index']);
    Route::post('boomproduct/{broid}', [ProductBomContraller::class, 'getboom']);
    Route::post('boomcreate/{broid}', [ProductBomContraller::class, 'boomcreate']);
    Route::get('getproductboom/{broid}', [ProductBomContraller::class, 'getproductboom']);
    Route::put('boomproduct/update/{broid}', [ProductBomContraller::class, 'update']);
    Route::get('boomproduct/search/{broid}', [ProductBomContraller::class, 'search']);
    Route::get('boomproduct/destroy/{broid}', [ProductBomContraller::class, 'destroy']);
    Route::get('boomproduct/show/{broid}', [ProductBomContraller::class, 'show']);

    // Purchase Order and Purchase Line
    Route::get('purchase/{purchase}', [PurcheaOrderContraller::class, 'index']);
    Route::post('purchase/store/', [PurcheaOrderContraller::class, 'store']);
    Route::post('purchase/purchaseorder/view', [PurcheaOrderContraller::class, 'purchaseorderView']);
    Route::get('purchase/edit/purchaseorder/{purchase}', [PurcheaOrderContraller::class, 'editPurchaseorder']);
    Route::post('purchase/update/purchaseorder/{purchase}', [PurcheaOrderContraller::class, 'updatePurchaseorder']);
    Route::post('purchase/addrow/purchaseline/{purchase}', [PurcheaOrderContraller::class, 'addrowPurchaseline']);
    Route::get('purchase/edit/purchaseline/{purchase}', [PurcheaOrderContraller::class, 'editPurchaseline']);
    Route::post('purchase/delete/purchaseline/{purchase}', [PurcheaOrderContraller::class, 'deletePurchaseline']);
    Route::post('purchase/update/purchaseline/{purchase}', [PurcheaOrderContraller::class, 'updatePurchaseline']);
    Route::post('purchase/update/Purchaselinealert/{purchase}', [PurcheaOrderContraller::class, 'Purchaselinealert']);
    Route::post('purchase/delete/purchaseorder/{purchase}', [PurcheaOrderContraller::class, 'deletePurchaseorder']);
    Route::post('purchase/delete/getPrulinkUnit/{purchase}', [PurcheaOrderContraller::class, 'getPrulinkUnit']);
    Route::post('purchase/PrulinkeSave/', [PurcheaOrderContraller::class, 'PrulinkeSave']);
    Route::get('purchase/search/{search}', [PurcheaOrderContraller::class, 'search']);
    Route::get('purchase/purchesesup/{search}', [PurcheaOrderContraller::class, 'purchesesup']);
    Route::get('purchase/purcheseuser/{search}', [PurcheaOrderContraller::class, 'purcheseuser']);
    Route::post('purchase/searchPurchase', [PurcheaOrderContraller::class, 'searchPurchase']);

    // ReceiveOrderContraller
    Route::get('receive/getpurchea', [ReceiveOrderContraller::class, 'getshowpurchea']);
    Route::post('receive/receptlink/', [ReceiveOrderContraller::class, 'receptlink']);
    Route::post('receive/recept/', [ReceiveOrderContraller::class, 'store']);
    Route::post('receive/getrecpt/', [ReceiveOrderContraller::class, 'getrecpt']);
    Route::post('receive/searchRecieve', [ReceiveOrderContraller::class, 'searchRecieve']);

    Route::post('stocktraview/', [StockTransactionContraller::class, 'index']);
    Route::get('stocktraview/search/{search}', [StockTransactionContraller::class, 'mySearch']);
    Route::get('stocktraview/stockalert/{search}', [StockTransactionContraller::class, 'stockalert']);
    Route::get('stocktraview/ex_alert/{search}', [StockTransactionContraller::class, 'ex_alert']);
    Route::get('stocktraview/viewproduct/{search}', [StockTransactionContraller::class, 'viewproduct']);
    // Stock count
    Route::get('stockcout/create/', [StockCountController::class, 'store']);
    Route::get('stockcout/stockview/{stockcout}', [StockCountController::class, 'stockview']);
    Route::get('stockcout/stocklineview/{stockcout}', [StockCountController::class, 'stocklineview']);
    Route::get('stockcout/editStocklineview/{stockcout}', [StockCountController::class, 'editStocklineview']);
    Route::post('stockcout/updatestockline/{stockcout}', [StockCountController::class, 'updatestockline']);
    Route::post('stockcout/addrowstockline/{stockcout}', [StockCountController::class, 'addrowstockline']);
    Route::post('stockcout/getValicode/{stockcout}', [StockCountController::class, 'getValicode']);
    Route::get('stockcout/getStock/{stockcout}', [StockCountController::class, 'getStock']);
    Route::post('stockcout/updatestock/{stockcout}', [StockCountController::class, 'updatestock']);
    Route::post('stockcout/getviewscockcount/', [StockCountController::class, 'getviewscockcount']);
    Route::post('stockcout/bookingcountstock/{stockcout}', [StockCountController::class, 'bookingcountstock']);
    Route::post('stockcheck/checkOptionstock', [StockalertController::class, 'checkOption_stock']);
    Route::get('stockcount/search/{search}', [StockCountController::class, 'searchStockCount']);

    // currency
    Route::get('curency/getdata/', [CurencyController::class, 'index']);
    Route::post('curency/create/{curency}', [CurencyController::class, 'create']);
    Route::post('curency/update/{curency}', [CurencyController::class, 'update']);
    Route::post('curency/delete/{curency}', [CurencyController::class, 'destroy']);

    // getSetupExchangeRateController
    Route::get('getSetup/', [ExchangeRateController::class, 'getSetup']);
    Route::post('employee_name/', [ExchangeRateController::class, 'employee_name']);
    Route::get('exchangerate/index/', [ExchangeRateController::class, 'index']);
    Route::get('getcurency/', [ExchangeRateController::class, 'getcurency']);
    Route::post('exchangerate/create/', [ExchangeRateController::class, 'create']);
    Route::post('exchangerate/update/{exchangerate}', [ExchangeRateController::class, 'update']);
    Route::get('exchangerate/destroy/{exchangerate}', [ExchangeRateController::class, 'destroy']);

    // Payment Method
    Route::post('payment/index', [PaymentMethodController::class, 'index']);
    Route::post('payment/create/', [PaymentMethodController::class, 'store']);
    Route::post('payment/update/{payment}', [PaymentMethodController::class, 'update']);
    Route::get('payment/destroy/{payment}', [PaymentMethodController::class, 'destroy']);
    Route::get('payment/allPay', [PaymentMethodController::class, 'allPay']);

    Route::get('paymentamount/index/', [PaymentController::class, 'index']);
    Route::post('paymentamount/create/', [PaymentController::class, 'create']);
    Route::post('paymentamount/update/{payment}', [PaymentController::class, 'update']);
    Route::get('paymentamount/destroy/{payment}', [PaymentController::class, 'destroy']);
    Route::post('payment/paymepos/', [PaymentController::class, 'PaymenPos']);

    // Payment Voucher
    Route::post('paymentvoucher/create/', [paymentVoucherController::class, 'create']);
    Route::get('paymentvoucher/getdataPayment/{payment}', [paymentVoucherController::class, 'getdataPayment']);
    Route::get('paymentvoucher/getdataPaymentlist/{payment}', [paymentVoucherController::class, 'getdataPaymentlist']);
    Route::post('paymentvoucher/search/{payment}', [paymentVoucherController::class, 'search']);
    Route::post('paymentvoucher/updatPayment/{payment}', [paymentVoucherController::class, 'updatPayment']);
    Route::get('paymentvoucher/getPaymenmethod/', [paymentVoucherController::class, 'getPaymenmethod']);
    Route::post('paymentvoucher/updatPaymentlink/{payment}', [paymentVoucherController::class, 'updatPaymentlink']);
    Route::post('paymentvoucher/addrowPaymentline/{purchase}', [paymentVoucherController::class, 'addrowPaymentline']);
    Route::get('paymentvoucher/userpaymentr/{purchase}', [paymentVoucherController::class, 'userpaymentr']);
    Route::get('paymentvoucher/paymentget/', [paymentVoucherController::class, 'paymentget']);
    Route::post('paymentvoucher/getpaymentvouchor/{purchase}', [paymentVoucherController::class, 'getpaymentvouchor']);
    Route::post('paymentvoucher/getPaymentlisk/{purchase}', [paymentVoucherController::class, 'getPaymentlisk']);
    Route::post('paymentvoucher/InsertLink/{purchase}', [paymentVoucherController::class, 'InsertLink']);
    Route::get('paymentvoucher/soppaymentr/{purchase}', [paymentVoucherController::class, 'soppaymentr']);

    Route::get('jusinsController/index/', [jusinsController::class, 'index']);

    // Prescription
    Route::get('prescription/index/', [PrescriptionsController::class, 'index']);
    Route::get('prescription/indexconvert/', [PrescriptionsController::class, 'indexConvert']);
    Route::get('prescription/indexpayment/', [PrescriptionsController::class, 'indexPayment']);
    Route::post('prescription/predetailcreatet/{prescri}', [PrescriptionsController::class, 'prescriptionDetailcreatet']);
    Route::get('prescription/getprescriptionDetail/{prescri}', [PrescriptionsController::class, 'GetprescriptionDetail']);
    Route::post('prescription/presdetailedit/', [PrescriptionsController::class, 'prescriptionDetailEdit']);
    Route::get('prescription/getjusines/', [PrescriptionsController::class, 'GetJusinest']);
    Route::post('prescription/prescriptionedit/', [PrescriptionsController::class, 'prescriptionEdit']);
    Route::post('prescription/createvisit/', [PrescriptionsController::class, 'createvisit']);
    Route::get('prescription/preselectData/{id}', [PrescriptionsController::class, 'preselectData']);
    Route::post('prescription/paymentVocher/{id}', [PrescriptionsController::class, 'paymentVocher']);
    Route::post('prescription/checkstock/', [PrescriptionsController::class, 'checkstock']);
    Route::post('prescription/Delete/{id}', [PrescriptionsController::class, 'prescriptionDelete']);

    Route::get('prescription/getprecriptoin/{id}', [PrescriptionsController::class, 'getprecriptoin']);

    Route::get('prescription/getdatavisit/', [PrescriptionsController::class, 'getdatavisit']);
    Route::post('prescription/updatPrescription', [PrescriptionsController::class, 'updatPrescription']);

    Route::post('prescription/getPrescription/', [PrescriptionsController::class, 'selectPrescription']);
    Route::post('prescription/getLabo/', [PrescriptionsController::class, 'selectLabo']);
    Route::post('prescription/searchPrescription/', [PrescriptionsController::class, 'searchPrescription']);
    Route::post('prescription/searchLabo/', [PrescriptionsController::class, 'searchLabo']);
    Route::get('prescription/getLaboalert/', [PrescriptionsController::class, 'getlaboUnsuccess']);

    Route::get('prescription/getAllPrescription', [PrescriptionsController::class, 'getAllPrescription']);
    Route::post('prescription/searchAll', [PrescriptionsController::class, 'searchAll']);
    Route::post('prescription/getAll', [PrescriptionsController::class, 'getAlldata']);
    Route::get('prescription/getAppointment', [PrescriptionsController::class, 'getAppointment']);

    Route::get('prescription/getAppointmentPrescription', [PrescriptionsController::class, 'getAppointmentPrescription']);
    Route::get('prescription/deletePrescription/{id}', [PrescriptionsController::class, 'deletePrescription']);

    Route::get('prescription/getAlertPrescription/', [PrescriptionsController::class, 'getalertPrescription']);

    Route::get('getresultLabo/{id}', [PrescriptionsController::class, 'getResult']);
    Route::post('prescription/getAllAppointment', [PrescriptionsController::class, 'getAllAppointment']);

    // closeshift view
    Route::post('sale/getclose/{id}', [Poscontroller::class, 'getclose']);
    Route::post('sale/searchclose/{id}', [Poscontroller::class, 'searchclose']);
    Route::get('/pos/salereport/{id}', [Poscontroller::class, 'salereport']);
    Route::post('/pos/getsht/{id}', [Poscontroller::class, 'getsht']);
    Route::post('/pos/shsht/{id}', [Poscontroller::class, 'shsht']);

    Route::post('pos/inpossale/', [Poscontroller::class, 'InposLine']);
    Route::get('pos/getposdata/', [Poscontroller::class, 'getposdata']);
    Route::post('pos/delete/{id}', [Poscontroller::class, 'deleteDatepos']);
    Route::post('pos/update/{id}', [Poscontroller::class, 'update']);
    Route::post('pos/clear/', [Poscontroller::class, 'cleardata']);
    Route::post('pos/search/', [Poscontroller::class, 'proserach']);
    Route::get('pos/saveItem/{id}', [Poscontroller::class, 'savePostItem']);
    Route::get('pos/reverseItem/{id}', [Poscontroller::class, 'reverseItem']);
    Route::get('pos/checkitemreverse/{id}', [Poscontroller::class, 'isHaveItemtoReverse']);

    // sale line view
    Route::post('pos/getsale_line/{id}', [CloseShiftController::class, 'getsale_line']);
    Route::post('pos/searchitem/{id}', [CloseShiftController::class, 'searchitem']);

    // inventory
    Route::post('pos/getinven/{id}', [CloseShiftController::class, 'getinven']);
    Route::post('pos/shinven/{id}', [CloseShiftController::class, 'shinven']);

    // close shift index()
    Route::post('/closeshift/create/', [CloseShiftController::class, 'create']);
    Route::post('/closeshift/update/{id}', [CloseShiftController::class, 'update']);
    Route::get('/closeshift/getdata/', [CloseShiftController::class, 'index']);
    Route::post('/closeshift/getcheck/', [CloseShiftController::class, 'getcheck']);
    Route::get('/pos/getposcloseshift/{id}', [CloseShiftController::class, 'getposcloseshift']);
    Route::post('/pos/detailcloseshif/', [CloseShiftController::class, 'detailcloseshif']);
    Route::post('/pos/getsale/{id}', [CloseShiftController::class, 'getsale']);
    Route::post('/pos/searchsale/{id}', [CloseShiftController::class, 'searchsale']);
    Route::post('/pos/detailsaleshif/', [CloseShiftController::class, 'detailsaleshif']);
    Route::post('/pos/printsale/{id}', [CloseShiftController::class, 'printsale']);

    // expenses
    Route::post('/pos/createexpen/', [CloseShiftController::class, 'createexpen']);
    Route::get('/pos/getexpen/', [CloseShiftController::class, 'getexpen']);
    Route::post('/pos/updateexpen/{id}', [CloseShiftController::class, 'updateexpen']);
    Route::post('/pos/deleteexpen/{id}', [CloseShiftController::class, 'deleteexpen']);
    Route::post('/pos/searchex/', [CloseShiftController::class, 'searchex']);
    Route::get('/ex/viewex/', [CloseShiftController::class, 'viewex']);
    Route::post('/ex/shex/', [CloseShiftController::class, 'shex']);
    Route::post('/pos/slprint/{id}', [CloseShiftController::class, 'slprint']);
    Route::post('/pos/salsh/{id}', [CloseShiftController::class, 'salsh']);
    Route::get('/pos/getexpenType/', [Poscontroller::class, 'getexpenType']);
    Route::get('/expensDetial/{id}', [CloseShiftController::class, 'expensDetial']);

    // Employee
    Route::post('/employee/create/', [employeecontroller::class, 'create']);
    Route::get('/employee/index/', [employeecontroller::class, 'index']);
    Route::get('/employee/delete/{id}', [employeecontroller::class, 'delete']);
    Route::post('/employee/update/{id}', [employeecontroller::class, 'update']);

    // role
    Route::post('/role/ctreate', [employeecontroller::class, 'rolecreate']);
    Route::post('/role/update', [employeecontroller::class, 'roleupdate']);
    Route::post('/role/delete', [employeecontroller::class, 'roledatele']);
    Route::post('/role/index', [employeecontroller::class, 'roleindex']);
    Route::post('/role/getid', [employeecontroller::class, 'rolegetID']);

    // permistion
    Route::post('/permission/create', [employeecontroller::class, 'permicreate']);
    Route::post('/permission/update', [employeecontroller::class, 'permiupdate']);
    Route::post('/permission/datele', [employeecontroller::class, 'permidatele']);
    Route::post('/permission/index', [employeecontroller::class, 'permindex']);
    Route::post('/permission/getId', [employeecontroller::class, 'permgetID']);

    // role permition
    Route::post('/permrole/create', [employeecontroller::class, 'permirolecreate']);
    Route::post('/permrole/create', [employeecontroller::class, 'permirolecreate']);
    Route::post('/permrole/create', [employeecontroller::class, 'permirolecreate']);
    Route::post('/permrole/create', [employeecontroller::class, 'permirolecreate']);
    Route::post('/permrole/create', [employeecontroller::class, 'permirolecreate']);
    Route::post('/permissionrole/getOne/{role}', [employeecontroller::class, 'getOne']);

    Route::post('/permrole/update', [employeecontroller::class, 'permiroleupdate']);
    Route::post('/permrole/detele', [employeecontroller::class, 'permiroledatele']);

    // Pateint UserController
    Route::post('/pateint/create/', [pateintcontroller::class, 'create']);
    Route::get('/pateint/index/', [pateintcontroller::class, 'index']);
    Route::get('/pateint/delete/{id}', [pateintcontroller::class, 'delete']);
    Route::post('/pateint/update/{id}', [pateintcontroller::class, 'update']);
    Route::post('/pateint/diagnosis/', [pateintcontroller::class, 'pdiagnosis']);
    Route::get('/pateint/getPateint/{id}', [pateintcontroller::class, 'getDataPateint']);

    Route::post('/pateint/diagnosisCreate/', [pateintcontroller::class, 'diagnosisCreate']);
    Route::post('/pateint/diagnosisUpdate/{id}', [pateintcontroller::class, 'diagnosisUpdate']);
    Route::get('/pateint/diagnosisSelect/{id}', [pateintcontroller::class, 'diagnosisSelect']);

    // Search patient
    Route::post('/patient/searchPatient/', [pateintcontroller::class, 'searchPatient']);

    Route::post('/patient/searchEnterPatient/', [pateintcontroller::class, 'searchEnterPatient']);

    Route::post('/uers/create/', [UserController::class, 'create']);
    Route::get('/uers/index/', [UserController::class, 'index']);
    Route::post('/setup/createsetup/', [UserController::class, 'createsetup']);
    Route::get('/setup/selectsetup/', [UserController::class, 'selectsetup']);
    Route::post('/setup/updatesetup/{id}', [UserController::class, 'updatesetup']);
    Route::post('/setup/getuseId/', [UserController::class, 'getuseId']);

    Route::post('/testservice/create/{created_by}', [testserviceController::class, 'create']);
    Route::get('/testservice/gettestservice/{id}', [testserviceController::class, 'gettestservice']);
    Route::post('/testservice/update/{id}', [testserviceController::class, 'update']);
    Route::get('/testservice/destroy/{id}', [testserviceController::class, 'destroy']);

    // product service test
    Route::post('/productservicetest', [ServiceTestController::class, 'index']);
    Route::get('/servicetestview/', [ServiceTestController::class, 'getServicetestview']);
    Route::post('/getallviewproduct/', [ServiceTestController::class, 'getAllViewProduct']);

    // User_view api
    Route::get('/getuserview/{id}', [User_viewController::class, 'index']);

    Route::post('/update/user/{id}', [UserController::class, 'update']);

    Route::get('/update/delete/{id}', [UserController::class, 'deleteUser']);

    Route::get('/exchangerate/find/{id}', [ExchangeRateController::class, 'search']);

    Route::post('/beginingstock/create', [BeginingStockController::class, 'createtb']);
    Route::post('/beginingstock/createlist', [BeginingStockController::class, 'create_list']);
    Route::post('/beginingstocks', [BeginingStockController::class, 'index']);
    Route::get('/beginingstock/getbyno', [BeginingStockController::class, 'getbyId']);
    Route::post('/beginingstock/update', [BeginingStockController::class, 'update']);
    Route::post('/beginingstock/updatelist', [BeginingStockController::class, 'update_list']);
    Route::get('/beginingstocks/delete', [BeginingStockController::class, 'delete']);
    Route::get('/beginingstock/deletelist', [BeginingStockController::class, 'delete_list']);
    Route::get('/beginingstock/getlist', [BeginingStockController::class, 'get_list']);
    Route::post('/beginingstock/btnReverse/{id}', [BeginingStockController::class, 'btnReverse']);
    Route::post('/beginingstock/btnCopy', [BeginingStockController::class, 'btnCopy']);
    Route::post('/beginingstock/submit', [BeginingStockController::class, 'submitbegining']);

    Route::get('/beginningstock/search/{search}', [BeginingStockController::class, 'searchStockBeginning']);

    // start stock
    Route::get('/beginningstock/stockStart/{id}', [BeginingStockController::class, 'stockStart']);
    Route::get('/beginningstock/getOpenBeginningstock', [BeginingStockController::class, 'getOpenBeginningstock']);
    Route::post('/beginningstock/Addmorelist/{id}', [BeginingStockController::class, 'Addmorelist']);
    Route::post('/beginningstock/searchList', [BeginingStockController::class, 'searchList']);

    // Laboratory
    Route::get('/laboratory/getData', [LaboController::class, 'index']);
    Route::post('/laboratory/createLabolist', [LaboController::class, 'create']);
    Route::post('/laboratory/updateList/{id}', [LaboController::class, 'edit']);
    Route::get('/laboratory/deletelist/{id}', [LaboController::class, 'destroy']);
    Route::get('laboratory/getLaboId/{id}', [LaboController::class, 'getLaboId']);
    Route::post('/laboratory/submitLabo/{id}', [LaboController::class, 'submitLabo']);
    Route::get('/laboratory/getLabolab', [LaboController::class, 'LaboLab']);
    Route::get('/laboratory/getPatientLabo', [LaboController::class, 'getPatientLabo']);
    Route::post('/laboratory/getViewPatientLabo', [LaboController::class, 'getViewPatientLabo']);
    Route::get('/prescription/deleteMedicine/{id}', [LaboController::class, 'DeleteMedicine']);
    Route::get('laboratory/getpatientLabo/{id}', [LaboController::class, 'getOneLabolab']);
    Route::get('laboratory/SubmitBack/{id}', [LaboController::class, 'SubmitBackfromLabo']);
    Route::post('laboratory/search/', [LaboController::class, 'searchPatientLabo']);
    Route::get('laboratory/report/', [LaboController::class, 'getDataReport']);

    // boom product in service
    Route::get('/boomproduct/getMetarial/', [ProductContraller::class, 'getMetariels']);
    Route::get('/boomproduct/getMetarialUnit/{id}', [ProductContraller::class, 'getMetarialUnit']);
    Route::post('/boomproduct/searchMetarial/{search}', [ProductContraller::class, 'searchMetarial']);

    // Search service
    Route::post('/searchServiceTest/', [ProductContraller::class, 'searchService']);
    Route::get('/gettypeproduct/', [ProductContraller::class, 'getProduct']);
    Route::get('/searchProductType/{search}', [ProductContraller::class, 'searchProductType']);
    Route::get('/getproductstockcount', [ProductContraller::class, 'getProductStockCount']);
    Route::get('/searchproductstockcount/{search}', [ProductContraller::class, 'SearchProductStockCount']);

    Route::post('createLabo/createNewAppointment/', [PrescriptionsController::class, 'createNewAppointment']);
    Route::post('prescription/removeAppoint/{id}', [PrescriptionsController::class, 'removeAppointment']);
    Route::post('prescription/addnewAppoint/{id}', [PrescriptionsController::class, 'addNewAppointment']);

    Route::post('search/createPrescription/', [pateintcontroller::class, 'createPrescription']);
    Route::post('/createPatientWithPrescription/create', [pateintcontroller::class, 'createPatientWithPrescription']);

    // report
    Route::get('salereport/get/', [ReportController::class, 'index']);
    Route::post('salereport/getsaleproduct/', [ReportController::class, 'getSaleProduct']);
    Route::get('salereport/getdatacountday', [ReportController::class, 'dataCountDay']);
    Route::post('product/productreport', [ReportController::class, 'productReport']);
    Route::post('report/incomeReport', [ReportController::class, 'incomeReport']);

    Route::post('expenseType/create', [ReportController::class, 'addExpenseType']);
    Route::post('expenseType/update/{id}', [ReportController::class, 'updateExpense']);
    Route::get('expenseType/delete/{id}', [ReportController::class, 'delete']);
    Route::post('report/getexpense', [ReportController::class, 'getExpense']);

    // Purchase
    Route::get('purchase/booking/{id}', [PurcheaOrderContraller::class, 'bookingPurchase']);

    Route::post('user/changePassword', [UserController::class, 'updatePassword']);

    // calender
    Route::get('/calendar', [CalendarController::class, 'index']);

    // get product with varian code
    Route::post('product/viewproductVariancode', [StockTransactionContraller::class, 'viewproductVariancode']);
    Route::get('saledetail/getpriceDatail/{id}', [CloseShiftController::class, 'getpriceDatail']);

    // reverse beginning stock
    Route::post('beginningstock/updateReverse', [BeginingStockController::class, 'updateReverse']);
    Route::post('payment/reverseSaleProduct', [PaymentController::class, 'reverseSaleProduct']);
    Route::post('payment/copyToAgain', [PaymentController::class, 'copyToAgain']);
    Route::get('payment/getReversePatient/{id}', [PaymentController::class, 'getReversePatient']);

    Route::post('patient/searchPatientLabo', [pateintcontroller::class, 'searchPatientLabo']);
    Route::post('labo/getDoctorName', [laboservicecontroller::class, 'getDoctorName']);
    Route::get('/severvices/get/{id}', [laboservicecontroller::class, 'get']);

    Route::get('stockcount/deletestockcount/{id}', [StockCountController::class, 'deletestockcount']);
    Route::get('stockcount/deleteLine/{id}', [StockCountController::class, 'deleteLine']);
    Route::post('product/posSearch', [ProductContraller::class, 'posSearch']);

    //submit stock count
    Route::post('stockcount/saveStock', [StockCountController::class, 'saveStock']);


    //create subservice test
    Route::post('/testservice/createSubtest/{id}', [testserviceController::class, 'createSubtest']);
    Route::get('/testservice/getSubtest/{id}', [testserviceController::class, 'getSubtest']);
    Route::post('/testservice/updateSubtest/{id}', [testserviceController::class, 'updateSubtest']);
    Route::get('/testservice/deleteSubtest/{id}', [testserviceController::class, 'deleteSubtest']);

    Route::get('/testservice/getAllsubtest/{id}', [testserviceController::class, 'getAllsubtest']);
    Route::get('/testservice/getOneSubtest/{id}', [testserviceController::class, 'getOneSubtest']);
    Route::get('/receiveorder/Reverse/{id}', [ReceiveOrderContraller::class, 'Reverse']);


});
